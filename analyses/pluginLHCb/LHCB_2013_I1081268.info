Name: LHCB_2013_I1081268
Year: 2013
Summary: Helicity angles in $B^0_S\to J/\psi f_0(980)$
Experiment: LHCB
Collider: LHC
InspireID: 1081268
Status: VALIDATED NOHEPDATA
Reentrant: true
Authors:
 - Peter Richardson <peter.richardson@durham.ac.uk>
References:
 - Phys.Lett.B 707 (2012) 497-505
RunInfo: Any process producing B_s0, originally pp
Description:
  'Measurement of the helicity angles in $B^0_S\to J/\psi f_0(980)$. The corrected data were read from Figure 4 in the paper.'
ValidationInfo:
  'Herwig7 event using EvtGen for B decays'
#ReleaseTests:
# - $A my-hepmc-prefix :MODE=some_rivet_flag
Keywords: []
BibKey: LHCb:2011ab
BibTeX: '@article{LHCb:2011ab,
    author = "Aaij, R. and others",
    collaboration = "LHCb",
    title = "{Measurement of the CP violating phase $\phi_s$ in $\bar{B}^0_s \to J/\psi f_0(980)$}",
    eprint = "1112.3056",
    archivePrefix = "arXiv",
    primaryClass = "hep-ex",
    reportNumber = "CERN-PH-EP-2011-205, LHCB-PAPER-2011-031",
    doi = "10.1016/j.physletb.2012.01.017",
    journal = "Phys. Lett. B",
    volume = "707",
    pages = "497--505",
    year = "2012"
}
'
