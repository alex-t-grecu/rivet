BEGIN PLOT /LHCB_2013_I1081268/d01-x01-y01
Title=$J/\psi$ helicity angle for $B^0_S\to J/\psi f_0(980)$
XLabel=$\cos\theta_{J/\psi}$
YLabel=$1/\Gamma\text{d}\Gamma/\text{d}\cos\theta_{J/\psi}$
LogY=0
END PLOT
BEGIN PLOT /LHCB_2013_I1081268/d01-x01-y02
Title=$f_0(980)$ helicity angle for $B^0_S\to J/\psi f_0(980)$
XLabel=$\cos\theta_{f_0(980)}$
YLabel=$1/\Gamma\text{d}\Gamma/\text{d}\cos\theta_{f_0(980)}$
LogY=0
END PLOT
