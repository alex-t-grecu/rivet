BEGIN PLOT /LHCB_2022_I1971920/d01-x01-y01
Title=$\alpha$ for $\Lambda_b^0\to\Lambda^0\gamma$ and $\bar\Lambda_b^0\to\bar\Lambda^0\gamma$
XLabel=
YLabel=$\alpha$
LogY=0
END PLOT
