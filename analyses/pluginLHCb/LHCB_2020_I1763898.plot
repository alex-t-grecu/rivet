BEGIN PLOT /LHCB_2020_I1763898/d0[1,3]
XLabel=$p_\perp$ [GeV]
YLabel= $\eta_c/J/\psi$
END PLOT
BEGIN PLOT /LHCB_2020_I1763898/d0[2,4]
XLabel=$p_\perp$ [GeV]
YLabel=$\mathrm{d}\sigma/\mathrm{d}p_\perp$ [nb/GeV]
END PLOT

BEGIN PLOT /LHCB_2020_I1763898/d01-x01-y01
Title=Ratio of $\eta_c$/$J/\psi$ for prompt $\eta_c$ production w.r.t $p_\perp$($2<y<4.5$)
END PLOT
BEGIN PLOT /LHCB_2020_I1763898/d02-x01-y01
Title=Differential cross section for prompt $\eta_c$ production w.r.t $p_\perp$($2<y<4.5$)
END PLOT
BEGIN PLOT /LHCB_2020_I1763898/d03-x01-y01
Title=Ratio of $\eta_c$/$J/\psi$ for non-prompt $\eta_c$ production w.r.t $p_\perp$($2<y<4.5$)
END PLOT
BEGIN PLOT /LHCB_2020_I1763898/d04-x01-y01
Title=Differential cross section for non-prompt $\eta_c$ production w.r.t $p_\perp$($2<y<4.5$)
END PLOT
