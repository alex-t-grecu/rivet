BEGIN PLOT /LHCB_2018_I1670013/d[01,02,03]
XLabel=$p_\perp$ [GeV]
YLabel=$\mathrm{Br}(\mu^+\mu^-)\mathrm{d}^2\sigma/\mathrm{d}p_\perp\mathrm{d}y$ [nb/GeV]
END PLOT

BEGIN PLOT /LHCB_2018_I1670013/d01-x01-y01
Title=$\mathrm{d}^2\sigma/\mathrm{d}p_\perp\mathrm{d}y$ for $\Upsilon(1S)$ at 13 TeV ($2.0<y<2.5$) 
END PLOT
BEGIN PLOT /LHCB_2018_I1670013/d01-x01-y02
Title=$\mathrm{d}^2\sigma/\mathrm{d}p_\perp\mathrm{d}y$ for $\Upsilon(1S)$ at 13 TeV ($2.5<y<3.0$) 
END PLOT
BEGIN PLOT /LHCB_2018_I1670013/d01-x01-y03
Title=$\mathrm{d}^2\sigma/\mathrm{d}p_\perp\mathrm{d}y$ for $\Upsilon(1S)$ at 13 TeV ($3.0<y<3.5$) 
END PLOT
BEGIN PLOT /LHCB_2018_I1670013/d01-x01-y04
Title=$\mathrm{d}^2\sigma/\mathrm{d}p_\perp\mathrm{d}y$ for $\Upsilon(1S)$ at 13 TeV ($3.5<y<4.0$) 
END PLOT
BEGIN PLOT /LHCB_2018_I1670013/d01-x01-y05
Title=$\mathrm{d}^2\sigma/\mathrm{d}p_\perp\mathrm{d}y$ for $\Upsilon(1S)$ at 13 TeV ($4.0<y<4.5$) 
END PLOT

BEGIN PLOT /LHCB_2018_I1670013/d02-x01-y01
Title=$\mathrm{d}^2\sigma/\mathrm{d}p_\perp\mathrm{d}y$ for $\Upsilon(2S)$ at 13 TeV ($2.0<y<2.5$) 
END PLOT
BEGIN PLOT /LHCB_2018_I1670013/d02-x01-y02
Title=$\mathrm{d}^2\sigma/\mathrm{d}p_\perp\mathrm{d}y$ for $\Upsilon(2S)$ at 13 TeV ($2.5<y<3.0$) 
END PLOT
BEGIN PLOT /LHCB_2018_I1670013/d02-x01-y03
Title=$\mathrm{d}^2\sigma/\mathrm{d}p_\perp\mathrm{d}y$ for $\Upsilon(2S)$ at 13 TeV ($3.0<y<3.5$) 
END PLOT
BEGIN PLOT /LHCB_2018_I1670013/d02-x01-y04
Title=$\mathrm{d}^2\sigma/\mathrm{d}p_\perp\mathrm{d}y$ for $\Upsilon(2S)$ at 13 TeV ($3.5<y<4.0$) 
END PLOT
BEGIN PLOT /LHCB_2018_I1670013/d02-x01-y05
Title=$\mathrm{d}^2\sigma/\mathrm{d}p_\perp\mathrm{d}y$ for $\Upsilon(2S)$ at 13 TeV ($4.0<y<4.5$) 
END PLOT

BEGIN PLOT /LHCB_2018_I1670013/d03-x01-y01
Title=$\mathrm{d}^2\sigma/\mathrm{d}p_\perp\mathrm{d}y$ for $\Upsilon(3S)$ at 13 TeV ($2.0<y<2.5$) 
END PLOT
BEGIN PLOT /LHCB_2018_I1670013/d03-x01-y02
Title=$\mathrm{d}^2\sigma/\mathrm{d}p_\perp\mathrm{d}y$ for $\Upsilon(3S)$ at 13 TeV ($2.5<y<3.0$) 
END PLOT
BEGIN PLOT /LHCB_2018_I1670013/d03-x01-y03
Title=$\mathrm{d}^2\sigma/\mathrm{d}p_\perp\mathrm{d}y$ for $\Upsilon(3S)$ at 13 TeV ($3.0<y<3.5$) 
END PLOT
BEGIN PLOT /LHCB_2018_I1670013/d03-x01-y04
Title=$\mathrm{d}^2\sigma/\mathrm{d}p_\perp\mathrm{d}y$ for $\Upsilon(3S)$ at 13 TeV ($3.5<y<4.0$) 
END PLOT
BEGIN PLOT /LHCB_2018_I1670013/d03-x01-y05
Title=$\mathrm{d}^2\sigma/\mathrm{d}p_\perp\mathrm{d}y$ for $\Upsilon(3S)$ at 13 TeV ($4.0<y<4.5$) 
END PLOT

BEGIN PLOT /LHCB_2018_I1670013/d04
XLabel=$p_\perp$ [GeV]
YLabel=$\mathrm{Br}(\mu^+\mu^-)\mathrm{d}\sigma/\mathrm{d}p_\perp$ [nb/GeV]
END PLOT
BEGIN PLOT /LHCB_2018_I1670013/d04-x01-y01
Title=$\mathrm{d}\sigma/\mathrm{d}p_\perp$ for $\Upsilon(1S)$ at 13 TeV
END PLOT
BEGIN PLOT /LHCB_2018_I1670013/d04-x01-y02
Title=$\mathrm{d}\sigma/\mathrm{d}p_\perp$ for $\Upsilon(2S)$ at 13 TeV
END PLOT
BEGIN PLOT /LHCB_2018_I1670013/d04-x01-y03
Title=$\mathrm{d}\sigma/\mathrm{d}p_\perp$ for $\Upsilon(3S)$ at 13 TeV
END PLOT

BEGIN PLOT /LHCB_2018_I1670013/d05
XLabel=$y$
YLabel=$\mathrm{Br}(\mu^+\mu^-)\mathrm{d}\sigma/\mathrm{d}y$ [nb]
END PLOT
BEGIN PLOT /LHCB_2018_I1670013/d05-x01-y01
Title=$\mathrm{d}\sigma/\mathrm{d}y$ for $\Upsilon(1S)$ at 13 TeV
END PLOT
BEGIN PLOT /LHCB_2018_I1670013/d05-x01-y02
Title=$\mathrm{d}\sigma/\mathrm{d}y$ for $\Upsilon(2S)$ at 13 TeV
END PLOT
BEGIN PLOT /LHCB_2018_I1670013/d05-x01-y03
Title=$\mathrm{d}\sigma/\mathrm{d}y$ for $\Upsilon(3S)$ at 13 TeV
END PLOT

BEGIN PLOT /LHCB_2018_I1670013/d06
XLabel=$p_\perp$ [GeV]
YLabel=$N(\Upsilon(2S)\to\mu^+\mu^-)/N(\Upsilon(1S)\to\mu^+\mu^-)$
LogY=0
END PLOT
BEGIN PLOT /LHCB_2018_I1670013/d06-x01-y01
Title=Double differential ratio of $\Upsilon(2S)$ w.r.t $\Upsilon(1S)$ at 13 TeV ($2.0<y<2.5$) 
END PLOT
BEGIN PLOT /LHCB_2018_I1670013/d06-x01-y02
Title=Double differential ratio of $\Upsilon(2S)$ w.r.t $\Upsilon(1S)$ at 13 TeV ($2.5<y<3.0$) 
END PLOT
BEGIN PLOT /LHCB_2018_I1670013/d06-x01-y03
Title=Double differential ratio of $\Upsilon(2S)$ w.r.t $\Upsilon(1S)$ at 13 TeV ($3.0<y<3.5$) 
END PLOT
BEGIN PLOT /LHCB_2018_I1670013/d06-x01-y04
Title=Double differential ratio of $\Upsilon(2S)$ w.r.t $\Upsilon(1S)$ at 13 TeV ($3.5<y<4.0$) 
END PLOT
BEGIN PLOT /LHCB_2018_I1670013/d06-x01-y05
Title=Double differential ratio of $\Upsilon(2S)$ w.r.t $\Upsilon(1S)$ at 13 TeV ($4.0<y<4.5$) 
END PLOT

BEGIN PLOT /LHCB_2018_I1670013/d07
XLabel=$p_\perp$ [GeV]
YLabel=$N(\Upsilon(3S)\to\mu^+\mu^-)/N(\Upsilon(1S)\to\mu^+\mu^-)$
LogY=0
END PLOT
BEGIN PLOT /LHCB_2018_I1670013/d07-x01-y01
Title=Double differential ratio of $\Upsilon(3S)$ w.r.t $\Upsilon(1S)$ at 13 TeV ($2.0<y<2.5$) 
END PLOT
BEGIN PLOT /LHCB_2018_I1670013/d07-x01-y02
Title=Double differential ratio of $\Upsilon(3S)$ w.r.t $\Upsilon(1S)$ at 13 TeV ($2.5<y<3.0$) 
END PLOT
BEGIN PLOT /LHCB_2018_I1670013/d07-x01-y03
Title=Double differential ratio of $\Upsilon(3S)$ w.r.t $\Upsilon(1S)$ at 13 TeV ($3.0<y<3.5$) 
END PLOT
BEGIN PLOT /LHCB_2018_I1670013/d07-x01-y04
Title=Double differential ratio of $\Upsilon(3S)$ w.r.t $\Upsilon(1S)$ at 13 TeV ($3.5<y<4.0$) 
END PLOT
BEGIN PLOT /LHCB_2018_I1670013/d07-x01-y05
Title=Double differential ratio of $\Upsilon(3S)$ w.r.t $\Upsilon(1S)$ at 13 TeV ($4.0<y<4.5$) 
END PLOT

BEGIN PLOT /LHCB_2018_I1670013/d08-x01-y01
XLabel=$p_\perp$ [GeV]
YLabel=$N(\Upsilon(2S)\to\mu^+\mu^-)/N(\Upsilon(1S)\to\mu^+\mu^-)$
Title=Ratio of $\Upsilon(2S)$ w.r.t $\Upsilon(1S)$ w.r.t $p_\perp$ at 13 TeV
END PLOT
BEGIN PLOT /LHCB_2018_I1670013/d08-x01-y02
XLabel=$p_\perp$ [GeV]
YLabel=$N(\Upsilon(3S)\to\mu^+\mu^-)/N(\Upsilon(1S)\to\mu^+\mu^-)$
Title=Ratio of $\Upsilon(3S)$ w.r.t $\Upsilon(1S)$ w.r.t $p_\perp$ at 13 TeV
LogY=0
END PLOT

BEGIN PLOT /LHCB_2018_I1670013/d09-x01-y01
XLabel=$y$
YLabel=$N(\Upsilon(2S)\to\mu^+\mu^-)/N(\Upsilon(1S)\to\mu^+\mu^-)$
Title=Ratio of $\Upsilon(2S)$ w.r.t $\Upsilon(1S)$ w.r.t $y$ at 13 TeV
END PLOT
BEGIN PLOT /LHCB_2018_I1670013/d09-x01-y02
XLabel=$y$
YLabel=$N(\Upsilon(3S)\to\mu^+\mu^-)/N(\Upsilon(1S)\to\mu^+\mu^-)$
Title=Ratio of $\Upsilon(3S)$ w.r.t $\Upsilon(1S)$ w.r.t $y$ at 13 TeV
LogY=0
END PLOT
