BEGIN PLOT /LHCB_2014_I1280929/d03-x01-y01
Title=$\Upsilon(1S)$ production
XLabel=$p_\perp$ [GeV]
YLabel=$\mathrm{Br}(\mu^+\mu^-)\mathrm{d}\sigma/\mathrm{d}p_\perp$ [nb/GeV]
END PLOT
BEGIN PLOT /LHCB_2014_I1280929/d04-x01-y01
Title=$\Upsilon(2S)$ production
XLabel=$p_\perp$ [GeV]
YLabel=$\mathrm{Br}(\mu^+\mu^-)\mathrm{d}\sigma/\mathrm{d}p_\perp$ [nb/GeV]
END PLOT
BEGIN PLOT /LHCB_2014_I1280929/d05-x01-y01
Title=$\Upsilon(3S)$ production
XLabel=$p_\perp$ [GeV]
YLabel=$\mathrm{Br}(\mu^+\mu^-)\mathrm{d}\sigma/\mathrm{d}p_\perp$ [nb/GeV]
END PLOT
BEGIN PLOT /LHCB_2014_I1280929/d06-x01-y01
Title=$\Upsilon(1S)$ production
XLabel=$y$
YLabel=$\mathrm{Br}(\mu^+\mu^-)\mathrm{d}\sigma/\mathrm{d}y$ [nb]
END PLOT
BEGIN PLOT /LHCB_2014_I1280929/d07-x01-y01
Title=$\Upsilon(2S)$ production
XLabel=$y$
YLabel=$\mathrm{Br}(\mu^+\mu^-)\mathrm{d}\sigma/\mathrm{d}y$ [nb]
END PLOT
BEGIN PLOT /LHCB_2014_I1280929/d08-x01-y01
Title=$\Upsilon(3S)$ production
XLabel=$y$
YLabel=$\mathrm{Br}(\mu^+\mu^-)\mathrm{d}\sigma/\mathrm{d}y$ [nb]
END PLOT

BEGIN PLOT /LHCB_2014_I1280929/d09-x01-y01
Title=Ratio of $\Upsilon(2S)/\Upsilon(1S)$ production
XLabel=$p_\perp$ [GeV]
YLabel=$N(\Upsilon(2S)\to\mu^+\mu^-)/N(\Upsilon(1S)\to\mu^+\mu^-)$
END PLOT
BEGIN PLOT /LHCB_2014_I1280929/d09-x01-y02
Title=Ratio of $\Upsilon(3S)/\Upsilon(1S)$ production
XLabel=$p_\perp$ [GeV]
YLabel=$N(\Upsilon(3S)\to\mu^+\mu^-)/N(\Upsilon(1S)\to\mu^+\mu^-)$
END PLOT

BEGIN PLOT /LHCB_2014_I1280929/d10-x01-y01
Title=Ratio of $\Upsilon(2S)/\Upsilon(1S)$ production
XLabel=$y$
YLabel=$N(\Upsilon(2S)\to\mu^+\mu^-)/N(\Upsilon(1S)\to\mu^+\mu^-)$
END PLOT
BEGIN PLOT /LHCB_2014_I1280929/d10-x01-y02
Title=Ratio of $\Upsilon(3S)/\Upsilon(1S)$ production
XLabel=$y$
YLabel=$N(\Upsilon(3S)\to\mu^+\mu^-)/N(\Upsilon(1S)\to\mu^+\mu^-)$
END PLOT
