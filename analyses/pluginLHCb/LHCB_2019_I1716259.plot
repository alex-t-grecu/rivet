BEGIN PLOT /LHCB_2019_I1716259/d01-x01-y01
Title=Ratio of $\Xi_b^-$ to $\Lambda_b^0$ production rates
YLabel=$\sigma(\Xi_b^-)\times\text{Br}(\Xi_b^-\to J/\psi\Xi^-)/\sigma(\Lambda_b^0)\times\text{Br}(\Lambda_b^0\to J/\psi\Lambda^0)$
XLabel=$\sqrt{s}$ [GeV]
LogY=0
ConnectGaps=1
END PLOT
BEGIN PLOT /LHCB_2019_I1716259/d02-x01-y01
Title=Ratio of $\Xi_b^-$ to $\Lambda_b^0$ production rates
YLabel=$\sigma(\Xi_b^-)/\sigma(\Lambda_b^0)$
XLabel=$\sqrt{s}$ [GeV]
LogY=0
ConnectGaps=1
END PLOT
