BEGIN PLOT /LHCB_2018_I1674916/d02-x01-y01
Title=$D^+_s/D^-_s$ Asymmetry at 7 TeV ($2<y<3$)
YLabel=Asymmetry
XLabel=$y$
LogY=0
END PLOT
BEGIN PLOT /LHCB_2018_I1674916/d02-x01-y02
Title=$D^+_s/D^-_s$ Asymmetry at 7 TeV ($3<y<3.5$)
YLabel=Asymmetry
XLabel=$y$
LogY=0
END PLOT
BEGIN PLOT /LHCB_2018_I1674916/d02-x01-y03
Title=$D^+_s/D^-_s$ Asymmetry at 7 TeV ($3.5<y<4.5$)
YLabel=Asymmetry
XLabel=$y$
LogY=0
END PLOT

BEGIN PLOT /LHCB_2018_I1674916/d03-x01-y01
Title=$D^+_s/D^-_s$ Asymmetry at 8 TeV ($2<y<3$)
YLabel=Asymmetry
XLabel=$y$
LogY=0
END PLOT
BEGIN PLOT /LHCB_2018_I1674916/d03-x01-y02
Title=$D^+_s/D^-_s$ Asymmetry at 8 TeV ($3<y<3.5$)
YLabel=Asymmetry
XLabel=$y$
LogY=0
END PLOT
BEGIN PLOT /LHCB_2018_I1674916/d03-x01-y03
Title=$D^+_s/D^-_s$ Asymmetry at 8 TeV ($3.5<y<4.5$)
YLabel=Asymmetry
XLabel=$y$
LogY=0
END PLOT
