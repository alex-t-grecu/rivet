BEGIN PLOT /MC_HFBRANCHING/BR_B0_clnu
Title = $B^0$ branching fractions
YLabel = Branching Fraction
XLabel = Decay mode
XCustomMajorTicks=2.0	$D^{*-}lv$	5.0	$D^{-}lv$	8.0	$D_{1}^{-}lv$	11.0	$D_{0}^{*-}lv$	14.0	$D_{1}^{'-}lv$	17.0	$D_{2}^{*-}lv$	19.0	other
LegendTitle=Three bins for each decay\newline $1^{st}$:hadron+$e\nu_e$ \newline $2^{nd}$:hadron+$\mu\nu_\mu$ \newline $3^{rd}$:hadron+$\tau\nu_\tau$
END PLOT


BEGIN PLOT /MC_HFBRANCHING/BR_B0S_clnu
Title = $B^0_s$ branching fractions
YLabel = Branching Fraction
XLabel = Decay mode
XCustomMajorTicks=2.0	$D_{s}^{*-}lv$	5.0	$D_{s}^{-}lv$	8.0	$D_{s1}(2536)^{-}lv$	11.0	$D_{s0}^{*-}lv$	14.0	$D_{s1}(2460)^{-}lv$	17.0	$D_{s2}^{*-}lv$	19.0	other
LegendTitle=Three bins for each decay\newline $1^{st}$:hadron+$e\nu_e$ \newline $2^{nd}$:hadron+$\mu\nu_\mu$ \newline $3^{rd}$:hadron+$\tau\nu_\tau$
END PLOT


BEGIN PLOT /MC_HFBRANCHING/BR_BPLUS_clnu
Title = $B^+$ branching fractions
YLabel = Branching Fraction
XLabel = Decay mode
XCustomMajorTicks=2.0	$\bar{D_0^*}$$lv$	5.0	$\bar{D^0}lv$	8.0	$\bar{D_1^0}lv$	11.0	$\bar{D_0^{*0}}lv$	14.0	$\bar{D_{1}^{'0}}lv$	17.0	$\bar{D_2^{*0}}lv$	19.0	other
LegendTitle=Three bins for each decay\newline $1^{st}$:hadron+$e\nu_e$ \newline $2^{nd}$:hadron+$\mu\nu_\mu$ \newline $3^{rd}$:hadron+$\tau\nu_\tau$
END PLOT


BEGIN PLOT /MC_HFBRANCHING/BR_LAMBDAB_clnu
Title = $\Lambda^0_b$ branching fractions
YLabel = Branching Fraction
XLabel = Decay mode
XCustomMajorTicks=2.0	$\Lambda_{c}^{+}lv$	5.0	$\Lambda_{c}(2593)^{+}lv$	8.0	$\Lambda_{c}(2625)^{+}lv$	10.0	other
LegendTitle=Three bins for each decay\newline $1^{st}$:hadron+$e\nu_e$ \newline $2^{nd}$:hadron+$\mu\nu_\mu$ \newline $3^{rd}$:hadron+$\tau\nu_\tau$
END PLOT


BEGIN PLOT /MC_HFBRANCHING/BR_D0_clnu
Title = $D^0$ branching fractions
YLabel = Branching Fraction
XLabel = Decay mode
XCustomMajorTicks=1.5	$K^{*-}lv$	3.5	$K^{-}lv$	5.5	$K_{1}^{-}lv$	7.5	$K_{2}^{*-}lv$	9.5	$\pi^{-}lv$	11.5	$\rho^{-}lv$	13.5	$K^{-}\pi^{0}lv$	15.5	$\bar{K_0}\pi^{-}lv$	17	other
LegendTitle=Two bins for each decay\newline $1^{st}$:hadron+$e\nu_e$ \newline $2^{nd}$:hadron+$\mu\nu_\mu$
END PLOT


BEGIN PLOT /MC_HFBRANCHING/BR_DPLUS_clnu
Title = $D^+$ branching fractions
YLabel = Branching Fraction
XLabel = Decay mode
XCustomMajorTicks=1.5	$\bar{K_0^*}lv$	3.5	$\bar{K^0}lv$	5.5	$\bar{K_1^0}^{-}lv$	7.5	$\bar{K_2^{*0}}^{-}lv$	9.5	$\pi^0lv$	11.5	$\rho^0lv$	13.5	$\eta$$lv$	15.5	$\eta^{'}lv$	17.5	$\omega\ell\nu$	19.5	$\bar{K_0}\pi^{0}lv$	21.5	$K^{-}\pi^{+}lv$	23.0	other
LegendTitle=Two bins for each decay\newline $1^{st}$:hadron+$e\nu_e$ \newline $2^{nd}$:hadron+$\mu\nu_\mu$
END PLOT


BEGIN PLOT /MC_HFBRANCHING/BR_DSPLUS_clnu
Title = $D^+_s$ branching fractions
YLabel = Branching Fraction
XLabel = Decay mode
XCustomMajorTicks=1.5	$\phi$$lv$	3.5	$\eta$$lv$	5.5	$\eta^{'}lv$	7.5	$\bar{K^0}lv$	9.5	$\bar{K^{0*}}lv$	11.0	other
LegendTitle=Two bins for each decay\newline $1^{st}$:hadron+$e\nu_e$ \newline $2^{nd}$:hadron+$\mu\nu_\mu$
END PLOT


BEGIN PLOT /MC_HFBRANCHING/BR_LAMBDACPLUS_clnu
Title = $\Lambda^+_c$ branching fractions
YLabel = Branching Fraction
XLabel = Decay mode
XCustomMajorTicks=1.5	$\Lambda^0$$lv$	3.5	$\Sigma^0$$lv$	5.5	$\Sigma^{0*}lv$	7.5	$n^0$$lv$	9.5	$\Delta^0$$lv$	11.5	$\pi^{+}\pi^{-}lv$	13.5	$n^{0}\pi^{0}lv$	15.0	other
LegendTitle=Two bins for each decay\newline $1^{st}$:hadron+$e\nu_e$ \newline $2^{nd}$:hadron+$\mu\nu_\mu$
END PLOT


BEGIN PLOT /MC_HFBRANCHING/B0_pT
Title = $B^0$ transverse momentum
YLabel = Normalised to 1
XLabel = $p_\mathrm{T}$ [GeV]
END PLOT


BEGIN PLOT /MC_HFBRANCHING/BPLUS_pT
Title = $B^+$ transverse momentum
YLabel = Normalised to 1
XLabel = $p_\mathrm{T}$ [GeV]
END PLOT


BEGIN PLOT /MC_HFBRANCHING/B0S_pT
Title = $B^0_s$ transverse momentum
YLabel = Normalised to 1
XLabel = $p_\mathrm{T}$ [GeV]
END PLOT


BEGIN PLOT /MC_HFBRANCHING/D0_pT
Title = $D^0$ transverse momentum
YLabel = Normalised to 1
XLabel = $p_\mathrm{T}$ [GeV]
END PLOT


BEGIN PLOT /MC_HFBRANCHING/DPLUS_pT
Title = $D^+$ transverse momentum
YLabel = Normalised to 1
XLabel = $p_\mathrm{T}$ [GeV]
END PLOT


BEGIN PLOT /MC_HFBRANCHING/DSPLUS_pT
Title = $D^+_s$ transverse momentum
YLabel = Normalised to 1
XLabel = $p_\mathrm{T}$ [GeV]


BEGIN PLOT /MC_HFBRANCHING/LAMBDAB_pT
Title = $\Lambda^0_b$ transverse momentum
YLabel = Normalised to 1
XLabel = $p_\mathrm{T}$ [GeV]
END PLOT


BEGIN PLOT /MC_HFBRANCHING/LAMBDACPLUS_pT
Title = $\Lambda^+_c$ transverse momentum
YLabel = Normalised to 1
XLabel = $p_\mathrm{T}$ [GeV]
END PLOT


BEGIN PLOT /MC_HFBRANCHING/B0_lepton_pT_LAB
Title = Lepton $p_\mathrm{T}$ from $B^0$ deccay in LAB frame
YLabel = Normalised to 1
XLabel = lepton $p_\mathrm{T}$ [GeV]
END PLOT


BEGIN PLOT /MC_HFBRANCHING/BPLUS_lepton_pT_LAB
Title = Lepton $p_\mathrm{T}$ from $B^+$ deccay in LAB frame
YLabel = Normalised to 1
XLabel = lepton $p_\mathrm{T}$ [GeV]
END PLOT


BEGIN PLOT /MC_HFBRANCHING/B0S_lepton_pT_LAB
Title = Lepton $p_\mathrm{T}$ from $B^0_s$ deccay in LAB frame
YLabel = Normalised to 1
XLabel = lepton $p_\mathrm{T}$ [GeV]
END PLOT


BEGIN PLOT /MC_HFBRANCHING/D0_lepton_pT_LAB
Title = Lepton $p_\mathrm{T}$ from $D^0$ deccay in LAB frame
YLabel = Normalised to 1
XLabel = lepton $p_\mathrm{T}$ [GeV]
END PLOT


BEGIN PLOT /MC_HFBRANCHING/DPLUS_lepton_pT_LAB
Title = Lepton $p_\mathrm{T}$ from $D^+$ deccay in LAB frame
YLabel = Normalised to 1
XLabel = lepton $p_\mathrm{T}$ [GeV]
END PLOT


BEGIN PLOT /MC_HFBRANCHING/DSPLUS_lepton_pT_LAB
Title = Lepton $p_\mathrm{T}$ from $D^+_s$ deccay in LAB frame
YLabel = Normalised to 1
XLabel = lepton $p_\mathrm{T}$ [GeV]
END PLOT


BEGIN PLOT /MC_HFBRANCHING/LAMBDAB_lepton_pT_LAB
Title = Lepton $p_\mathrm{T}$ from $\Lambda^0_b$ deccay in LAB frame
YLabel = Normalised to 1
XLabel = lepton $p_\mathrm{T}$ [GeV]
END PLOT


BEGIN PLOT /MC_HFBRANCHING/LAMBDACPLUS_lepton_pT_LAB
Title = Lepton $p_\mathrm{T}$ from $\Lambda^+_c$ deccay in LAB frame
YLabel = Normalised to 1
XLabel = lepton $p_\mathrm{T}$ [GeV]
END PLOT


BEGIN PLOT /MC_HFBRANCHING/B0_e_pT
Title = Electron $p_\mathrm{T}$ in $B^0$ COM
YLabel = Normalised to 1
XLabel = e $p_\mathrm{T}$ [GeV]
END PLOT


BEGIN PLOT /MC_HFBRANCHING/B0_mu_pT
Title = Muon $p_\mathrm{T}$ in $B^0$ COM
YLabel = Normalised to 1
XLabel = $\mu$ $p_\mathrm{T}$ [GeV]
END PLOT


BEGIN PLOT /MC_HFBRANCHING/B0_tau_pT
Title = Tau $p_\mathrm{T}$ in $B^0$ COM
YLabel = Normalised to 1
XLabel = $\tau$ $p_\mathrm{T}$ [GeV]
END PLOT


BEGIN PLOT /MC_HFBRANCHING/B0_lepton_pT_COM
Title = Lepton $p_\mathrm{T}$ in $B^0$ COM
YLabel = Normalised to 1
XLabel = lepton $p_\mathrm{T}$ [GeV]
END PLOT


BEGIN PLOT /MC_HFBRANCHING/B0S_e_pT
Title = Electron $p_\mathrm{T}$ in $B^0_s$ COM
YLabel = Normalised to 1
XLabel = e $p_\mathrm{T}$ [GeV]
END PLOT


BEGIN PLOT /MC_HFBRANCHING/B0S_mu_pT
Title = Muon $p_\mathrm{T}$ in $B^0_s$ COM
YLabel = Normalised to 1
XLabel = $\mu$ $p_\mathrm{T}$ [GeV]
END PLOT


BEGIN PLOT /MC_HFBRANCHING/B0S_tau_pT
Title = Tau $p_\mathrm{T}$ in $B^0_s$ COM
YLabel = Normalised to 1
XLabel = $\tau$ $p_\mathrm{T}$ [GeV]
END PLOT


BEGIN PLOT /MC_HFBRANCHING/B0S_lepton_pT_COM
Title = Lepton $p_\mathrm{T}$ in $B^0_s$ COM
YLabel = Normalised to 1
XLabel = lepton $p_\mathrm{T}$ [GeV]
END PLOT


BEGIN PLOT /MC_HFBRANCHING/BPLUS_e_pT
Title = Electron $p_\mathrm{T}$ in $B^+$ COM
YLabel = Normalised to 1
XLabel = e $p_\mathrm{T}$ [GeV]
END PLOT


BEGIN PLOT /MC_HFBRANCHING/BPLUS_mu_pT
Title = Muon $p_\mathrm{T}$ in $B^+$ COM
YLabel = Normalised to 1
XLabel = $\mu$ $p_\mathrm{T}$ [GeV]
END PLOT


BEGIN PLOT /MC_HFBRANCHING/BPLUS_tau_pT
Title = Tau $p_\mathrm{T}$ in $B^+$ COM
YLabel = Normalised to 1
XLabel = $\tau$ $p_\mathrm{T}$ [GeV]
END PLOT


BEGIN PLOT /MC_HFBRANCHING/BPLUS_lepton_pT_COM
Title = Lepton $p_\mathrm{T}$ in $B^+$ COM
YLabel = Normalised to 1
XLabel = lepton $p_\mathrm{T}$ [GeV]
END PLOT


BEGIN PLOT /MC_HFBRANCHING/LAMBDAB_e_pT
Title = Electron $p_\mathrm{T}$ in $\Lambda^0_b$ COM
YLabel = Normalised to 1
XLabel = e $p_\mathrm{T}$ [GeV]
END PLOT


BEGIN PLOT /MC_HFBRANCHING/LAMBDAB_mu_pT
Title = Muon $p_\mathrm{T}$ in $\Lambda^0_b$ COM
YLabel = Normalised to 1
XLabel = $\mu$ $p_\mathrm{T}$ [GeV]
END PLOT


BEGIN PLOT /MC_HFBRANCHING/LAMBDAB_tau_pT
Title = Tau $p_\mathrm{T}$ in $\Lambda^0_b$ COM
YLabel = Normalised to 1
XLabel = $\tau$ $p_\mathrm{T}$ [GeV]
END PLOT


BEGIN PLOT /MC_HFBRANCHING/LAMBDAB_lepton_pT_COM
Title = Lepton $p_\mathrm{T}$ in $\Lambda^0_b$ COM
YLabel = Normalised to 1
XLabel = lepton $p_\mathrm{T}$ [GeV]
END PLOT


BEGIN PLOT /MC_HFBRANCHING/D0_e_pT
Title = Electron $p_\mathrm{T}$ in $D^0$ COM
YLabel = Normalised to 1
XLabel = e $p_\mathrm{T}$ [GeV]
END PLOT


BEGIN PLOT /MC_HFBRANCHING/D0_mu_pT
Title = Muon $p_\mathrm{T}$ in $D^0$ COM
YLabel = Normalised to 1
XLabel = $\mu$ $p_\mathrm{T}$ [GeV]
END PLOT


BEGIN PLOT /MC_HFBRANCHING/D0_lepton_pT_COM
Title = Lepton $p_\mathrm{T}$ in $D^0$ COM
YLabel = Normalised to 1
XLabel = lepton $p_\mathrm{T}$ [GeV]
END PLOT


BEGIN PLOT /MC_HFBRANCHING/DSPLUS_e_pT
Title = Electron $p_\mathrm{T}$ in $D^+_s$ COM
YLabel = Normalised to 1
XLabel = e $p_\mathrm{T}$ [GeV]
END PLOT


BEGIN PLOT /MC_HFBRANCHING/DSPLUS_mu_pT
Title = Muon $p_\mathrm{T}$ in $D^+_s$ COM
YLabel = Normalised to 1
XLabel = $\mu$ $p_\mathrm{T}$ [GeV]
END PLOT


BEGIN PLOT /MC_HFBRANCHING/DSPLUS_lepton_pT_COM
Title = Lepton $p_\mathrm{T}$ in $D^+_s$ COM
YLabel = Normalised to 1
XLabel = lepton $p_\mathrm{T}$ [GeV]
END PLOT


BEGIN PLOT /MC_HFBRANCHING/DPLUS_e_pT
Title = Electron $p_\mathrm{T}$ in $D^+$ COM
YLabel = Normalised to 1
XLabel = e $p_\mathrm{T}$ [GeV]
END PLOT


BEGIN PLOT /MC_HFBRANCHING/DPLUS_mu_pT
Title = Muon $p_\mathrm{T}$ in $D^+$ COM
YLabel = Normalised to 1
XLabel = $\mu$ $p_\mathrm{T}$ [GeV]
END PLOT


BEGIN PLOT /MC_HFBRANCHING/DPLUS_lepton_pT_COM
Title = Lepton $p_\mathrm{T}$ in $D^+$ COM
YLabel = Normalised to 1
XLabel = lepton $p_\mathrm{T}$ [GeV]
END PLOT


BEGIN PLOT /MC_HFBRANCHING/LAMBDACPLUS_e_pT
Title = Electron $p_\mathrm{T}$ in $\Lambda^+_c$ COM
YLabel = Normalised to 1
XLabel = e $p_\mathrm{T}$ [GeV]
END PLOT


BEGIN PLOT /MC_HFBRANCHING/LAMBDACPLUS_mu_pT
Title = Muon $p_\mathrm{T}$ in $\Lambda^+_c$ COM
YLabel = Normalised to 1
XLabel = $\mu$ $p_\mathrm{T}$ [GeV]
END PLOT


BEGIN PLOT /MC_HFBRANCHING/LAMBDACPLUS_lepton_pT_COM
Title = Lepton $p_\mathrm{T}$ in $\Lambda^+_c$ COM
YLabel = Normalised to 1
XLabel = lepton $p_\mathrm{T}$ [GeV]
END PLOT
