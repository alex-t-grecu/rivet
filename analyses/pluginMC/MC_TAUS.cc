// -*- C++ -*-
#include "Rivet/Analyses/MC_PARTICLES_BASE.hh"
#include "Rivet/Projections/TauFinder.hh"

namespace Rivet {


  /// @brief MC validation analysis for taus
  class MC_TAUS : public MC_PARTICLES_BASE {
  public:

    /// Constructor
    MC_TAUS()
      : MC_PARTICLES_BASE("MC_TAUS", 2, "tau")
    {    }


    /// Book projections and histograms
    void init() {
      TauFinder taus(TauDecay::ANY);
      declare(taus, "Taus");

      MC_PARTICLES_BASE::init();
    }


    /// Per-event analysis
    void analyze(const Event& event) {
      const Particles taus = apply<TauFinder>(event, "Taus").particlesByPt(0.5*GeV);
      MC_PARTICLES_BASE::_analyze(event, taus);
    }


    /// Normalisations etc.
    void finalize() {
      MC_PARTICLES_BASE::finalize();
    }

  };


  RIVET_DECLARE_PLUGIN(MC_TAUS);

}
