BEGIN PLOT /KLOE_2013_I1199266/d01-x01-y01
Title=Cross section for $e^+e^-\to e^+e^-\eta$
XLabel=$\sqrt{s}$ [GeV]
YLabel=$\sigma$ [pb]
LogY=0
END PLOT
