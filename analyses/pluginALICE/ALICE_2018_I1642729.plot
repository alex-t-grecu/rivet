BEGIN PLOT /ALICE_2018_I1642729/d01-x01-y01
Title=Inclusive $\Xi^0_c$ differential cross section ($|y|<0.5$)->
XLabel=$p_\perp$ [GeV]
YLabel=$\mathrm{Br}(\Xi_c^0\to\ell\nu X)\text{d}\sigma/\text{d}p_\perp$ [$\mu$b/GeV]
END PLOT
BEGIN PLOT /ALICE_2018_I1642729/d02-x01-y01
Title=Ratio of inclusive $\Xi^0_c$ to $D^0$ ($|y|<0.5$)
XLabel=$p_\perp$ [GeV]
YLabel=$\mathrm{Br}(\Xi_c^0\to\ell\nu X)\sigma(\Xi_c^0)/\sigma(D^0)$
END PLOT
