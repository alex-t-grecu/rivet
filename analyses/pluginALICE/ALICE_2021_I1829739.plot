BEGIN PLOT /ALICE_2021_I1829739/d01-x01-y01
Title=Prompt $\Lambda_c^+$ differential cross section ($|y|<0.5$)
XLabel=$p_\perp$ [GeV]
YLabel=$\text{d}\sigma/\text{d}p_\perp$ [$\mu$b/GeV]
END PLOT

BEGIN PLOT /ALICE_2021_I1829739/d04-x01-y01
Title=Prompt $\Lambda_c^+$ tp $D^0$ ratio ($|y|<1$)
XLabel=$p_\perp$ [GeV]
YLabel=$\sigma(\lambda_c^+)/\sigma(D^0)$
END PLOT

BEGIN PLOT /ALICE_2021_I1829739/d07-x01-y01
Title=Prompt $\Lambda_c^+$ cross section ($|y|<0.5$, $1<p_\perp<12$\,GeV)
YLabel=$\sigma(\lambda_c^+)$
XCustomMajorTicks=1 pp 2 pPb
END PLOT
BEGIN PLOT /ALICE_2021_I1829739/d07-x01-y02
Title=Prompt $\Lambda_c^+$ cross section ($|y|<0.5$)
YLabel=$\sigma(\lambda_c^+)$
XCustomMajorTicks=1 pp 2 pPb
END PLOT
BEGIN PLOT /ALICE_2021_I1829739/d08-x01-y01
Title=Prompt $\Lambda_c^+$ tp $D^0$ ratio ($|y|<1$)
XLabel=
YLabel=$\sigma(\lambda_c^+)/\sigma(D^0)$
XCustomMajorTicks=1 pp 2 pPb
END PLOT
