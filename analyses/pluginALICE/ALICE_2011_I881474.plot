# BEGIN PLOT /ALICE_2011_I881474/d0
XLabel=$p_\perp$ $[\mathrm{GeV}/c]$
YLabel=$1/N_\mathrm{evt} \, d^{2}N/dy\,dp_\perp$ $[(\mathrm{GeV}/c)^{-1}]$
# END PLOT


# BEGIN PLOT /ALICE_2011_I881474/d01-x01-y01
Title=$K^0_s$ transverse momentum, $|y|<0.75$, $\sqrt{s}=0.9\,\mathrm{TeV}$ (INEL)
# END PLOT

# BEGIN PLOT /ALICE_2011_I881474/d02-x01-y01
Title=$\Lambda$ transverse momentum, $|y|<0.75$, $\sqrt{s}=0.9\,\mathrm{TeV}$ (INEL)
# END PLOT

# BEGIN PLOT /ALICE_2011_I881474/d03-x01-y01
Title=$\bar \Lambda$ transverse momentum, $|y|<0.75$, $\sqrt{s}=0.9\,\mathrm{TeV}$ (INEL)
# END PLOT

# BEGIN PLOT /ALICE_2011_I881474/d04-x01-y01
Title=$\Xi$ transverse momentum, $|y|<0.8$, $\sqrt{s}=0.9\,\mathrm{TeV}$ (INEL)
# END PLOT

# BEGIN PLOT /ALICE_2011_I881474/d05-x01-y01
Title=$\phi(1020)$ transverse momentum, $|y|<0.6$, $\sqrt{s}=0.9\,\mathrm{TeV}$ (INEL)
# END PLOT

# BEGIN PLOT /ALICE_2011_I881474/d06-x01-y01
Title=$\Lambda / K^0_s$ ratio, $|y|<0.75$, $\sqrt{s}=0.9\,\mathrm{TeV}$ (INEL)
YLabel=$(\sigma(\Lambda)+\sigma(\bar \Lambda))/2 \sigma(K^0_s)$
LogY=0
# END PLOT
