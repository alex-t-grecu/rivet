BEGIN PLOT /BELLE_2006_I688850/d01-x01-y01
Title=$\pi^+\pi^-$ mass in in $B^0\to\rho^0\pi^0$
XLabel=$m_{\pi^+\pi^-}$ [GeV]
YLabel=$1/\Gamma\text{d}\Gamma/\text{d}m_{\pi^+\pi^-}$ [$\text{GeV}^{-1}$]
LogY=0
END PLOT
BEGIN PLOT /BELLE_2006_I688850/d01-x01-y02
Title=Helicity angle in $B^0\to\rho^0\pi^0$
XLabel=$\cos\theta$
YLabel=$1/\Gamma\text{d}\Gamma/\text{d}\cos\theta$
LogY=0
END PLOT
