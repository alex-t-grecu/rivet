Name: BELLE_2005_I667959
Year: 2005
Summary: Mass distributions and helicity angles in $B^+\to K^+\pi^+\pi^-$ and $B^+\to K^+K^+K^-$
Experiment: BELLE
Collider: KEKB
InspireID: 667959
Status: VALIDATED NOHEPDATA
Reentrant: true
Authors:
 - Peter Richardson <peter.richardson@durham.ac.uk>
References:
 - Phys.Rev.D 71 (2005) 092003
RunInfo: Any process producing B+ mesons, originally Upsilon(4S) decays
Description:
  'Measurement of Mass distributions and helicity angles in $B^+\to K^+\pi^+\pi^-$ and $B^+\to K^+K^+K^-$ decays. The data were read from the figures in the paper and the backgrounds given subtracted.'
ValidationInfo:
  'Herwig7 event using EvtGen for B decays'
#ReleaseTests:
# - $A my-hepmc-prefix :MODE=some_rivet_flag
Keywords: []
BibKey: Belle:2004drb
BibTeX: '@article{Belle:2004drb,
    author = "Garmash, A. and others",
    collaboration = "Belle",
    title = "{Dalitz analysis of the three-body charmless decays B+ ---\ensuremath{>} K+ pi+ pi- and B+ ---\ensuremath{>} K+ K+ K-}",
    eprint = "hep-ex/0412066",
    archivePrefix = "arXiv",
    reportNumber = "BELLE-PREPRINT-2004-40, KEK-PREPRINT-2004-81",
    doi = "10.1103/PhysRevD.71.092003",
    journal = "Phys. Rev. D",
    volume = "71",
    pages = "092003",
    year = "2005"
}
'
