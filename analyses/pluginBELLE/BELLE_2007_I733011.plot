BEGIN PLOT /BELLE_2007_I733011/d0[2.3]
XLabel=$\cos\theta_h$
YLabel=$1/\Gamma\text{d}\Gamma/\text{d}\cos\theta_h$
LogY=0

BEGIN PLOT /BELLE_2007_I733011/d01-x01-y01
Title=$D^0\pi^+$ mass in $\bar{B}^0\to D^0\pi^+\pi^-$ ($\cos\theta_h>0$)
XLabel=$m_{D^0\pi^+}$ [GeV]
YLabel=$1/\Gamma\text{d}\Gamma/\text{d}m_{D^0\pi^+}$ [$\text{GeV}^{-1}$]
LogY=0
END PLOT
BEGIN PLOT /BELLE_2007_I733011/d01-x01-y02
Title=$\pi^+\pi^-$ mass in $\bar{B}^0\to D^0\pi^+\pi^-$ ($\cos\theta_h>0$)
XLabel=$m_{\pi^+\pi^-}$ [GeV]
YLabel=$1/\Gamma\text{d}\Gamma/\text{d}m_{\pi^+\pi^-}$ [$\text{GeV}^{-1}$]
LogY=0
END PLOT

BEGIN PLOT /BELLE_2007_I733011/d02-x01-y01
Title=Helicity angle for $D\pi$ in $\bar{B}^0\to D^0\pi^+\pi^-$ ($D^*_2$ region)
END PLOT
BEGIN PLOT /BELLE_2007_I733011/d02-x01-y02
Title=Helicity angle for $D\pi$ in $\bar{B}^0\to D^0\pi^+\pi^-$ ($D^*_0$ region)
END PLOT

BEGIN PLOT /BELLE_2007_I733011/d03-x01-y01
Title=Helicity angle for $\pi\pi$ in $\bar{B}^0\to D^0\pi^+\pi^-$ ($\rho$ region)
END PLOT
BEGIN PLOT /BELLE_2007_I733011/d03-x01-y02
Title=Helicity angle for $\pi\pi$ in $\bar{B}^0\to D^0\pi^+\pi^-$ ($f_2$ region)
END PLOT
BEGIN PLOT /BELLE_2007_I733011/d03-x01-y03
Title=Helicity angle for $\pi\pi$ in $\bar{B}^0\to D^0\pi^+\pi^-$ ($\sigma$ region)
END PLOT
