BEGIN PLOT /BELLE_2016_I1283183/d01-x01-y0
YLabel=$A_{FB}$
XLabel=$\sqrt{s}$
LogY=0
END PLOT

BEGIN PLOT /BELLE_2016_I1283183/d01-x01-y01
Title=Forward-Backward asymmetry in $B\to X_s\ell^+\ell^-$, $0.2<q^2<4.3\,\mathrm{GeV}^2$
END PLOT
BEGIN PLOT /BELLE_2016_I1283183/d01-x01-y02
Title=Forward-Backward asymmetry in $B\to X_s\ell^+\ell^-$, $4.3<q^2<8.1\,\mathrm{GeV}^2$
END PLOT
BEGIN PLOT /BELLE_2016_I1283183/d01-x01-y03
Title=Forward-Backward asymmetry in $B\to X_s\ell^+\ell^-$, $10.2<q^2<12.5\,\mathrm{GeV}^2$
END PLOT
BEGIN PLOT /BELLE_2016_I1283183/d01-x01-y04
Title=Forward-Backward asymmetry in $B\to X_s\ell^+\ell^-$, $14.3<q^2<25\,\mathrm{GeV}^2$
END PLOT
