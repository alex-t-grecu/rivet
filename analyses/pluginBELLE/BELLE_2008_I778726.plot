BEGIN PLOT /BELLE_2008_I778726/d01-x01-y01
Title=Branching ratio w.r.t $p\bar{p}$ mass for $B^0\to p\bar{p}K^{*0}$
XLabel=$m_{p\bar{p}}$ [GeV]
YLabel=$\text{d}\mathcal{B}/\text{d}m_{p\bar{p}}\times10^6$ [$\text{GeV}^{-1}$]
LogY=0
END PLOT
BEGIN PLOT /BELLE_2008_I778726/d01-x01-y02
Title=Branching ratio w.r.t $p\bar{p}$ mass for $B^+\to p\bar{p}K^{*+}$
XLabel=$m_{p\bar{p}}$ [GeV]
YLabel=$\text{d}\mathcal{B}/\text{d}m_{p\bar{p}}\times10^6$ [$\text{GeV}^{-1}$]
LogY=0
END PLOT

BEGIN PLOT /BELLE_2008_I778726/d02-x01-y01
Title=Kaon helicty angle for  $B^0\to p\bar{p}K^{*0}$ with $m_{p\bar{p}}<2.85\,$GeV
XLabel=$\cos\theta_K$ 
YLabel=$1/\Gamma\text{d}\Gamma\text{d}\cos\theta_K$ 
LogY=0
END PLOT
BEGIN PLOT /BELLE_2008_I778726/d02-x01-y02
Title=Kaon helicty angle for  $B^+\to p\bar{p}K^{*+}$ with $m_{p\bar{p}}<2.85\,$GeV
XLabel=$\cos\theta_K$ 
YLabel=$1/\Gamma\text{d}\Gamma\text{d}\cos\theta_K$ 
LogY=0
END PLOT

BEGIN PLOT /BELLE_2008_I778726/d03-x01-y01
Title=Proton,k helicty angle for  $B^0\to p\bar{p}K^{*0}$ with $m_{p\bar{p}}<2.85\,$GeV
XLabel=$\cos\theta_p$ 
YLabel=$1/\Gamma\text{d}\Gamma\text{d}\cos\theta_p$ 
LogY=0
END PLOT
BEGIN PLOT /BELLE_2008_I778726/d03-x01-y02
Title=Proton helicty angle for  $B^+\to p\bar{p}K^{*+}$ with $m_{p\bar{p}}<2.85\,$GeV
XLabel=$\cos\theta_p$ 
YLabel=$1/\Gamma\text{d}\Gamma\text{d}\cos\theta_p$ 
LogY=0
END PLOT
