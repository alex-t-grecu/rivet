BEGIN PLOT /BELLE_2018_I1700174/d01-x01-y01
Title=Cross Section for $e^+e^-\to\gamma\chi_{c1}$
XLabel=$\sqrt{s}$ [GeV]
YLabel=$\sigma$ [fb]
LogY=0
END PLOT
