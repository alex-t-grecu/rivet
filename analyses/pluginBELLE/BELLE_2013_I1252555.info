Name: BELLE_2013_I1252555
Year: 2013
Summary: Measurement of the cross section for $e^+e^-\to \omega\pi^0$, $K^*\bar{K}$ and $K_2^*(1430)\bar{K}$ for $\sqrt{s}\sim10.6$ GeV
Experiment: BELLE
Collider: KEKB
InspireID: 1252555
Status: VALIDATED NOHEPDATA
Reentrant: true
Authors:
 - Peter Richardson <peter.richardson@dur.ac.uk>
References:
 - Phys.Rev.D 88 (2013) 5, 052019
RunInfo: e+ e- to hadrons, pi0 and KS0 set stable
Beams: [e+, e-]
Energies: [10.52,10.58,10.876]
Options:
 - ENERGY=*
Description:
  'Measurement of the cross section for $e^+e^-\to \omega\pi^0$, $K^*\bar{K}$ and $K_2^*(1430)\bar{K}$ for $\sqrt{s}\sim10.86$ GeV.
   Beam energy must be specified as analysis option "ENERGY" when rivet-merging samples.'
ValidationInfo:
  'Herwig7 events'
#ReleaseTests:
# - $A my-hepmc-prefix :MODE=some_rivet_flag
Keywords: []
BibKey: Belle:2013hkg
BibTeX: '@article{Belle:2013hkg,
    author = "Shen, C. P. and others",
    collaboration = "Belle",
    title = "{Measurement of $e^+ e^- \to \omega \pi^0$, $K^{\ast}(892)\bar{K}$ and $K_2^{\ast}(1430)\bar{K}$ at $\sqrt{s}$ near 10.6 GeV}",
    eprint = "1309.0575",
    archivePrefix = "arXiv",
    primaryClass = "hep-ex",
    reportNumber = "BELLE-PREPRINT-2013-19, KEK-PREPRINT-2013-30",
    doi = "10.1103/PhysRevD.88.052019",
    journal = "Phys. Rev. D",
    volume = "88",
    number = "5",
    pages = "052019",
    year = "2013"
}
'
