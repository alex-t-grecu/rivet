BEGIN PLOT /BELLE_2013_I1245023/
LogY=0
YLabel=$\text{d}\sigma/\text{d}|\cos\theta|$ [nb]
XLabel=$|\cos\theta|$
END PLOT

BEGIN PLOT /BELLE_2013_I1245023/d01-x01-y01
Title=Cross section for $\gamma\gamma\to K^0_SK^0_S$ with $|\cos\theta|<0.8$
XLabel=$\sqrt{s}$
YLabel=$\sigma(\gamma\gamma\to K^0_SK^0_S)$ [nb]
LogY=1
END PLOT
BEGIN PLOT /BELLE_2013_I1245023/d01-x01-y02
Title=Cross section for $\gamma\gamma\to K^0_SK^0_S$ with $|\cos\theta|<0.6$
XLabel=$\sqrt{s}$
YLabel=$\sigma(\gamma\gamma\to K^0_SK^0_S)$ [nb]
LogY=1
END PLOT

BEGIN PLOT /BELLE_2013_I1245023/d02-x01-y01
Title=Cross section for $\gamma\gamma\to K^0_SK^0_S$ with $1.10<\sqrt{s}<1.11$ GeV
END PLOT
BEGIN PLOT /BELLE_2013_I1245023/d02-x01-y02
Title=Cross section for $\gamma\gamma\to K^0_SK^0_S$ with $1.11<\sqrt{s}<1.12$ GeV
END PLOT
BEGIN PLOT /BELLE_2013_I1245023/d02-x01-y03
Title=Cross section for $\gamma\gamma\to K^0_SK^0_S$ with $1.12<\sqrt{s}<1.13$ GeV
END PLOT
BEGIN PLOT /BELLE_2013_I1245023/d03-x01-y01
Title=Cross section for $\gamma\gamma\to K^0_SK^0_S$ with $1.13<\sqrt{s}<1.14$ GeV
END PLOT
BEGIN PLOT /BELLE_2013_I1245023/d03-x01-y02
Title=Cross section for $\gamma\gamma\to K^0_SK^0_S$ with $1.14<\sqrt{s}<1.15$ GeV
END PLOT
BEGIN PLOT /BELLE_2013_I1245023/d03-x01-y03
Title=Cross section for $\gamma\gamma\to K^0_SK^0_S$ with $1.15<\sqrt{s}<1.16$ GeV
END PLOT
BEGIN PLOT /BELLE_2013_I1245023/d04-x01-y01
Title=Cross section for $\gamma\gamma\to K^0_SK^0_S$ with $1.16<\sqrt{s}<1.17$ GeV
END PLOT
BEGIN PLOT /BELLE_2013_I1245023/d04-x01-y02
Title=Cross section for $\gamma\gamma\to K^0_SK^0_S$ with $1.17<\sqrt{s}<1.18$ GeV
END PLOT
BEGIN PLOT /BELLE_2013_I1245023/d04-x01-y03
Title=Cross section for $\gamma\gamma\to K^0_SK^0_S$ with $1.18<\sqrt{s}<1.19$ GeV
END PLOT
BEGIN PLOT /BELLE_2013_I1245023/d05-x01-y01
Title=Cross section for $\gamma\gamma\to K^0_SK^0_S$ with $1.19<\sqrt{s}<1.20$ GeV
END PLOT
BEGIN PLOT /BELLE_2013_I1245023/d05-x01-y02
Title=Cross section for $\gamma\gamma\to K^0_SK^0_S$ with $1.20<\sqrt{s}<1.21$ GeV
END PLOT
BEGIN PLOT /BELLE_2013_I1245023/d05-x01-y03
Title=Cross section for $\gamma\gamma\to K^0_SK^0_S$ with $1.21<\sqrt{s}<1.22$ GeV
END PLOT
BEGIN PLOT /BELLE_2013_I1245023/d06-x01-y01
Title=Cross section for $\gamma\gamma\to K^0_SK^0_S$ with $1.22<\sqrt{s}<1.23$ GeV
END PLOT
BEGIN PLOT /BELLE_2013_I1245023/d06-x01-y02
Title=Cross section for $\gamma\gamma\to K^0_SK^0_S$ with $1.23<\sqrt{s}<1.24$ GeV
END PLOT
BEGIN PLOT /BELLE_2013_I1245023/d06-x01-y03
Title=Cross section for $\gamma\gamma\to K^0_SK^0_S$ with $1.24<\sqrt{s}<1.25$ GeV
END PLOT
BEGIN PLOT /BELLE_2013_I1245023/d07-x01-y01
Title=Cross section for $\gamma\gamma\to K^0_SK^0_S$ with $1.25<\sqrt{s}<1.26$ GeV
END PLOT
BEGIN PLOT /BELLE_2013_I1245023/d07-x01-y02
Title=Cross section for $\gamma\gamma\to K^0_SK^0_S$ with $1.26<\sqrt{s}<1.27$ GeV
END PLOT
BEGIN PLOT /BELLE_2013_I1245023/d07-x01-y03
Title=Cross section for $\gamma\gamma\to K^0_SK^0_S$ with $1.27<\sqrt{s}<1.28$ GeV
END PLOT
BEGIN PLOT /BELLE_2013_I1245023/d08-x01-y01
Title=Cross section for $\gamma\gamma\to K^0_SK^0_S$ with $1.28<\sqrt{s}<1.29$ GeV
END PLOT
BEGIN PLOT /BELLE_2013_I1245023/d08-x01-y02
Title=Cross section for $\gamma\gamma\to K^0_SK^0_S$ with $1.29<\sqrt{s}<1.30$ GeV
END PLOT
BEGIN PLOT /BELLE_2013_I1245023/d08-x01-y03
Title=Cross section for $\gamma\gamma\to K^0_SK^0_S$ with $1.30<\sqrt{s}<1.31$ GeV
END PLOT
BEGIN PLOT /BELLE_2013_I1245023/d09-x01-y01
Title=Cross section for $\gamma\gamma\to K^0_SK^0_S$ with $1.31<\sqrt{s}<1.32$ GeV
END PLOT
BEGIN PLOT /BELLE_2013_I1245023/d09-x01-y02
Title=Cross section for $\gamma\gamma\to K^0_SK^0_S$ with $1.32<\sqrt{s}<1.33$ GeV
END PLOT
BEGIN PLOT /BELLE_2013_I1245023/d09-x01-y03
Title=Cross section for $\gamma\gamma\to K^0_SK^0_S$ with $1.33<\sqrt{s}<1.34$ GeV
END PLOT
BEGIN PLOT /BELLE_2013_I1245023/d10-x01-y01
Title=Cross section for $\gamma\gamma\to K^0_SK^0_S$ with $1.34<\sqrt{s}<1.35$ GeV
END PLOT
BEGIN PLOT /BELLE_2013_I1245023/d10-x01-y02
Title=Cross section for $\gamma\gamma\to K^0_SK^0_S$ with $1.35<\sqrt{s}<1.36$ GeV
END PLOT
BEGIN PLOT /BELLE_2013_I1245023/d10-x01-y03
Title=Cross section for $\gamma\gamma\to K^0_SK^0_S$ with $1.36<\sqrt{s}<1.37$ GeV
END PLOT
BEGIN PLOT /BELLE_2013_I1245023/d11-x01-y01
Title=Cross section for $\gamma\gamma\to K^0_SK^0_S$ with $1.37<\sqrt{s}<1.38$ GeV
END PLOT
BEGIN PLOT /BELLE_2013_I1245023/d11-x01-y02
Title=Cross section for $\gamma\gamma\to K^0_SK^0_S$ with $1.38<\sqrt{s}<1.39$ GeV
END PLOT
BEGIN PLOT /BELLE_2013_I1245023/d11-x01-y03
Title=Cross section for $\gamma\gamma\to K^0_SK^0_S$ with $1.39<\sqrt{s}<1.40$ GeV
END PLOT
BEGIN PLOT /BELLE_2013_I1245023/d12-x01-y01
Title=Cross section for $\gamma\gamma\to K^0_SK^0_S$ with $1.40<\sqrt{s}<1.41$ GeV
END PLOT
BEGIN PLOT /BELLE_2013_I1245023/d12-x01-y02
Title=Cross section for $\gamma\gamma\to K^0_SK^0_S$ with $1.41<\sqrt{s}<1.42$ GeV
END PLOT
BEGIN PLOT /BELLE_2013_I1245023/d12-x01-y03
Title=Cross section for $\gamma\gamma\to K^0_SK^0_S$ with $1.42<\sqrt{s}<1.43$ GeV
END PLOT
BEGIN PLOT /BELLE_2013_I1245023/d13-x01-y01
Title=Cross section for $\gamma\gamma\to K^0_SK^0_S$ with $1.43<\sqrt{s}<1.44$ GeV
END PLOT
BEGIN PLOT /BELLE_2013_I1245023/d13-x01-y02
Title=Cross section for $\gamma\gamma\to K^0_SK^0_S$ with $1.44<\sqrt{s}<1.45$ GeV
END PLOT
BEGIN PLOT /BELLE_2013_I1245023/d13-x01-y03
Title=Cross section for $\gamma\gamma\to K^0_SK^0_S$ with $1.45<\sqrt{s}<1.46$ GeV
END PLOT
BEGIN PLOT /BELLE_2013_I1245023/d14-x01-y01
Title=Cross section for $\gamma\gamma\to K^0_SK^0_S$ with $1.46<\sqrt{s}<1.47$ GeV
END PLOT
BEGIN PLOT /BELLE_2013_I1245023/d14-x01-y02
Title=Cross section for $\gamma\gamma\to K^0_SK^0_S$ with $1.47<\sqrt{s}<1.48$ GeV
END PLOT
BEGIN PLOT /BELLE_2013_I1245023/d14-x01-y03
Title=Cross section for $\gamma\gamma\to K^0_SK^0_S$ with $1.48<\sqrt{s}<1.49$ GeV
END PLOT
BEGIN PLOT /BELLE_2013_I1245023/d15-x01-y01
Title=Cross section for $\gamma\gamma\to K^0_SK^0_S$ with $1.49<\sqrt{s}<1.50$ GeV
END PLOT
BEGIN PLOT /BELLE_2013_I1245023/d15-x01-y02
Title=Cross section for $\gamma\gamma\to K^0_SK^0_S$ with $1.50<\sqrt{s}<1.51$ GeV
END PLOT
BEGIN PLOT /BELLE_2013_I1245023/d15-x01-y03
Title=Cross section for $\gamma\gamma\to K^0_SK^0_S$ with $1.51<\sqrt{s}<1.52$ GeV
END PLOT
BEGIN PLOT /BELLE_2013_I1245023/d16-x01-y01
Title=Cross section for $\gamma\gamma\to K^0_SK^0_S$ with $1.52<\sqrt{s}<1.53$ GeV
END PLOT
BEGIN PLOT /BELLE_2013_I1245023/d16-x01-y02
Title=Cross section for $\gamma\gamma\to K^0_SK^0_S$ with $1.53<\sqrt{s}<1.54$ GeV
END PLOT
BEGIN PLOT /BELLE_2013_I1245023/d16-x01-y03
Title=Cross section for $\gamma\gamma\to K^0_SK^0_S$ with $1.54<\sqrt{s}<1.55$ GeV
END PLOT
BEGIN PLOT /BELLE_2013_I1245023/d17-x01-y01
Title=Cross section for $\gamma\gamma\to K^0_SK^0_S$ with $1.55<\sqrt{s}<1.56$ GeV
END PLOT
BEGIN PLOT /BELLE_2013_I1245023/d17-x01-y02
Title=Cross section for $\gamma\gamma\to K^0_SK^0_S$ with $1.56<\sqrt{s}<1.57$ GeV
END PLOT
BEGIN PLOT /BELLE_2013_I1245023/d17-x01-y03
Title=Cross section for $\gamma\gamma\to K^0_SK^0_S$ with $1.57<\sqrt{s}<1.58$ GeV
END PLOT
BEGIN PLOT /BELLE_2013_I1245023/d18-x01-y01
Title=Cross section for $\gamma\gamma\to K^0_SK^0_S$ with $1.58<\sqrt{s}<1.59$ GeV
END PLOT
BEGIN PLOT /BELLE_2013_I1245023/d18-x01-y02
Title=Cross section for $\gamma\gamma\to K^0_SK^0_S$ with $1.59<\sqrt{s}<1.60$ GeV
END PLOT
BEGIN PLOT /BELLE_2013_I1245023/d18-x01-y03
Title=Cross section for $\gamma\gamma\to K^0_SK^0_S$ with $1.60<\sqrt{s}<1.61$ GeV
END PLOT
BEGIN PLOT /BELLE_2013_I1245023/d19-x01-y01
Title=Cross section for $\gamma\gamma\to K^0_SK^0_S$ with $1.61<\sqrt{s}<1.62$ GeV
END PLOT
BEGIN PLOT /BELLE_2013_I1245023/d19-x01-y02
Title=Cross section for $\gamma\gamma\to K^0_SK^0_S$ with $1.62<\sqrt{s}<1.63$ GeV
END PLOT
BEGIN PLOT /BELLE_2013_I1245023/d19-x01-y03
Title=Cross section for $\gamma\gamma\to K^0_SK^0_S$ with $1.63<\sqrt{s}<1.64$ GeV
END PLOT
BEGIN PLOT /BELLE_2013_I1245023/d20-x01-y01
Title=Cross section for $\gamma\gamma\to K^0_SK^0_S$ with $1.64<\sqrt{s}<1.65$ GeV
END PLOT
BEGIN PLOT /BELLE_2013_I1245023/d20-x01-y02
Title=Cross section for $\gamma\gamma\to K^0_SK^0_S$ with $1.65<\sqrt{s}<1.66$ GeV
END PLOT
BEGIN PLOT /BELLE_2013_I1245023/d20-x01-y03
Title=Cross section for $\gamma\gamma\to K^0_SK^0_S$ with $1.66<\sqrt{s}<1.67$ GeV
END PLOT
BEGIN PLOT /BELLE_2013_I1245023/d21-x01-y01
Title=Cross section for $\gamma\gamma\to K^0_SK^0_S$ with $1.67<\sqrt{s}<1.68$ GeV
END PLOT
BEGIN PLOT /BELLE_2013_I1245023/d21-x01-y02
Title=Cross section for $\gamma\gamma\to K^0_SK^0_S$ with $1.68<\sqrt{s}<1.69$ GeV
END PLOT
BEGIN PLOT /BELLE_2013_I1245023/d21-x01-y03
Title=Cross section for $\gamma\gamma\to K^0_SK^0_S$ with $1.69<\sqrt{s}<1.70$ GeV
END PLOT
BEGIN PLOT /BELLE_2013_I1245023/d22-x01-y01
Title=Cross section for $\gamma\gamma\to K^0_SK^0_S$ with $1.70<\sqrt{s}<1.71$ GeV
END PLOT
BEGIN PLOT /BELLE_2013_I1245023/d22-x01-y02
Title=Cross section for $\gamma\gamma\to K^0_SK^0_S$ with $1.71<\sqrt{s}<1.72$ GeV
END PLOT
BEGIN PLOT /BELLE_2013_I1245023/d22-x01-y03
Title=Cross section for $\gamma\gamma\to K^0_SK^0_S$ with $1.72<\sqrt{s}<1.73$ GeV
END PLOT
BEGIN PLOT /BELLE_2013_I1245023/d23-x01-y01
Title=Cross section for $\gamma\gamma\to K^0_SK^0_S$ with $1.73<\sqrt{s}<1.74$ GeV
END PLOT
BEGIN PLOT /BELLE_2013_I1245023/d23-x01-y02
Title=Cross section for $\gamma\gamma\to K^0_SK^0_S$ with $1.74<\sqrt{s}<1.75$ GeV
END PLOT
BEGIN PLOT /BELLE_2013_I1245023/d23-x01-y03
Title=Cross section for $\gamma\gamma\to K^0_SK^0_S$ with $1.75<\sqrt{s}<1.76$ GeV
END PLOT
BEGIN PLOT /BELLE_2013_I1245023/d24-x01-y01
Title=Cross section for $\gamma\gamma\to K^0_SK^0_S$ with $1.76<\sqrt{s}<1.77$ GeV
END PLOT
BEGIN PLOT /BELLE_2013_I1245023/d24-x01-y02
Title=Cross section for $\gamma\gamma\to K^0_SK^0_S$ with $1.77<\sqrt{s}<1.78$ GeV
END PLOT
BEGIN PLOT /BELLE_2013_I1245023/d24-x01-y03
Title=Cross section for $\gamma\gamma\to K^0_SK^0_S$ with $1.78<\sqrt{s}<1.79$ GeV
END PLOT
BEGIN PLOT /BELLE_2013_I1245023/d25-x01-y01
Title=Cross section for $\gamma\gamma\to K^0_SK^0_S$ with $1.79<\sqrt{s}<1.80$ GeV
END PLOT
BEGIN PLOT /BELLE_2013_I1245023/d25-x01-y02
Title=Cross section for $\gamma\gamma\to K^0_SK^0_S$ with $1.80<\sqrt{s}<1.81$ GeV
END PLOT
BEGIN PLOT /BELLE_2013_I1245023/d25-x01-y03
Title=Cross section for $\gamma\gamma\to K^0_SK^0_S$ with $1.81<\sqrt{s}<1.82$ GeV
END PLOT
BEGIN PLOT /BELLE_2013_I1245023/d26-x01-y01
Title=Cross section for $\gamma\gamma\to K^0_SK^0_S$ with $1.82<\sqrt{s}<1.83$ GeV
END PLOT
BEGIN PLOT /BELLE_2013_I1245023/d26-x01-y02
Title=Cross section for $\gamma\gamma\to K^0_SK^0_S$ with $1.83<\sqrt{s}<1.84$ GeV
END PLOT
BEGIN PLOT /BELLE_2013_I1245023/d26-x01-y03
Title=Cross section for $\gamma\gamma\to K^0_SK^0_S$ with $1.84<\sqrt{s}<1.85$ GeV
END PLOT
BEGIN PLOT /BELLE_2013_I1245023/d27-x01-y01
Title=Cross section for $\gamma\gamma\to K^0_SK^0_S$ with $1.85<\sqrt{s}<1.86$ GeV
END PLOT
BEGIN PLOT /BELLE_2013_I1245023/d27-x01-y02
Title=Cross section for $\gamma\gamma\to K^0_SK^0_S$ with $1.86<\sqrt{s}<1.87$ GeV
END PLOT
BEGIN PLOT /BELLE_2013_I1245023/d27-x01-y03
Title=Cross section for $\gamma\gamma\to K^0_SK^0_S$ with $1.87<\sqrt{s}<1.88$ GeV
END PLOT
BEGIN PLOT /BELLE_2013_I1245023/d28-x01-y01
Title=Cross section for $\gamma\gamma\to K^0_SK^0_S$ with $1.88<\sqrt{s}<1.89$ GeV
END PLOT
BEGIN PLOT /BELLE_2013_I1245023/d28-x01-y02
Title=Cross section for $\gamma\gamma\to K^0_SK^0_S$ with $1.89<\sqrt{s}<1.90$ GeV
END PLOT
BEGIN PLOT /BELLE_2013_I1245023/d28-x01-y03
Title=Cross section for $\gamma\gamma\to K^0_SK^0_S$ with $1.12<\sqrt{s}<1.92$ GeV
END PLOT
BEGIN PLOT /BELLE_2013_I1245023/d29-x01-y01
Title=Cross section for $\gamma\gamma\to K^0_SK^0_S$ with $1.10<\sqrt{s}<1.94$ GeV
END PLOT
BEGIN PLOT /BELLE_2013_I1245023/d29-x01-y02
Title=Cross section for $\gamma\gamma\to K^0_SK^0_S$ with $1.11<\sqrt{s}<1.96$ GeV
END PLOT
BEGIN PLOT /BELLE_2013_I1245023/d29-x01-y03
Title=Cross section for $\gamma\gamma\to K^0_SK^0_S$ with $1.12<\sqrt{s}<1.98$ GeV
END PLOT
BEGIN PLOT /BELLE_2013_I1245023/d30-x01-y01
Title=Cross section for $\gamma\gamma\to K^0_SK^0_S$ with $1.10<\sqrt{s}<2.00$ GeV
END PLOT
BEGIN PLOT /BELLE_2013_I1245023/d30-x01-y02
Title=Cross section for $\gamma\gamma\to K^0_SK^0_S$ with $2.11<\sqrt{s}<2.02$ GeV
END PLOT
BEGIN PLOT /BELLE_2013_I1245023/d30-x01-y03
Title=Cross section for $\gamma\gamma\to K^0_SK^0_S$ with $2.12<\sqrt{s}<2.04$ GeV
END PLOT
BEGIN PLOT /BELLE_2013_I1245023/d31-x01-y01
Title=Cross section for $\gamma\gamma\to K^0_SK^0_S$ with $2.10<\sqrt{s}<2.06$ GeV
END PLOT
BEGIN PLOT /BELLE_2013_I1245023/d31-x01-y02
Title=Cross section for $\gamma\gamma\to K^0_SK^0_S$ with $2.11<\sqrt{s}<2.08$ GeV
END PLOT
BEGIN PLOT /BELLE_2013_I1245023/d31-x01-y03
Title=Cross section for $\gamma\gamma\to K^0_SK^0_S$ with $2.12<\sqrt{s}<2.10$ GeV
END PLOT
BEGIN PLOT /BELLE_2013_I1245023/d32-x01-y01
Title=Cross section for $\gamma\gamma\to K^0_SK^0_S$ with $2.10<\sqrt{s}<2.12$ GeV
END PLOT
BEGIN PLOT /BELLE_2013_I1245023/d32-x01-y02
Title=Cross section for $\gamma\gamma\to K^0_SK^0_S$ with $2.12<\sqrt{s}<2.14$ GeV
END PLOT
BEGIN PLOT /BELLE_2013_I1245023/d32-x01-y03
Title=Cross section for $\gamma\gamma\to K^0_SK^0_S$ with $2.14<\sqrt{s}<2.16$ GeV
END PLOT
BEGIN PLOT /BELLE_2013_I1245023/d33-x01-y01
Title=Cross section for $\gamma\gamma\to K^0_SK^0_S$ with $2.16<\sqrt{s}<2.18$ GeV
END PLOT
BEGIN PLOT /BELLE_2013_I1245023/d33-x01-y02
Title=Cross section for $\gamma\gamma\to K^0_SK^0_S$ with $2.18<\sqrt{s}<2.20$ GeV
END PLOT
BEGIN PLOT /BELLE_2013_I1245023/d33-x01-y03
Title=Cross section for $\gamma\gamma\to K^0_SK^0_S$ with $2.20<\sqrt{s}<2.22$ GeV
END PLOT
BEGIN PLOT /BELLE_2013_I1245023/d34-x01-y01
Title=Cross section for $\gamma\gamma\to K^0_SK^0_S$ with $2.22<\sqrt{s}<2.24$ GeV
END PLOT
BEGIN PLOT /BELLE_2013_I1245023/d34-x01-y02
Title=Cross section for $\gamma\gamma\to K^0_SK^0_S$ with $2.24<\sqrt{s}<2.26$ GeV
END PLOT
BEGIN PLOT /BELLE_2013_I1245023/d34-x01-y03
Title=Cross section for $\gamma\gamma\to K^0_SK^0_S$ with $2.26<\sqrt{s}<2.28$ GeV
END PLOT
BEGIN PLOT /BELLE_2013_I1245023/d35-x01-y01
Title=Cross section for $\gamma\gamma\to K^0_SK^0_S$ with $2.28<\sqrt{s}<2.30$ GeV
END PLOT
BEGIN PLOT /BELLE_2013_I1245023/d35-x01-y02
Title=Cross section for $\gamma\gamma\to K^0_SK^0_S$ with $2.30<\sqrt{s}<2.32$ GeV
END PLOT
BEGIN PLOT /BELLE_2013_I1245023/d35-x01-y03
Title=Cross section for $\gamma\gamma\to K^0_SK^0_S$ with $2.32<\sqrt{s}<2.34$ GeV
END PLOT
BEGIN PLOT /BELLE_2013_I1245023/d36-x01-y01
Title=Cross section for $\gamma\gamma\to K^0_SK^0_S$ with $2.34<\sqrt{s}<2.36$ GeV
END PLOT
BEGIN PLOT /BELLE_2013_I1245023/d36-x01-y02
Title=Cross section for $\gamma\gamma\to K^0_SK^0_S$ with $2.36<\sqrt{s}<2.38$ GeV
END PLOT
BEGIN PLOT /BELLE_2013_I1245023/d36-x01-y03
Title=Cross section for $\gamma\gamma\to K^0_SK^0_S$ with $2.38<\sqrt{s}<2.40$ GeV
END PLOT
BEGIN PLOT /BELLE_2013_I1245023/d37-x01-y01
Title=Cross section for $\gamma\gamma\to K^0_SK^0_S$ with $2.40<\sqrt{s}<2.44$ GeV
END PLOT
BEGIN PLOT /BELLE_2013_I1245023/d37-x01-y02
Title=Cross section for $\gamma\gamma\to K^0_SK^0_S$ with $2.44<\sqrt{s}<2.48$ GeV
END PLOT
BEGIN PLOT /BELLE_2013_I1245023/d37-x01-y03
Title=Cross section for $\gamma\gamma\to K^0_SK^0_S$ with $2.48<\sqrt{s}<2.52$ GeV
END PLOT
BEGIN PLOT /BELLE_2013_I1245023/d38-x01-y01
Title=Cross section for $\gamma\gamma\to K^0_SK^0_S$ with $2.52<\sqrt{s}<2.56$ GeV
END PLOT
BEGIN PLOT /BELLE_2013_I1245023/d38-x01-y02
Title=Cross section for $\gamma\gamma\to K^0_SK^0_S$ with $2.56<\sqrt{s}<2.60$ GeV
END PLOT
BEGIN PLOT /BELLE_2013_I1245023/d38-x01-y03
Title=Cross section for $\gamma\gamma\to K^0_SK^0_S$ with $2.60<\sqrt{s}<2.70$ GeV
END PLOT
BEGIN PLOT /BELLE_2013_I1245023/d39-x01-y01
Title=Cross section for $\gamma\gamma\to K^0_SK^0_S$ with $2.70<\sqrt{s}<2.80$ GeV
END PLOT
BEGIN PLOT /BELLE_2013_I1245023/d39-x01-y02
Title=Cross section for $\gamma\gamma\to K^0_SK^0_S$ with $2.80<\sqrt{s}<2.90$ GeV
END PLOT
BEGIN PLOT /BELLE_2013_I1245023/d39-x01-y03
Title=Cross section for $\gamma\gamma\to K^0_SK^0_S$ with $2.90<\sqrt{s}<3.00$ GeV
END PLOT
BEGIN PLOT /BELLE_2013_I1245023/d40-x01-y01
Title=Cross section for $\gamma\gamma\to K^0_SK^0_S$ with $3.00<\sqrt{s}<3.10$ GeV
END PLOT
BEGIN PLOT /BELLE_2013_I1245023/d40-x01-y02
Title=Cross section for $\gamma\gamma\to K^0_SK^0_S$ with $3.10<\sqrt{s}<3.20$ GeV
END PLOT
BEGIN PLOT /BELLE_2013_I1245023/d40-x01-y03
Title=Cross section for $\gamma\gamma\to K^0_SK^0_S$ with $3.20<\sqrt{s}<3.30$ GeV
END PLOT


