BEGIN PLOT /BELLE_2021_I1867474/d01-x01-y01
Title=$\pi^+\pi^-$ mass distribution in $D^0\to \pi^+\pi^-\eta$
XLabel=$m^2_{\pi^+\pi^-}$ [$\mathrm{GeV}^2$]
YLabel=$1/\Gamma\mathrm{d} \Gamma/\mathrm{d}m^2_{\pi^+\pi^-}$ [$\mathrm{GeV}^{-2}$]
LogY=0
END PLOT
BEGIN PLOT /BELLE_2021_I1867474/d01-x01-y02
Title=$\eta\pi^-$ mass distribution in $D^0\to \pi^+\pi^-\eta$
XLabel=$m^2_{\eta\pi^-}$ [$\mathrm{GeV}^2$]
YLabel=$1/\Gamma\mathrm{d} \Gamma/\mathrm{d}m^2_{\eta\pi^-}$ [$\mathrm{GeV}^{-2}$]
LogY=0
END PLOT
BEGIN PLOT /BELLE_2021_I1867474/d01-x01-y03
Title=$\eta\pi^+$ mass distribution in $D^0\to \pi^+\pi^-\eta$
XLabel=$m^2_{\eta\pi^+}$ [$\mathrm{GeV}^2$]
YLabel=$1/\Gamma\mathrm{d} \Gamma/\mathrm{d}m^2_{\eta\pi^+}$ [$\mathrm{GeV}^{-2}$]
LogY=0
END PLOT
BEGIN PLOT /BELLE_2021_I1867474/d02-x01-y01
Title=$K^+K^-$ mass distribution in $D^0\to K^+K^-\eta$
XLabel=$m^2_{K^+K^-}$ [$\mathrm{GeV}^2$]
YLabel=$1/\Gamma\mathrm{d} \Gamma/\mathrm{d}m^2_{K^+K^-}$ [$\mathrm{GeV}^{-2}$]
LogY=0
END PLOT
BEGIN PLOT /BELLE_2021_I1867474/d02-x01-y02
Title=$\eta K^-$ mass distribution in $D^0\to K^+K^-\eta$
XLabel=$m^2_{\eta K^-}$ [$\mathrm{GeV}^2$]
YLabel=$1/\Gamma\mathrm{d} \Gamma/\mathrm{d}m^2_{\eta K^-}$ [$\mathrm{GeV}^{-2}$]
LogY=0
END PLOT
BEGIN PLOT /BELLE_2021_I1867474/d02-x01-y03
Title=$\eta K^+$ mass distribution in $D^0\to K^+K^-\eta$
XLabel=$m^2_{\eta K^+}$ [$\mathrm{GeV}^2$]
YLabel=$1/\Gamma\mathrm{d} \Gamma/\mathrm{d}m^2_{\eta K^+}$ [$\mathrm{GeV}^{-2}$]
LogY=0
END PLOT
