BEGIN PLOT /BELLE_2005_I679052/d01-x01-y01
Title=Differential branching ratio for $B\to X_s\ell^+\ell^-$ vs $m_X$
XLabel=$m_X$ [GeV]
YLabel=$\text{d}\mathcal{B}/\text{d}m_X \times 10^6$ [$\text{GeV}^{_1}$]
LogY=0
END PLOT
BEGIN PLOT /BELLE_2005_I679052/d01-x01-y02
Title=Differential branching ratio for $B\to X_s\ell^+\ell^-$ vs $q^2$
XLabel=$q^2$ [$\text{GeV}^{2}$]
YLabel=$\text{d}\mathcal{B}/\text{d}q^2$ [$\text{GeV}^{-2}$]
END PLOT
