BEGIN PLOT /BELLE_2010_I899499/d01-x01-y01
Title=Differential branching ratio for $B\to X_s\ell^+\ell^-$ vs $m_X$
XLabel=$m_X$ [GeV]
YLabel=$\mathrm{d}\mathcal{B}/\mathrm{d}m_X \times 10^6$ [$\mathrm{GeV}^{_1}$]
LogY=0
END PLOT
BEGIN PLOT /BELLE_2010_I899499/d01-x01-y02
Title=Differential branching ratio for $B\to X_s\ell^+\ell^-$ vs $q^2$
XLabel=$q^2$ [$\mathrm{GeV}^{2}$]
YLabel=$\mathrm{d}\mathcal{B}/\mathrm{d}q^2 \times 10^6$ [$\mathrm{GeV}^{_2}$]
END PLOT
