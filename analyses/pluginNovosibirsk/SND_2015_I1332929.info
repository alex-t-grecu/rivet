Name: SND_2015_I1332929
Year: 2015
Summary: Cross section for $e^+e^-\to\eta\pi^+\pi^-$ between 1.22 and 2 GeV
Experiment: SND
Collider: VEPP-2M
InspireID: 1332929
Status: VALIDATED
Authors:
 - Peter Richardson <peter.richardson@durham.ac.uk>
References:
- Phys.Rev. D91 (2015) 052013, 2015 
RunInfo: e+e- to hadrons
NeedCrossSection: yes
Beams: [e+,e-]
Energies:  [1.225, 1.25, 1.278, 1.3, 1.325, 1.356, 1.375, 1.4, 1.425, 1.443, 1.475, 1.5, 1.522, 1.55, 1.575, 1.6, 1.625, 1.65, 1.678, 1.7, 1.723, 1.756, 1.775, 1.8, 1.825, 1.843, 1.871, 1.897, 1.922, 1.943, 1.96, 1.978, 2.0]
Options:
 - ENERGY=*
Description:
  'Measurement of the cross section for $e^+e^-\to\eta\pi^+\pi^-$ at energies between 1.22 and 2 GeV.
   Beam energy must be specified as analysis option "ENERGY" when rivet-merging samples.'
Keywords: 
BibKey: Aulchenko:2014vkn
BibTeX: '@article{Aulchenko:2014vkn,
      author         = "Aulchenko, V. M. and others",
      title          = "{Measurement of the $e^+e^- \to \eta\pi^+\pi^-$ cross
                        section in the center-of-mass energy range 1.22-2.00 GeV
                        with the SND detector at the VEPP-2000 collider}",
      collaboration  = "SND",
      journal        = "Phys. Rev.",
      volume         = "D91",
      year           = "2015",
      number         = "5",
      pages          = "052013",
      doi            = "10.1103/PhysRevD.91.052013",
      eprint         = "1412.1971",
      archivePrefix  = "arXiv",
      primaryClass   = "hep-ex",
      SLACcitation   = "%%CITATION = ARXIV:1412.1971;%%"
}'
