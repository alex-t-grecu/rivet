BEGIN PLOT /JADE_1990_I295180/d01-x01-y01
Title=Cross section for $\gamma\gamma\to \pi^0\pi^0$ with $|\cos\theta|<0.3$
XLabel=$\sqrt{s}$ [GeV]
YLabel=$\sigma(\gamma\gamma\to \pi^0\pi^0)$ [nb]
LogY=1
END PLOT
