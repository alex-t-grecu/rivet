# BEGIN PLOT /HRS_1986_I18502/d01-x01-y01
Title=Total charged multiplicity
XLabel=$n_\mathrm{ch}$
YLabel=$2/\sigma \, \mathrm{d}{\sigma}/\mathrm{d}{n_\mathrm{ch}}$ ($\%$)
# END PLOT

# BEGIN PLOT /HRS_1986_I18502/d03-x01-y01
Title=Mean charged multiplicity
LogY=0
# END PLOT
