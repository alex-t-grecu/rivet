BEGIN PLOT /MARKI_1979_I133123/d01-x01-y01
Title=$D^0$ cross section at 7 GeV
XLabel=$\sqrt{s}$ [GeV]
YLabel=$\sigma$ [nb]
LogY=0
END PLOT
BEGIN PLOT /MARKI_1979_I133123/d01-x01-y02
Title=$D^+$ cross section at 7 GeV
XLabel=$\sqrt{s}$ [GeV]
YLabel=$\sigma$ [nb]
LogY=0
END PLOT
BEGIN PLOT /MARKI_1979_I133123/d0[3,4]-x01-y01
Title=$D^0$ spectrum at 7 GeV
XLabel=$z$
YLabel=$s\mathrm{d}\sigma/\mathrm{d}z$ [$\mu\mathrm{b} \mathrm{GeV}^2$]
LogY=0
END PLOT
BEGIN PLOT /MARKI_1979_I133123/d0[3,4]-x01-y02
Title=$D^+$ spectrum at 7 GeV
XLabel=$z$
YLabel=$s\mathrm{d}\sigma/\mathrm{d}z$ [$\mu\mathrm{b} \mathrm{GeV}^2$]
LogY=0
END PLOT
