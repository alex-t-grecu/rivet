BEGIN PLOT /BABAR_2011_I883525/d01-x01-y01
Title=Cross section for $e^+e^-\to e^+e^-\eta$
XLabel=$Q^2$ [$\text{GeV}^2$]
YLabel=$\text{d}\sigma/\text{d}Q^2$ [$\text{fb}/\text{GeV}^2$]
END PLOT
BEGIN PLOT /BABAR_2011_I883525/d02-x01-y01
Title=Cross section for $e^+e^-\to e^+e^-\eta^\prime$
XLabel=$Q^2$ [$\text{GeV}^2$]
YLabel=$\text{d}\sigma/\text{d}Q^2$ [$\text{fb}/\text{GeV}^2$]
END PLOT
