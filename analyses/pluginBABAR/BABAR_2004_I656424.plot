BEGIN PLOT /BABAR_2004_I656424/d01-x01-y01
Title=Helicity angle in $B\to D_{s1}^+(\to D_s^+\gamma) \bar{D}$
XLabel=$\cos\theta$
YLabel=$1/\Gamma\text{d}\Gamma/\text{d}\cos\theta$
LogY=0
END PLOT
