BEGIN PLOT /BABAR_2015_I1388182
LogY=0
XLabel=$\cos\phi^\pm$
YLabel=Asymmetry
END PLOT

BEGIN PLOT /BABAR_2015_I1388182/d01-x01-y01
Title=Charge Asymmetry in $e^+e^-\to\mu^+\mu^-\gamma$ for $0.2<m_{\mu\mu}<0.5\,$GeV 
END PLOT
BEGIN PLOT /BABAR_2015_I1388182/d01-x01-y02
Title=Charge Asymmetry in $e^+e^-\to\mu^+\mu^-\gamma$ for $0.5<m_{\mu\mu}<1.0\,$GeV 
END PLOT
BEGIN PLOT /BABAR_2015_I1388182/d01-x01-y03
Title=Charge Asymmetry in $e^+e^-\to\mu^+\mu^-\gamma$ for $1.0<m_{\mu\mu}<1.5\,$GeV 
END PLOT
BEGIN PLOT /BABAR_2015_I1388182/d01-x01-y04
Title=Charge Asymmetry in $e^+e^-\to\mu^+\mu^-\gamma$ for $1.5<m_{\mu\mu}<2.0\,$GeV 
END PLOT
BEGIN PLOT /BABAR_2015_I1388182/d01-x01-y05
Title=Charge Asymmetry in $e^+e^-\to\mu^+\mu^-\gamma$ for $2.0<m_{\mu\mu}<2.5\,$GeV 
END PLOT
BEGIN PLOT /BABAR_2015_I1388182/d01-x01-y06
Title=Charge Asymmetry in $e^+e^-\to\mu^+\mu^-\gamma$ for $2.5<m_{\mu\mu}<3.0\,$GeV 
END PLOT
BEGIN PLOT /BABAR_2015_I1388182/d01-x01-y07
Title=Charge Asymmetry in $e^+e^-\to\mu^+\mu^-\gamma$ for $3.0<m_{\mu\mu}<3.5\,$GeV 
END PLOT
BEGIN PLOT /BABAR_2015_I1388182/d01-x01-y08
Title=Charge Asymmetry in $e^+e^-\to\mu^+\mu^-\gamma$ for $3.5<m_{\mu\mu}<4.0\,$GeV 
END PLOT
BEGIN PLOT /BABAR_2015_I1388182/d01-x01-y09
Title=Charge Asymmetry in $e^+e^-\to\mu^+\mu^-\gamma$ for $4.0<m_{\mu\mu}<4.5\,$GeV 
END PLOT
BEGIN PLOT /BABAR_2015_I1388182/d01-x01-y10
Title=Charge Asymmetry in $e^+e^-\to\mu^+\mu^-\gamma$ for $4.5<m_{\mu\mu}<5.0\,$GeV 
END PLOT
BEGIN PLOT /BABAR_2015_I1388182/d01-x01-y11
Title=Charge Asymmetry in $e^+e^-\to\mu^+\mu^-\gamma$ for $5.0<m_{\mu\mu}<5.5\,$GeV 
END PLOT
BEGIN PLOT /BABAR_2015_I1388182/d01-x01-y12
Title=Charge Asymmetry in $e^+e^-\to\mu^+\mu^-\gamma$ for $5.5<m_{\mu\mu}<6.0\,$GeV 
END PLOT
BEGIN PLOT /BABAR_2015_I1388182/d01-x01-y13
Title=Charge Asymmetry in $e^+e^-\to\mu^+\mu^-\gamma$ for $6.0<m_{\mu\mu}<6.5\,$GeV 
END PLOT
BEGIN PLOT /BABAR_2015_I1388182/d01-x01-y14
Title=Charge Asymmetry in $e^+e^-\to\mu^+\mu^-\gamma$ for $6.5<m_{\mu\mu}<7.0\,$GeV 
END PLOT

BEGIN PLOT /BABAR_2015_I1388182/d02-x01-y01
Title=Slope of charge asymmetry for $e^+e^-\to\mu^+\mu^-\gamma$
XLabel=$m_{\mu\mu}$ [GeV]
YLabel=$A_0$
END PLOT

BEGIN PLOT /BABAR_2015_I1388182/d03-x01-y01
Title=Charge Asymmetry in $e^+e^-\to\pi^+\pi^-\gamma$ for $0.3<m_{\mu\mu}<0.4\,$GeV 
END PLOT
BEGIN PLOT /BABAR_2015_I1388182/d03-x01-y02
Title=Charge Asymmetry in $e^+e^-\to\pi^+\pi^-\gamma$ for $0.4<m_{\mu\mu}<0.5\,$GeV 
END PLOT
BEGIN PLOT /BABAR_2015_I1388182/d03-x01-y03
Title=Charge Asymmetry in $e^+e^-\to\pi^+\pi^-\gamma$ for $0.5<m_{\mu\mu}<0.6\,$GeV 
END PLOT
BEGIN PLOT /BABAR_2015_I1388182/d03-x01-y04
Title=Charge Asymmetry in $e^+e^-\to\pi^+\pi^-\gamma$ for $0.6<m_{\mu\mu}<0.7\,$GeV 
END PLOT
BEGIN PLOT /BABAR_2015_I1388182/d03-x01-y05
Title=Charge Asymmetry in $e^+e^-\to\pi^+\pi^-\gamma$ for $0.7<m_{\mu\mu}<0.8\,$GeV 
END PLOT
BEGIN PLOT /BABAR_2015_I1388182/d03-x01-y06
Title=Charge Asymmetry in $e^+e^-\to\pi^+\pi^-\gamma$ for $0.8<m_{\mu\mu}<0.9\,$GeV 
END PLOT
BEGIN PLOT /BABAR_2015_I1388182/d03-x01-y07
Title=Charge Asymmetry in $e^+e^-\to\pi^+\pi^-\gamma$ for $0.9<m_{\mu\mu}<1.0\,$GeV 
END PLOT
BEGIN PLOT /BABAR_2015_I1388182/d03-x01-y08
Title=Charge Asymmetry in $e^+e^-\to\pi^+\pi^-\gamma$ for $1.0<m_{\mu\mu}<1.1\,$GeV 
END PLOT
BEGIN PLOT /BABAR_2015_I1388182/d03-x01-y09
Title=Charge Asymmetry in $e^+e^-\to\pi^+\pi^-\gamma$ for $1.1<m_{\mu\mu}<1.2\,$GeV 
END PLOT
BEGIN PLOT /BABAR_2015_I1388182/d03-x01-y10
Title=Charge Asymmetry in $e^+e^-\to\pi^+\pi^-\gamma$ for $1.2<m_{\mu\mu}<1.3\,$GeV 
END PLOT
BEGIN PLOT /BABAR_2015_I1388182/d03-x01-y11
Title=Charge Asymmetry in $e^+e^-\to\pi^+\pi^-\gamma$ for $1.3<m_{\mu\mu}<1.4\,$GeV 
END PLOT
BEGIN PLOT /BABAR_2015_I1388182/d03-x01-y12
Title=Charge Asymmetry in $e^+e^-\to\pi^+\pi^-\gamma$ for $1.4<m_{\mu\mu}<1.5\,$GeV 
END PLOT
BEGIN PLOT /BABAR_2015_I1388182/d03-x01-y13
Title=Charge Asymmetry in $e^+e^-\to\pi^+\pi^-\gamma$ for $1.5<m_{\mu\mu}<1.6\,$GeV 
END PLOT
BEGIN PLOT /BABAR_2015_I1388182/d03-x01-y14
Title=Charge Asymmetry in $e^+e^-\to\pi^+\pi^-\gamma$ for $1.6<m_{\mu\mu}<1.7\,$GeV 
END PLOT
BEGIN PLOT /BABAR_2015_I1388182/d03-x01-y15
Title=Charge Asymmetry in $e^+e^-\to\pi^+\pi^-\gamma$ for $1.7<m_{\mu\mu}<1.8\,$GeV 
END PLOT

BEGIN PLOT /BABAR_2015_I1388182/d04-x01-y01
Title=Slope of charge asymmetry for $e^+e^-\to\pi^+\pi^-\gamma$
XLabel=$m_{\pi\pi}$ [GeV]
YLabel=$A_0$
END PLOT
