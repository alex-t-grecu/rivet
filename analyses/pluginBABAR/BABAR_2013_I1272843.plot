BEGIN PLOT /BABAR_2013_I1272843/d01
XLabel=$q^2$ [$\text{GeV}^2$]
LogY=0
END PLOT
BEGIN PLOT /BABAR_2013_I1272843/d02
XLabel=$M_X$ [$\text{GeV}$]
LogY=0
END PLOT

BEGIN PLOT /BABAR_2013_I1272843/d01-x0[1,2]-y01
Title=Differential branching ratio w.r.t $q^2$ for $B\to X_s e^+e^-$
YLabel=$\text{d}\mathcal{B}/\text{d}q^2$ [$\text{GeV}^{-2}$]
END PLOT
BEGIN PLOT /BABAR_2013_I1272843/d01-x0[1,2]-y02
Title=Differential branching ratio w.r.t $q^2$ for $B\to X_s \mu^+\mu^-$
YLabel=$\text{d}\mathcal{B}/\text{d}q^2$ [$\text{GeV}^{-2}$]
END PLOT
BEGIN PLOT /BABAR_2013_I1272843/d01-x0[1,2]-y03
Title=Differential branching ratio w.r.t $q^2$ for $B\to X_s \ell^+\ell^-$
YLabel=$\text{d}\mathcal{B}/\text{d}q^2$ [$\text{GeV}^{-2}$]
END PLOT
BEGIN PLOT /BABAR_2013_I1272843/d01-x0[1,2]-y03
Title=$A_{CP}$ versus $q^2$ for $B\to X_s \ell^+\ell^-$
YLabel=$A_{CP}$
END PLOT

BEGIN PLOT /BABAR_2013_I1272843/d02-x0[1,2]-y01
Title=Differential branching ratio w.r.t $M_X$ for $B\to X_s e^+e^-$
YLabel=$\text{d}\mathcal{B}/\text{d}M_X$ [$\text{GeV}^{-1}$]
END PLOT
BEGIN PLOT /BABAR_2013_I1272843/d02-x0[1,2]-y02
Title=Differential branching ratio w.r.t $M_X$ for $B\to X_s \mu^+\mu^-$
YLabel=$\text{d}\mathcal{B}/\text{d}M_X$ [$\text{GeV}^{-1}$]
END PLOT
BEGIN PLOT /BABAR_2013_I1272843/d02-x0[1,2]-y03
Title=Differential branching ratio w.r.t $M_X$ for $B\to X_s \ell^+\ell^-$
YLabel=$\text{d}\mathcal{B}/\text{d}M_X$ [$\text{GeV}^{-1}$]
END PLOT
