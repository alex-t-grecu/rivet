BEGIN PLOT /EXAMPLE_HTT/DeltaMtop
Title=Delta Top
XLabel=$\Delta t$
YLabel=Events
END PLOT

BEGIN PLOT /EXAMPLE_HTT/Mpruned
Title=Pruned top candidate mass
XLabel=$m_\mathrm{pruned}(t)$ [GeV]
YLabel=Events
END PLOT

BEGIN PLOT /EXAMPLE_HTT/Munfiltered
Title=Unfiltered top candidate mass
XLabel=$m_\mathrm{unfilt}(t)$ [GeV]
YLabel=[Events]
END PLOT

BEGIN PLOT /EXAMPLE_HTT/Mcand
Title=top candidate mass
XLabel=$m(t)$ [GeV]
YLabel=Events
END PLOT

BEGIN PLOT /EXAMPLE_HTT/DMpruned
Title=Mass difference between pruned top candidate and actual top candidate
XLabel=$|m_\mathrm{pruned}(t) - m(t)|$ [GeV]
YLabel=Events
END PLOT

BEGIN PLOT /EXAMPLE_HTT/DMunfiltered
Title=Mass difference between unfiltered top candidate and actual top candidate
XLabel=$|m_\mathrm{unfilt}(t) - m(t)|$ [GeV]
YLabel=Events
END PLOT
