BEGIN PLOT /CMS_2021_I1876550/d01
XLabel=$p_\perp$ [GeV]
YLabel=$\text{d}\sigma/\text{d}p_\perp$ [$\mu$b/GeV]
LogY=1
END PLOT
BEGIN PLOT /CMS_2021_I1876550/d02
XLabel=$|\eta|$
YLabel=$\text{d}\sigma/\text{d}|\eta|$ [$\mu$b]
LogY=1
END PLOT

BEGIN PLOT /CMS_2021_I1876550/d01-x01-y01
Title=Cross section for prompt $D^{*\pm}$ mesons ($|\eta<2.1|$)
END PLOT
BEGIN PLOT /CMS_2021_I1876550/d01-x01-y02
Title=Cross section for prompt $D^0$ mesons ($|\eta<2.1|$)
END PLOT
BEGIN PLOT /CMS_2021_I1876550/d01-x01-y03
Title=Cross section for prompt $D^+$ mesons ($|\eta<2.1|$)
END PLOT

BEGIN PLOT /CMS_2021_I1876550/d02-x01-y01
Title=Cross section for prompt $D^{*\pm}$ mesons ($4<p_\perp<100$\,GeV)
END PLOT
BEGIN PLOT /CMS_2021_I1876550/d02-x01-y02
Title=Cross section for prompt $D^0$ mesons ($4<p_\perp<100$\,GeV)
END PLOT
BEGIN PLOT /CMS_2021_I1876550/d02-x01-y03
Title=Cross section for prompt $D^+$ mesons ($4<p_\perp<100$\,GeV)
END PLOT
