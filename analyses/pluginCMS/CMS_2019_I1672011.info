Name: CMS_2019_I1672011
Year: 2019
Summary: Prompt $\psi(2S)$ production at 5.02 TeV
Experiment: CMS
Collider: LHC
InspireID: 1672011
Status: VALIDATED
Reentrant: true
Authors:
 - Peter Richardson <peter.richardson@durham.ac.uk>
References:
 - Phys.Lett.B 790 (2019) 509-532, 2019
 - arXiv:1805.02248 [hep-ex]
 - CMS-HIN-16-015
RunInfo: hadronic events with psi(2S) production
Beams: [p+, p+]
Energies: [5020]
Description:
  'Measurement of the double differential cross section for prompt $\psi(2S)$ production at 5.02 TeV by the CMS collaboration. Only the proton-proton results are implemented.'
ValidationInfo:
  'Hrewig 7 events'
#ReleaseTests:
# - $A my-hepmc-prefix :MODE=some_rivet_flag
Keywords: []
BibKey: CMS:2018gbb
BibTeX: '@article{CMS:2018gbb,
    author = "Sirunyan, Albert M and others",
    collaboration = "CMS",
    title = "{Measurement of prompt $\psi$(2S) production cross sections in proton-lead and proton-proton collisions at $\sqrt{s_{_\mathrm{NN}}}=$ 5.02 TeV}",
    eprint = "1805.02248",
    archivePrefix = "arXiv",
    primaryClass = "hep-ex",
    reportNumber = "CMS-HIN-16-015, CERN-EP-2018-056",
    doi = "10.1016/j.physletb.2019.01.058",
    journal = "Phys. Lett. B",
    volume = "790",
    pages = "509--532",
    year = "2019"
}
'
