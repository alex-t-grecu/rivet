###########################################
Cut 'n' count cutflows 
###########################################
########## Gbb Cutflows
BEGIN PLOT /ATLAS_2022_I2182381/CF_Gbb_0l_B
Title=Gbb 0L Boosted region Cutflow
YLabel=Events at 139 fb$^{\textrm{-1}}$
XCustomMajorTicks=1  $\,$  2   $N_{\textrm{lep,base}}=0$  3  $\Delta\phi^{4j}_{\textrm{min}}\geq0.4$  4  $m^{b\textrm{-jets}}_{\textrm{T,min}}\geq130$  5  $E^{\textrm{miss}}_T\geq550$  6  $p_T^{\textrm{jet}}\geq65$  7  $m_{\textrm{eff}}\geq2600$
XMajorTicksAngle=-20

END PLOT


BEGIN PLOT /ATLAS_2022_I2182381/CF_Gbb_0l_C
Title=Gbb 0L Compressed region Cutflow
YLabel=Events at 139 fb$^{\textrm{-1}}$
XCustomMajorTicks=1  $\,$  2   $N_{\textrm{lep,base}}=0$  3  $\Delta\phi^{4j}_{\textrm{min}}\geq0.4$  4  $m^{b\textrm{-jets}}_{\textrm{T,min}}\geq130$  5  $E^{\textrm{miss}}_T\geq550$  6  $m_{\textrm{eff}}\geq1600$
XMajorTicksAngle=-20

END PLOT

BEGIN PLOT /ATLAS_2022_I2182381/CF_Gbb_0l_M
Title=Gbb 0L Moderate region Cutflow
YLabel=Events (139 invfb)
XCustomMajorTicks=1  $\,$  2   $N_{\textrm{lep,base}}=0$  3  $\Delta\phi^{4j}_{\textrm{min}}\geq0.4$  4  $m^{b\textrm{-jets}}_{\textrm{T,min}}\geq130$  5  $E^{\textrm{miss}}_T\geq550$  6  $m_{\textrm{eff}}\geq1600$
XMajorTicksAngle=-20

END PLOT

########## Gtb Cutflows
BEGIN PLOT /ATLAS_2022_I2182381/CF_Gtb_0l_B
Title=Gtb 0L Boosted region Cutflow
XCustomMajorTicks=1  $\,$  2   $E^{\textrm{miss}}_T\quad\textrm{Trig.}$   3   $N_{\textrm{lep,base}}=0$  4  $\Delta\phi^{4j}_{\textrm{min}}\geq0.4$  5  $m^{b\textrm{-jets}}_{\textrm{T,min}}\geq130$  6  $m_{\textrm{eff}}\geq2500$   7   $E^{\textrm{miss}}_T\geq550$  8  $M_J^{\Sigma}\geq200$
XMajorTicksAngle=-45
YLabel=Events at 139 fb$^{\textrm{-1}}$

END PLOT

BEGIN PLOT /ATLAS_2022_I2182381/CF_Gtb_0l_C
Title=Gtb 0L Compressed region Cutflow
YLabel=Events at 139 fb$^{\textrm{-1}}$
XCustomMajorTicks=1  $\,$  2 $E^{\textrm{miss}}_T\quad\textrm{Trig.}$   3   $N_{\textrm{lep,base}}=0$  4  $\Delta\phi^{4j}_{\textrm{min}}\geq0.4$  5  $m^{b\textrm{-jets}}_{\textrm{T,min}}\geq130$  6  $N_{\textrm{jet}}\geq7$  7 $N_{b\textrm{-jets}}\geq4$  8 $m_{\textrm{eff}}\geq1300$   9   $E^{\textrm{miss}}_T\geq500$  10  $M_J^{\Sigma}\geq50$
XMajorTicksAngle=-45

END PLOT


BEGIN PLOT /ATLAS_2022_I2182381/CF_Gtb_0l_M
Title=Gtb 0L Moderate region Cutflow
YLabel=Events at 139 fb$^{\textrm{-1}}$
XCustomMajorTicks=1  $\,$  2 $E^{\textrm{miss}}_T\quad\textrm{Trig.}$   3   $N_{\textrm{lep,base}}=0$  4  $\Delta\phi^{4j}_{\textrm{min}}\geq0.4$  5  $m^{b\textrm{-jets}}_{\textrm{T,min}}\geq130$  6  $N_{\textrm{jet}}\geq7$  7 $N_{b\textrm{-jets}}\geq4$  8 $m_{\textrm{eff}}\geq2000$   9   $E^{\textrm{miss}}_T\geq550$  10  $M_J^{\Sigma}\geq200$
XMajorTicksAngle=-45

END PLOT

########## Gtt 0L Cutflows
BEGIN PLOT /ATLAS_2022_I2182381/CF_Gtt_0l_B
Title=Gtt 0L Boosted region Cutflow
YLabel=Events at 139 fb$^{\textrm{-1}}$
XCustomMajorTicks=1  $\,$  2   $N_{\textrm{lep,base}}=0$  3  $\Delta\phi^{4j}_{\textrm{min}}\geq0.4$  4  $N_{\textrm{jet}}\geq5$  5   $E^{\textrm{miss}}_T\geq600$  6  $m_{\textrm{eff}}\geq2900$  7   $m^{b\textrm{-jets}}_{\textrm{T,min}}\geq120$  8  $M_J^{\Sigma}\geq300$
XMajorTicksAngle=-60

END PLOT


BEGIN PLOT /ATLAS_2022_I2182381/CF_Gtt_0l_C
Title=Gtt 0L Compressed region Cutflow
YLabel=Events at 139 fb$^{\textrm{-1}}$
XCustomMajorTicks=1  $\,$  2   $N_{\textrm{lep,base}}=0$  3  $\Delta\phi^{4j}_{\textrm{min}}\geq0.4$  4  $N_{\textrm{jet}}\geq10$  5  $N_{b\textrm{-jets}}\geq4$ 6  $E^{\textrm{miss}}_T\geq400$  7  $m_{\textrm{eff}}\geq800$  8  $m^{b\textrm{-jets}}_{\textrm{T,min}}\geq180$  9  $M_J^{\Sigma}\geq100$
XMajorTicksAngle=-60

END PLOT

BEGIN PLOT /ATLAS_2022_I2182381/CF_Gtt_0l_M1
Title=Gtt 0L Moderate region 1 Cutflow
YLabel=Events (139 invfb)
XCustomMajorTicks=1  $\,$  2   $N_{\textrm{lep,base}}=0$  3  $\Delta\phi^{4j}_{\textrm{min}}\geq0.4$  4  $N_{\textrm{jet}}\geq5\;\textrm{\&}\;N_{b\textrm{-jets}}\geq3$  5   $E^{\textrm{miss}}_T\geq600$  6  $m_{\textrm{eff}}\geq1700$  7   $m^{b\textrm{-jets}}_{\textrm{T,min}}\geq120$  8  $M_J^{\Sigma}\geq300$
XMajorTicksAngle=-60

END PLOT


BEGIN PLOT /ATLAS_2022_I2182381/CF_Gtt_0l_M2
Title=Gtt 0L Moderate region 2 Cutflow 
YLabel=Events (139 invfb)
XCustomMajorTicks=1  $\,$  2   $N_{\textrm{lep,base}}=0$  3  $\Delta\phi^{4j}_{\textrm{min}}\geq0.4$  4  $N_{\textrm{jet}}\geq5\;\textrm{\&}\;N_{b\textrm{-jets}}\geq3$  5   $E^{\textrm{miss}}_T\geq500$  6  $m_{\textrm{eff}}\geq1100$  7   $m^{b\textrm{-jets}}_{\textrm{T,min}}\geq120$  8  $M_J^{\Sigma}\geq200$
XMajorTicksAngle=-60

END PLOT


BEGIN PLOT /ATLAS_2022_I2182381/CF_Gtt_1l_B
Title=Gtt 1L Boosted region Cutflow
YLabel=Events at 139 fb$^{\textrm{-1}}$
XCustomMajorTicks=1  $\,$  2   $N_{\textrm{lep,signal}}\geq1$  3  $N_{\textrm{jet}}\geq4\;\textrm{\&}$\newline$N_{b\textrm{-jets}}\geq3$  4  $E^{\textrm{miss}}_T\geq600$  5  $m_{\textrm{eff}}\geq2300$  6 $m_T\geq150$   7   $m^{b\textrm{-jets}}_{\textrm{T,min}}\geq120$  8  $M_J^{\Sigma}\geq200$
XMajorTicksAngle=-60

END PLOT


BEGIN PLOT /ATLAS_2022_I2182381/CF_Gtt_1l_C
Title=Gtt 1L Compressed region Cutflow
YLabel=Events at 139 fb$^{\textrm{-1}}$
XCustomMajorTicks=1  $\,$  2   $N_{\textrm{lep,signal}}\geq1$  3  $N_{\textrm{jet}}\geq9\;\textrm{\&}$\newline$N_{b\textrm{-jets}}\geq3$  4  $E^{\textrm{miss}}_T\geq300$  5  $m_{\textrm{eff}}\geq800$  6 $m_T\geq150$   7   $m^{b\textrm{-jets}}_{\textrm{T,min}}\geq120$
XMajorTicksAngle=-60

END PLOT

BEGIN PLOT /ATLAS_2022_I2182381/CF_Gtt_1l_M1
Title=Gtt 1L Moderate region 1 Cutflow
YLabel=Events (139 invfb)
XCustomMajorTicks=1  $\,$  2   $N_{\textrm{lep,signal}}\geq1$  3  $N_{\textrm{jet}}\geq5\;\textrm{\&}$\newline$N_{b\textrm{-jets}}\geq3$  4  $E^{\textrm{miss}}_T\geq600$  5  $m_{\textrm{eff}}\geq2000$  6 $m_T\geq200$   7   $m^{b\textrm{-jets}}_{\textrm{T,min}}\geq120$  8  $M_J^{\Sigma}\geq200$
XMajorTicksAngle=-60

END PLOT


BEGIN PLOT /ATLAS_2022_I2182381/CF_Gtt_1l_M2
Title=Gtt 1L Moderate region 2 Cutflow 
YLabel=Events (139 invfb)
XCustomMajorTicks=1  $\,$  2   $N_{\textrm{lep,signal}}\geq1$  3  $N_{\textrm{jet}}\geq8\;\textrm{\&}$\newline$N_{b\textrm{-jets}}\geq3$  4  $E^{\textrm{miss}}_T\geq500$  5  $m_{\textrm{eff}}\geq1100$  6 $m_T\geq200$   7   $m^{b\textrm{-jets}}_{\textrm{T,min}}\geq120$  8  $M_J^{\Sigma}\geq100$
XMajorTicksAngle=-60

END PLOT


###########################################
Neural Net cutflows 
###########################################

########## Gbb Cutflows
BEGIN PLOT /ATLAS_2022_I2182381/CF_Gbb_2000_1800
Title=Gbb 0L NN 2000,1800 SR cutflow
YLabel=Events at 139 fb$^{\textrm{-1}}$

XCustomMajorTicks=1  $\,$  2   $N_{\textrm{lep,base}}=0$  3  $\Delta\phi^{4j}_{\textrm{min}}\geq0.4$  4  $P(\textrm{Gbb})\geq0.997$
XMajorTicksAngle=-20

END PLOT

BEGIN PLOT /ATLAS_2022_I2182381/CF_Gbb_2100_1600
Title=Gbb 0L NN 2100,1600 SR cutflow
YLabel=Events at 139 fb$^{\textrm{-1}}$
XCustomMajorTicks=1  $\,$  2   $N_{\textrm{lep,base}}=0$  3  $\Delta\phi^{4j}_{\textrm{min}}\geq0.4$  4  $P(\textrm{Gbb})\geq0.9993$
XMajorTicksAngle=-20

END PLOT

BEGIN PLOT /ATLAS_2022_I2182381/CF_Gbb_2300_1000
Title=Gbb 0L NN 2300,1000 SR cutflow
YLabel=Events at 139 fb$^{\textrm{-1}}$
XCustomMajorTicks=1  $\,$  2   $N_{\textrm{lep,base}}=0$  3  $\Delta\phi^{4j}_{\textrm{min}}\geq0.6$  4  $P(\textrm{Gbb})\geq0.9994$
XMajorTicksAngle=-20

END PLOT

BEGIN PLOT /ATLAS_2022_I2182381/CF_Gbb_2800_1400
Title=Gbb 0L NN 2800,1400 SR cutflow
YLabel=Events at 139 fb$^{\textrm{-1}}$
XCustomMajorTicks=1  $\,$  2   $N_{\textrm{lep,base}}=0$  3  $\Delta\phi^{4j}_{\textrm{min}}\geq0.6$  4  $P(\textrm{Gbb})\geq0.999$
XMajorTicksAngle=-20

END PLOT


########## Gtt Cutflows
BEGIN PLOT /ATLAS_2022_I2182381/CF_Gtt_2100_1
Title=Gtt 0L NN 2100,1 SR cutflow
YLabel=Events at 139 fb$^{\textrm{-1}}$

XCustomMajorTicks=1  $\,$  2  $N_{\textrm{lep,base}}\geq1\;\textrm{or}$\newline$\left(N_{\textrm{lep,base}}=0\;\textrm{\&}\;\Delta\phi^{4j}_{\textrm{min}}\geq0.4\right)$  3  $P(\textrm{Gtt})\geq0.9997$

END PLOT

BEGIN PLOT /ATLAS_2022_I2182381/CF_Gtt_1800_1
Title=Gtt 0L NN 1800,1 SR cutflow
YLabel=Events at 139 fb$^{\textrm{-1}}$
XCustomMajorTicks=1  $\,$  2  $N_{\textrm{lep,base}}\geq1\;\textrm{or}$\newline$\left(N_{\textrm{lep,base}}=0\;\textrm{\&}\;\Delta\phi^{4j}_{\textrm{min}}\geq0.4\right)$  3  $P(\textrm{Gtt})\geq0.9997$

END PLOT

BEGIN PLOT /ATLAS_2022_I2182381/CF_Gtt_2300_1200
Title=Gtt 0L NN 2300,1200 SR cutflow
YLabel=Events at 139 fb$^{\textrm{-1}}$
XCustomMajorTicks=1  $\,$  2  $N_{\textrm{lep,base}}\geq1\;\textrm{or}$\newline$\left(N_{\textrm{lep,base}}=0\;\textrm{\&}\;\Delta\phi^{4j}_{\textrm{min}}\geq0.4\right)$  3  $P(\textrm{Gtt})\geq0.9993$

END PLOT

BEGIN PLOT /ATLAS_2022_I2182381/CF_Gtt_1900_1400
Title=Gtt 0L NN 1900,1400 SR cutflow
YLabel=Events at 139 fb$^{\textrm{-1}}$
XCustomMajorTicks=1  $\,$  2  $N_{\textrm{lep,base}}\geq1\;\textrm{or}$\newline$\left(N_{\textrm{lep,base}}=0\;\textrm{\&}\;\Delta\phi^{4j}_{\textrm{min}}\geq0.4\right)$  3  $P(\textrm{Gtt})\geq0.9987$

END PLOT