BEGIN PLOT /H1_1996_I421105/d04-x01-y01
Title=Normalized Differential Cross Section for $D^*$
XLabel=$p_T$ (GeV)
YLabel=${1/\sigma}   {d\sigma}/{d{p_T}} (GeV^{-1})$ 
END PLOT

BEGIN PLOT /H1_1996_I421105/d04-x01-y02
Title=Differential Cross Section  for $D^*$
XLabel=$p_T$ (GeV)
YLabel=$ {d\sigma}/{d{p_T}} (GeV^{-1})$ 
END PLOT

BEGIN PLOT /H1_1996_I421105/d05-x01-y01
Title=Normalized Differential Cross Section  for $D^0$
XLabel=$p_T$ (GeV)
YLabel=${1/\sigma}  {d\sigma}/{d{p_T}} (GeV^{-1})$ 
END PLOT

BEGIN PLOT /H1_1996_I421105/d05-x01-y02
Title=Differential Cross Section  for $D^0$
XLabel=$p_T$ (GeV)]
YLabel=${d\sigma}/{d{p_T}} (GeV^{-1})$ 
END PLOT

BEGIN PLOT /H1_1996_I421105/d06-x01-y01
Title=Normalized $x_D$ Distribution for $D^*$
XLabel=$x_D$  
YLabel=${1/\sigma}  {d\sigma}/{d{x_D}} $ 
END PLOT

BEGIN PLOT /H1_1996_I421105/d06-x01-y02
Title=$x_D$ Distribution for $D^*$
XLabel=$x_D$  
YLabel=${d\sigma}/{d{x_D}}$ 
END PLOT

BEGIN PLOT /H1_1996_I421105/d07-x01-y01
Title=Normalized $x_D$ Distribution  for $D^0$
XLabel=$x_D$  
YLabel=${1/\sigma}  {d\sigma}/{d{x_D}}$ 
END PLOT

BEGIN PLOT /H1_1996_I421105/d07-x01-y02
Title=$x_D$ Distribution  for $D^0$
XLabel=$x_D$ 
YLabel=${d\sigma}/{d{x_D}} $ 
END PLOT

BEGIN PLOT /H1_1996_I421105/d01-x01-y01
Title=[Insert title for histogram d01-x01-y01 here]
XLabel=[Insert $x$-axis label for histogram d01-x01-y01 here]
YLabel=[Insert $y$-axis label for histogram d01-x01-y01 here]
END PLOT
