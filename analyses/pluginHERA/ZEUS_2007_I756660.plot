BEGIN PLOT /ZEUS_2007_I756660/d01-x01-y01
Title=3-jet cross section as a function of $\mathrm{M}_{3j}$
XLabel=$\mathrm{M}_{3j}$(GeV)
YLabel=$\mathrm{d}\sigma/\mathrm{d}\mathrm{M}_{3j}$(pb/GeV)
END PLOT

BEGIN PLOT /ZEUS_2007_I756660/d02-x01-y01
Title=4-jet cross section as a function of $\mathrm{M}_{4j}$
XLabel=$\mathrm{M}_{4j}$(GeV)
YLabel=$\mathrm{d}\sigma/\mathrm{d}\mathrm{M}_{4j}$(pb/GeV)
END PLOT

BEGIN PLOT /ZEUS_2007_I756660/d03-x01-y01
Title=Cross section as a function of $x_{\gamma}^{obs}$ for $25 \leq \mathrm{M}_{3j} \leq 50$ GeV
XLabel=$x_{\gamma}^{obs}$
YLabel=$\mathrm{d}\sigma/\mathrm{d}x_{\gamma}^{obs}$(pb)
END PLOT

BEGIN PLOT /ZEUS_2007_I756660/d03-x01-y02
Title=Cross section as a function of $x_{\gamma}^{obs}$ for $\mathrm{M}_{3j} \geq 50$ GeV
XLabel=$x_{\gamma}^{obs}$
YLabel=$\mathrm{d}\sigma/\mathrm{d}x_{\gamma}^{obs}$(pb)
END PLOT

BEGIN PLOT /ZEUS_2007_I756660/d04-x01-y01
Title=Cross section as a function of $x_{\gamma}^{obs}$ for $25 \leq \mathrm{M}_{4j} \leq 50$ GeV
XLabel=$x_{\gamma}^{obs}$
YLabel=$\mathrm{d}\sigma/\mathrm{d}x_{\gamma}^{obs}$(pb)
END PLOT

BEGIN PLOT /ZEUS_2007_I756660/d04-x01-y02
Title=Cross section as a function of $x_{\gamma}^{obs}$ for $\mathrm{M}_{4j} \geq 50$ GeV
XLabel=$x_{\gamma}^{obs}$
YLabel=$\mathrm{d}\sigma/\mathrm{d}x_{\gamma}^{obs}$(pb)
END PLOT

BEGIN PLOT /ZEUS_2007_I756660/d05-x01-y01
Title=Cross section as a function of $y$ for $25 \leq \mathrm{M}_{3j} \leq 50$ GeV
XLabel=$y$
YLabel=$\mathrm{d}\sigma/\mathrm{d}y$(pb)
END PLOT

BEGIN PLOT /ZEUS_2007_I756660/d05-x01-y02
Title=Cross section as a function of $y$ for $\mathrm{M}_{3j} \geq 50$ GeV
XLabel=$y$
YLabel=$\mathrm{d}\sigma/\mathrm{d}y$(pb)
END PLOT

BEGIN PLOT /ZEUS_2007_I756660/d06-x01-y01
Title=Cross section as a function of $y$ for $25 \leq \mathrm{M}_{4j} \leq 50$ GeV
XLabel=$y$
YLabel=$\mathrm{d}\sigma/\mathrm{d}y$(pb)
END PLOT

BEGIN PLOT /ZEUS_2007_I756660/d06-x01-y02
Title=Cross section as a function of $y$ for $\mathrm{M}_{4j} \geq 50$ GeV
XLabel=$y$
YLabel=$\mathrm{d}\sigma/\mathrm{d}y$(pb)
END PLOT

BEGIN PLOT /ZEUS_2007_I756660/d07-x01-y01
Title=Cross section as a function of $\mathrm{E}_{\mathrm{T}}^{\mathrm{jet1}}$ for $25 \leq \mathrm{M}_{3j} \leq 50$ GeV
XLabel=$\mathrm{E}_{\mathrm{T}}^{\mathrm{jet1}}$ (GeV)
YLabel=$\mathrm{d}\sigma/\mathrm{d}\mathrm{E}_{\mathrm{T}}^{\mathrm{jet1}}$(pb/GeV)
END PLOT

BEGIN PLOT /ZEUS_2007_I756660/d08-x01-y01
Title=Cross section as a function of $\mathrm{E}_{\mathrm{T}}^{\mathrm{jet2}}$ for $25 \leq \mathrm{M}_{3j} \leq 50$ GeV
XLabel=$\mathrm{E}_{\mathrm{T}}^{\mathrm{jet2}}$ (GeV)
YLabel=$\mathrm{d}\sigma/\mathrm{d}\mathrm{E}_{\mathrm{T}}^{\mathrm{jet2}}$(pb/GeV)
END PLOT

BEGIN PLOT /ZEUS_2007_I756660/d09-x01-y01
Title=Cross section as a function of $\mathrm{E}_{\mathrm{T}}^{\mathrm{jet3}}$ for $25 \leq \mathrm{M}_{3j} \leq 50$ GeV
XLabel=$\mathrm{E}_{\mathrm{T}}^{\mathrm{jet3}}$ (GeV)
YLabel=$\mathrm{d}\sigma/\mathrm{d}\mathrm{E}_{\mathrm{T}}^{\mathrm{jet3}}$(pb/GeV)
END PLOT

BEGIN PLOT /ZEUS_2007_I756660/d07-x01-y02
Title=Cross section as a function of $\mathrm{E}_{\mathrm{T}}^{\mathrm{jet1}}$ for $\mathrm{M}_{3j} \geq 50$ GeV
XLabel=$\mathrm{E}_{\mathrm{T}}^{\mathrm{jet1}}$ (GeV)
YLabel=$\mathrm{d}\sigma/\mathrm{d}\mathrm{E}_{\mathrm{T}}^{\mathrm{jet1}}$(pb/GeV)
END PLOT

BEGIN PLOT /ZEUS_2007_I756660/d08-x01-y02
Title=Cross section as a function of $\mathrm{E}_{\mathrm{T}}^{\mathrm{jet2}}$ for $\mathrm{M}_{3j} \geq 50$ GeV
XLabel=$\mathrm{E}_{\mathrm{T}}^{\mathrm{jet2}}$ (GeV)
YLabel=$\mathrm{d}\sigma/\mathrm{d}\mathrm{E}_{\mathrm{T}}^{\mathrm{jet2}}$(pb/GeV)
END PLOT

BEGIN PLOT /ZEUS_2007_I756660/d09-x01-y02
Title=Cross section as a function of $\mathrm{E}_{\mathrm{T}}^{\mathrm{jet3}}$ for $\mathrm{M}_{3j} \geq 50$ GeV
XLabel=$\mathrm{E}_{\mathrm{T}}^{\mathrm{jet3}}$ (GeV)
YLabel=$\mathrm{d}\sigma/\mathrm{d}\mathrm{E}_{\mathrm{T}}^{\mathrm{jet3}}$(pb/GeV)
END PLOT

BEGIN PLOT /ZEUS_2007_I756660/d10-x01-y01
Title=Cross section as a function of $\mathrm{E}_{\mathrm{T}}^{\mathrm{jet1}}$ for $25 \leq \mathrm{M}_{4j} \leq 50$ GeV
XLabel=$\mathrm{E}_{\mathrm{T}}^{\mathrm{jet1}}$ (GeV)
YLabel=$\mathrm{d}\sigma/\mathrm{d}\mathrm{E}_{\mathrm{T}}^{\mathrm{jet1}}$(pb/GeV)
END PLOT

BEGIN PLOT /ZEUS_2007_I756660/d11-x01-y01
Title=Cross section as a function of $\mathrm{E}_{\mathrm{T}}^{\mathrm{jet2}}$ for $25 \leq \mathrm{M}_{4j} \leq 50$ GeV
XLabel=$\mathrm{E}_{\mathrm{T}}^{\mathrm{jet2}}$ (GeV)
YLabel=$\mathrm{d}\sigma/\mathrm{d}\mathrm{E}_{\mathrm{T}}^{\mathrm{jet2}}$(pb/GeV)
END PLOT

BEGIN PLOT /ZEUS_2007_I756660/d12-x01-y01
Title=Cross section as a function of $\mathrm{E}_{\mathrm{T}}^{\mathrm{jet3}}$ for $25 \leq \mathrm{M}_{4j} \leq 50$ GeV
XLabel=$\mathrm{E}_{\mathrm{T}}^{\mathrm{jet3}}$ (GeV)
YLabel=$\mathrm{d}\sigma/\mathrm{d}\mathrm{E}_{\mathrm{T}}^{\mathrm{jet3}}$(pb/GeV)
END PLOT

BEGIN PLOT /ZEUS_2007_I756660/d13-x01-y01
Title=Cross section as a function of $\mathrm{E}_{\mathrm{T}}^{\mathrm{jet4}}$ for $25 \leq \mathrm{M}_{4j} \leq 50$ GeV
XLabel=$\mathrm{E}_{\mathrm{T}}^{\mathrm{jet4}}$ (GeV)
YLabel=$\mathrm{d}\sigma/\mathrm{d}\mathrm{E}_{\mathrm{T}}^{\mathrm{jet4}}$(pb/GeV)
END PLOT

BEGIN PLOT /ZEUS_2007_I756660/d10-x01-y02
Title=Cross section as a function of $\mathrm{E}_{\mathrm{T}}^{\mathrm{jet1}}$ for $\mathrm{M}_{4j} \geq 50$ GeV
XLabel=$\mathrm{E}_{\mathrm{T}}^{\mathrm{jet1}}$ (GeV)
YLabel=$\mathrm{d}\sigma/\mathrm{d}\mathrm{E}_{\mathrm{T}}^{\mathrm{jet1}}$(pb/GeV)
END PLOT

BEGIN PLOT /ZEUS_2007_I756660/d11-x01-y02
Title=Cross section as a function of $\mathrm{E}_{\mathrm{T}}^{\mathrm{jet2}}$ for $\mathrm{M}_{4j} \geq 50$ GeV
XLabel=$\mathrm{E}_{\mathrm{T}}^{\mathrm{jet2}}$ (GeV)
YLabel=$\mathrm{d}\sigma/\mathrm{d}\mathrm{E}_{\mathrm{T}}^{\mathrm{jet2}}$(pb/GeV)
END PLOT

BEGIN PLOT /ZEUS_2007_I756660/d12-x01-y02
Title=Cross section as a function of $\mathrm{E}_{\mathrm{T}}^{\mathrm{jet3}}$ for $\mathrm{M}_{4j} \geq 50$ GeV
XLabel=$\mathrm{E}_{\mathrm{T}}^{\mathrm{jet3}}$ (GeV)
YLabel=$\mathrm{d}\sigma/\mathrm{d}\mathrm{E}_{\mathrm{T}}^{\mathrm{jet3}}$(pb/GeV)
END PLOT

BEGIN PLOT /ZEUS_2007_I756660/d13-x01-y02
Title=Cross section as a function of $\mathrm{E}_{\mathrm{T}}^{\mathrm{jet4}}$ for $\mathrm{M}_{4j} \geq 50$ GeV
XLabel=$\mathrm{E}_{\mathrm{T}}^{\mathrm{jet4}}$ (GeV)
YLabel=$\mathrm{d}\sigma/\mathrm{d}\mathrm{E}_{\mathrm{T}}^{\mathrm{jet4}}$(pb/GeV)
END PLOT

BEGIN PLOT /ZEUS_2007_I756660/d14-x01-y01
Title=Cross section as a function of $\eta^{\mathrm{jet1}}$ for $25 \leq \mathrm{M}_{3j} \leq 50$ GeV
XLabel=$\eta^{\mathrm{jet1}}$
YLabel=$\mathrm{d}\sigma/\mathrm{d}\eta^{\mathrm{jet1}}$(pb)
END PLOT

BEGIN PLOT /ZEUS_2007_I756660/d15-x01-y01
Title=Cross section as a function of $\eta^{\mathrm{jet2}}$ for $25 \leq \mathrm{M}_{3j} \leq 50$ GeV
XLabel=$\eta^{\mathrm{jet2}}$
YLabel=$\mathrm{d}\sigma/\mathrm{d}\eta^{\mathrm{jet2}}$(pb)
END PLOT

BEGIN PLOT /ZEUS_2007_I756660/d16-x01-y01
Title=Cross section as a function of $\eta^{\mathrm{jet3}}$ for $25 \leq \mathrm{M}_{3j} \leq 50$ GeV
XLabel=$\eta^{\mathrm{jet3}}$
YLabel=$\mathrm{d}\sigma/\mathrm{d}\eta^{\mathrm{jet3}}$(pb)
END PLOT

BEGIN PLOT /ZEUS_2007_I756660/d14-x01-y02
Title=Cross section as a function of $\eta^{\mathrm{jet1}}$ for $\mathrm{M}_{3j} \geq 50$ GeV
XLabel=$\eta^{\mathrm{jet1}}$
YLabel=$\mathrm{d}\sigma/\mathrm{d}\eta^{\mathrm{jet1}}$(pb)
END PLOT

BEGIN PLOT /ZEUS_2007_I756660/d15-x01-y02
Title=Cross section as a function of $\eta^{\mathrm{jet2}}$ for $\mathrm{M}_{3j} \geq 50$ GeV
XLabel=$\eta^{\mathrm{jet2}}$
YLabel=$\mathrm{d}\sigma/\mathrm{d}\eta^{\mathrm{jet2}}$(pb)
END PLOT

BEGIN PLOT /ZEUS_2007_I756660/d16-x01-y02
Title=Cross section as a function of $\eta^{\mathrm{jet3}}$ for $\mathrm{M}_{3j} \geq 50$ GeV
XLabel=$\eta^{\mathrm{jet3}}$
YLabel=$\mathrm{d}\sigma/\mathrm{d}\eta^{\mathrm{jet3}}$(pb)
END PLOT

BEGIN PLOT /ZEUS_2007_I756660/d17-x01-y01
Title=Cross section as a function of $\eta^{\mathrm{jet1}}$ for $25 \leq \mathrm{M}_{4j} \leq 50$ GeV
XLabel=$\eta^{\mathrm{jet1}}$
YLabel=$\mathrm{d}\sigma/\mathrm{d}\eta^{\mathrm{jet1}}$(pb)
END PLOT

BEGIN PLOT /ZEUS_2007_I756660/d18-x01-y01
Title=Cross section as a function of $\eta^{\mathrm{jet2}}$ for $25 \leq \mathrm{M}_{4j} \leq 50$ GeV
XLabel=$\eta^{\mathrm{jet2}}$
YLabel=$\mathrm{d}\sigma/\mathrm{d}\eta^{\mathrm{jet2}}$(pb)
END PLOT

BEGIN PLOT /ZEUS_2007_I756660/d19-x01-y01
Title=Cross section as a function of $\eta^{\mathrm{jet3}}$ for $25 \leq \mathrm{M}_{4j} \leq 50$ GeV
XLabel=$\eta^{\mathrm{jet3}}$
YLabel=$\mathrm{d}\sigma/\mathrm{d}\eta^{\mathrm{jet3}}$(pb)
END PLOT

BEGIN PLOT /ZEUS_2007_I756660/d20-x01-y01
Title=Cross section as a function of $\eta^{\mathrm{jet4}}$ for $25 \leq \mathrm{M}_{4j} \leq 50$ GeV
XLabel=$\eta^{\mathrm{jet4}}$
YLabel=$\mathrm{d}\sigma/\mathrm{d}\eta^{\mathrm{jet4}}$(pb)
END PLOT

BEGIN PLOT /ZEUS_2007_I756660/d17-x01-y02
Title=Cross section as a function of $\eta^{\mathrm{jet1}}$ for $\mathrm{M}_{4j} \geq 50$ GeV
XLabel=$\eta^{\mathrm{jet1}}$
YLabel=$\mathrm{d}\sigma/\mathrm{d}\eta^{\mathrm{jet1}}$(pb)
END PLOT

BEGIN PLOT /ZEUS_2007_I756660/d18-x01-y02
Title=Cross section as a function of $\eta^{\mathrm{jet2}}$ for $\mathrm{M}_{4j} \geq 50$ GeV
XLabel=$\eta^{\mathrm{jet2}}$
YLabel=$\mathrm{d}\sigma/\mathrm{d}\eta^{\mathrm{jet2}}$(pb)
END PLOT

BEGIN PLOT /ZEUS_2007_I756660/d19-x01-y02
Title=Cross section as a function of $\eta^{\mathrm{jet3}}$ for $\mathrm{M}_{4j} \geq 50$ GeV
XLabel=$\eta^{\mathrm{jet3}}$
YLabel=$\mathrm{d}\sigma/\mathrm{d}\eta^{\mathrm{jet3}}$(pb)
END PLOT

BEGIN PLOT /ZEUS_2007_I756660/d20-x01-y02
Title=Cross section as a function of $\eta^{\mathrm{jet4}}$ for $\mathrm{M}_{4j} \geq 50$ GeV
XLabel=$\eta^{\mathrm{jet4}}$
YLabel=$\mathrm{d}\sigma/\mathrm{d}\eta^{\mathrm{jet4}}$(pb)
END PLOT

BEGIN PLOT /ZEUS_2007_I756660/d21-x01-y01
Title=Cross section as a function of $\mathrm{cos}(\phi_3)$ for $25 \leq \mathrm{M}_{3j} \leq 50$ GeV
XLabel=$\mathrm{cos}(\phi_3)$
YLabel=$\mathrm{d}\sigma/\mathrm{d}\mathrm{cos}(\phi_3)$(pb)
YMax=20e4
END PLOT

BEGIN PLOT /ZEUS_2007_I756660/d21-x01-y02
Title=Cross section as a function of $\mathrm{cos}(\phi_3)$ for $\mathrm{M}_{3j} \geq 50$ GeV
XLabel=$\mathrm{cos}(\phi_3)$
YLabel=$\mathrm{d}\sigma/\mathrm{d}\mathrm{cos}(\phi_3)$(pb)
END PLOT

