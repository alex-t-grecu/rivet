BEGIN PLOT /BESIII_2022_I1900094/d01-x01-y01
Title=$\omega$ heilcity angle in $D^0\to\omega\phi$
XLabel=$|\cos\theta_\omega|$
YLabel=$1/\Gamma\mathrm{d} \Gamma/\mathrm{d}|\cos\theta_\omega|$
LogY=0
END PLOT
BEGIN PLOT /BESIII_2022_I1900094/d01-x01-y02
Title=$\phi$ heilcity angle in $D^0\to\omega\phi$
XLabel=$|\cos\theta_\phi|$
YLabel=$1/\Gamma\mathrm{d} \Gamma/\mathrm{d}|\cos\theta_\phi|$
LogY=0
END PLOT
