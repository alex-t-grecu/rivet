BEGIN PLOT /BESII_2004_I650570/d01-x01-y01
Title=$m_{p\bar\Lambda^0}$ mass distribution in $J/\psi\to pK^-\bar\Lambda^0$ + c.c.
XLabel=$m_{p\bar\Lambda^0}$ [$\mathrm{GeV}$]
YLabel=$1/\Gamma\mathrm{d} \Gamma/\mathrm{d}m_{p\bar\Lambda^0}$ [$\mathrm{GeV}^{-1}$]
LogY=0
END PLOT
BEGIN PLOT /BESII_2004_I650570/d01-x01-y02
Title=$m_{p\bar\Lambda^0}-m_p-m_\Lambda$ mass distribution in $J/\psi\to pK^-\bar\Lambda^0$ + c.c.
XLabel=$m_{p\bar\Lambda^0}-m_p-m_\Lambda$ [$\mathrm{GeV}$]
YLabel=$1/\Gamma\mathrm{d} \Gamma/\mathrm{d}m_{p\bar\Lambda^0}$ [$\mathrm{GeV}^{-1}$]
LogY=0
END PLOT
BEGIN PLOT /BESII_2004_I650570/d01-x01-y03
Title=Proton helicty angle in $J/\psi\to pK^-\bar\Lambda^0$ + c.c. ($m_{p\bar\Lambda^0}-m_p-m_\Lambda<0.15\,$GeV)
XLabel=$\cos\theta_p$
YLabel=$\text{d}\mathcal{B}/\text{d}\cos\theta_p\times10^6$
LogY=0
END PLOT
