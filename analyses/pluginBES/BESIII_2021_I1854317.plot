BEGIN PLOT /BESIII_2021_I1854317/d01-x01-y03
Title=$\pi^+\pi^0$ mass distribution in $D_s^+\to \pi^+\pi^0K^0_S$
XLabel=$m^2_{\pi^+\pi^0}$ [$\mathrm{GeV}^{2}$]
YLabel=$1/\Gamma\mathrm{d} \Gamma/\mathrm{d}m^2_{\pi^+\pi^0}$ [$\mathrm{GeV}^{-2}$]
LogY=0
END PLOT
BEGIN PLOT /BESIII_2021_I1854317/d01-x01-y02
Title=$K^0_S\pi^+$ mass distribution in $D_s^+\to K^0_S\pi^+K^0_S$
XLabel=$m^2_{K^0_S\pi^+}$ [$\mathrm{GeV}^{2}$]
YLabel=$1/\Gamma\mathrm{d} \Gamma/\mathrm{d}m^2_{K^0_S\pi^+}$ [$\mathrm{GeV}^{-2}$]
LogY=0
END PLOT
BEGIN PLOT /BESIII_2021_I1854317/d01-x01-y01
Title=$K^0_S\pi^0$ mass distribution in $D_s^+\to \pi^+\pi^0K^0_S$
XLabel=$m^2_{K^0_S\pi^0}$ [$\mathrm{GeV}^{2}$]
YLabel=$1/\Gamma\mathrm{d} \Gamma/\mathrm{d}m^2_{K^0_S\pi^0}$ [$\mathrm{GeV}^{-2}$]
LogY=0
END PLOT
BEGIN PLOT /BESIII_2021_I1854317/dalitz
Title=Dalitz plot for $D_s^+\to \pi^+\pi^0K^0_S$
YLabel=$m^2_{\pi^+K^0_S}$ [$\mathrm{GeV}^2$]
XLabel=$m^2_{\pi^0K^0_S}$ [$\mathrm{GeV}^2$]
ZLabel=$1/\Gamma{\rm d}^2 \Gamma/{\rm d}m^2_{\pi^0K^0_S}/{\rm d}m^2_{\pi^+K^0_S}$ [$\rm{GeV}^{-4}$]
LogY=0
END PLOT
