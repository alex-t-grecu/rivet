BEGIN PLOT /BESIII_2023_I2674370/d01-x01-y01
Title=$\sigma(e^+e^-\to pK^-\bar\Lambda^0 +\text{c.c.})$
XLabel=$\sqrt{s}$/GeV
YLabel=$\sigma(e^+e^-\to pK^-\bar\Lambda^0 +\text{c.c.}$/pb
ConnectGaps=1
LogY=0
END PLOT
