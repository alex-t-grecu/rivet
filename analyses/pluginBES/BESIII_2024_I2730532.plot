BEGIN PLOT /BESIII_2024_I2730532/
LogY=0
END PLOT

BEGIN PLOT /BESIII_2024_I2730532/d01-x01-y01
Title=$\pi^+\pi^-$ mass in $D^0\to2\pi^+2\pi^-$
XLabel=$m_{\pi^+\pi^-}$ [GeV]
YLabel=$1/\Gamma\mathrm{d}\Gamma/\mathrm{d}m_{\pi^+\pi^-}$ [$\mathrm{GeV}^{-1}$]
END PLOT
BEGIN PLOT /BESIII_2024_I2730532/d01-x01-y02
Title=$\pi^+\pi^+\pi^-$ mass in $D^0\to2\pi^+2\pi^-$
XLabel=$m_{\pi^+\pi^+\pi^-}$ [GeV]
YLabel=$1/\Gamma\mathrm{d}\Gamma/\mathrm{d}m_{\pi^+\pi^+\pi^-}$ [$\mathrm{GeV}^{-1}$]
END PLOT
BEGIN PLOT /BESIII_2024_I2730532/d01-x01-y03
Title=$\pi^-\pi^-\pi^+$ mass in $D^0\to2\pi^+2\pi^-$
XLabel=$m_{\pi^-\pi^-\pi^+}$ [GeV]
YLabel=$1/\Gamma\mathrm{d}\Gamma/\mathrm{d}m_{\pi^-\pi^-\pi^+}$ [$\mathrm{GeV}^{-1}$]
END PLOT

BEGIN PLOT /BESIII_2024_I2730532/d02-x01-y01
Title=$\pi^+\pi^-\pi^0$ mass in $D^0\to\pi^+\pi^-2\pi^0$
XLabel=$m_{\pi^+\pi^-\pi^0}$ [GeV]
YLabel=$1/\Gamma\mathrm{d}\Gamma/\mathrm{d}m_{\pi^+\pi^-\pi^0}$ [$\mathrm{GeV}^{-1}$]
END PLOT
BEGIN PLOT /BESIII_2024_I2730532/d02-x01-y02
Title=$\pi^+\pi^0\pi^0$ mass in $D^0\to\pi^+\pi^-2\pi^0$
XLabel=$m_{\pi^+\pi^0\pi^0}$ [GeV]
YLabel=$1/\Gamma\mathrm{d}\Gamma/\mathrm{d}m_{\pi^+\pi^0\pi^0}$ [$\mathrm{GeV}^{-1}$]
END PLOT
BEGIN PLOT /BESIII_2024_I2730532/d02-x01-y03
Title=$\pi^-\pi^0\pi^0$ mass in $D^0\to\pi^+\pi^-2\pi^0$
XLabel=$m_{\pi^-\pi^0\pi^0}$ [GeV]
YLabel=$1/\Gamma\mathrm{d}\Gamma/\mathrm{d}m_{\pi^-\pi^0\pi^0}$ [$\mathrm{GeV}^{-1}$]
END PLOT
BEGIN PLOT /BESIII_2024_I2730532/d02-x01-y04
Title=$\pi^+\pi^-$ mass in $D^0\to\pi^+\pi^-2\pi^0$
XLabel=$m_{\pi^+\pi^-}$ [GeV]
YLabel=$1/\Gamma\mathrm{d}\Gamma/\mathrm{d}m_{\pi^+\pi^-}$ [$\mathrm{GeV}^{-1}$]
END PLOT
BEGIN PLOT /BESIII_2024_I2730532/d02-x01-y05
Title=$\pi^0\pi^0$ mass in $D^0\to\pi^+\pi^-2\pi^0$
XLabel=$m_{\pi^0\pi^0}$ [GeV]
YLabel=$1/\Gamma\mathrm{d}\Gamma/\mathrm{d}m_{\pi^0\pi^0}$ [$\mathrm{GeV}^{-1}$]
END PLOT
BEGIN PLOT /BESIII_2024_I2730532/d02-x01-y06
Title=$\pi^+\pi^0$ mass in $D^0\to\pi^+\pi^-2\pi^0$
XLabel=$m_{\pi^+\pi^0}$ [GeV]
YLabel=$1/\Gamma\mathrm{d}\Gamma/\mathrm{d}m_{\pi^+\pi^0}$ [$\mathrm{GeV}^{-1}$]
END PLOT
BEGIN PLOT /BESIII_2024_I2730532/d02-x01-y07
Title=$\pi^-\pi^0$ mass in $D^0\to\pi^+\pi^-2\pi^0$
XLabel=$m_{\pi^-\pi^0}$ [GeV]
YLabel=$1/\Gamma\mathrm{d}\Gamma/\mathrm{d}m_{\pi^-\pi^0}$ [$\mathrm{GeV}^{-1}$]
END PLOT
