BEGIN PLOT /BESIII_2024_I2824143/d01-x01-y01
Title=$\sigma(e^+e^-\to \Xi^0\bar{\Xi}^0)$
XLabel=$\sqrt{s}$ [GeV]
YLabel=$\sigma(e^+e^-\to \Xi^0\bar{\Xi}^0)$ [fb]
ConnectGaps=1
LogY=0
END PLOT
