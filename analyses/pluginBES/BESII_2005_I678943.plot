BEGIN PLOT /BESII_2005_I678943/d01-x01-y01
Title=$\pi^+\pi^-$ mass in $J/\psi\to2\pi^+2\pi^-$
XLabel=$m_{\pi^+\pi^-}$ [GeV]
YLabel=$1/\Gamma\text{d}\Gamma/\text{d}m_{\pi^+\pi^-}$ [$\text{GeV}^{-1}$]
LogY=0
END PLOT
