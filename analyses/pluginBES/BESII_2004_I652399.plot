BEGIN PLOT /BESII_2004_I652399/d01-x01-y01
Title=$\pi^+\pi^-$ mass in $J/\psi\to\omega \pi^+\pi^-$
XLabel=$m_{\pi^+\pi^-}$ [GeV]
YLabel=$1/\Gamma\mathrm{d}\Gamma\mathrm{d}m_{\pi^+\pi^-}$ [$\mathrm{GeV}^{-1}$]
LogY=0
END PLOT
BEGIN PLOT /BESII_2004_I652399/d01-x01-y02
Title=$\omega \pi$ mass in $J/\psi\to\omega \pi^+\pi^-$
XLabel=$m_{\omega \pi}$ [GeV]
YLabel=$1/\Gamma\mathrm{d}\Gamma\mathrm{d}m_{\omega \pi}$ [$\mathrm{GeV}^{-1}$]
LogY=0
END PLOT
BEGIN PLOT /BESII_2004_I652399/d02-x01-y01
Title=Angle between decay planes in $J/\psi\to\omega \pi^+\pi^-$ ($0.3<m_{\pi^+\pi^+}<0.5\,$GeV)
XLabel=$\chi$
YLabel=$1/\Gamma\mathrm{d}\Gamma\mathrm{d}\chi$
LogY=0
END PLOT
BEGIN PLOT /BESII_2004_I652399/d02-x02-y01
Title=$\omega$ polar angle in $J/\psi\to\omega \pi^+\pi^-$ ($0.3<m_{\pi^+\pi^+}<0.5\,$GeV)
XLabel=$\cos\theta_\omega$
YLabel=$1/\Gamma\mathrm{d}\Gamma\mathrm{d}\cos\theta_\omega$
LogY=0
END PLOT
BEGIN PLOT /BESII_2004_I652399/d02-x03-y01
Title=Pion decay angle in $J/\psi\to\omega \pi^+\pi^-$ ($0.3<m_{\pi^+\pi^+}<0.5\,$GeV)
XLabel=$\cos\alpha_\pi$
YLabel=$1/\Gamma\mathrm{d}\Gamma\mathrm{d}\cos\alpha_\pi$
LogY=0
END PLOT
BEGIN PLOT /BESII_2004_I652399/d02-x04-y01
Title=$\omega$ decay plane angle in $J/\psi\to\omega \pi^+\pi^-$ ($0.3<m_{\pi^+\pi^+}<0.5\,$GeV)
XLabel=$\cos\beta_\omega$
YLabel=$1/\Gamma\mathrm{d}\Gamma\mathrm{d}\cos\beta_\omega$
LogY=0
END PLOT

BEGIN PLOT /BESII_2004_I652399/d02-x01-y02
Title=Angle between decay planes in $J/\psi\to\omega \pi^+\pi^-$ ($0.5<m_{\pi^+\pi^+}<0.7\,$GeV)
XLabel=$\chi$
YLabel=$1/\Gamma\mathrm{d}\Gamma\mathrm{d}\chi$
LogY=0
END PLOT
BEGIN PLOT /BESII_2004_I652399/d02-x02-y02
Title=$\omega$ polar angle in $J/\psi\to\omega \pi^+\pi^-$ ($0.5<m_{\pi^+\pi^+}<0.7\,$GeV)
XLabel=$\cos\theta_\omega$
YLabel=$1/\Gamma\mathrm{d}\Gamma\mathrm{d}\cos\theta_\omega$
LogY=0
END PLOT
BEGIN PLOT /BESII_2004_I652399/d02-x03-y02
Title=Pion decay angle in $J/\psi\to\omega \pi^+\pi^-$ ($0.5<m_{\pi^+\pi^+}<0.7\,$GeV)
XLabel=$\cos\alpha_\pi$
YLabel=$1/\Gamma\mathrm{d}\Gamma\mathrm{d}\cos\alpha_\pi$
LogY=0
END PLOT
BEGIN PLOT /BESII_2004_I652399/d02-x04-y02
Title=$\omega$ decay plane angle in $J/\psi\to\omega \pi^+\pi^-$ ($0.5<m_{\pi^+\pi^+}<0.7\,$GeV)
XLabel=$\cos\beta_\omega$
YLabel=$1/\Gamma\mathrm{d}\Gamma\mathrm{d}\cos\beta_\omega$
LogY=0
END PLOT
