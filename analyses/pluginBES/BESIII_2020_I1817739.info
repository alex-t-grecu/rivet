Name: BESIII_2020_I1817739
Year: 2020
Summary: Cross section for $e^+e^-\to\omega\pi^0$ and $\omega\eta$ for centre-of-mass energies between 2.00 and 3.08 GeV
Experiment: BESIII
Collider: BEPC
InspireID: 1817739
Status: VALIDATED
Authors:
 - Peter Richardson <peter.richardson@durham.ac.uk>
References:
 - arXiv:2009.08099
RunInfo: e+ e- to hadrons
Beams: [e+, e-]
Energies:  [2.0000, 2.0500, 2.1000, 2.1250, 2.1500, 2.1750,
            2.2000, 2.2324, 2.3094, 2.3864, 2.3960, 2.5000,
            2.6444, 2.6464, 2.7000, 2.8000, 2.9000, 2.9500,
            2.9810, 3.0000, 3.0200, 3.0800]
Options:
 - ENERGY=*
Description:
  'Measurement of the cross section for $e^+e^-\to\omega\pi^0$ and $\omega\eta$ for centre-of-mass energies between 2.00 and 3.08 GeV by the BESIII collaboration.
   Beam energy must be specified as analysis option "ENERGY" when rivet-merging samples.'
ValidationInfo:
  'Herwig 7 events'
#ReleaseTests:
# - $A my-hepmc-prefix :MODE=some_rivet_flag
Keywords: []
BibKey: Ablikim:2020das
BibTeX: '@article{Ablikim:2020das,
    author = "Ablikim, M. and others",
    collaboration = "BESIII",
    title = "{Observation of a resonant structure in $e^{+}e^{-} \to \omega\eta$ and another in $e^{+}e^{-} \to \omega\pi^{0}$ at center-of-mass energies between 2.00 and 3.08 GeV}",
    eprint = "2009.08099",
    archivePrefix = "arXiv",
    primaryClass = "hep-ex",
    month = "9",
    year = "2020"
}'
