BEGIN PLOT /BESIII_2022_I2169640/d01-x01-y01
Title=Cross section for $e^+e^-\to \phi \chi_{c1}$
XLabel=$\sqrt{s}$ [GeV]
YLabel=$\sigma(e^+e^-\to \phi \chi_{c1})$ [pb]
LogY=0
ConnectGaps=1
END PLOT
BEGIN PLOT /BESIII_2022_I2169640/d01-x01-y02
Title=Cross section for $e^+e^-\to \phi \chi_{c2}$
XLabel=$\sqrt{s}$ [GeV]
YLabel=$\sigma(e^+e^-\to \phi \chi_{c2})$ [pb]
LogY=0
ConnectGaps=1
END PLOT
