BEGIN PLOT /BESIII_2018_I1623555/d01-x01-y01
Title=$X$ distribution for $\eta^\prime\to\eta\pi^+\pi^-$
XLabel=$X$
YLabel=$1/\Gamma\text{d}\Gamma/\text{d}X$
LogY=0
END PLOT
BEGIN PLOT /BESIII_2018_I1623555/d01-x01-y02
Title=$Y$ distribution for $\eta^\prime\to\eta\pi^+\pi^-$
XLabel=$Y$
YLabel=$1/\Gamma\text{d}\Gamma/\text{d}Y$
LogY=0
END PLOT
BEGIN PLOT /BESIII_2018_I1623555/d02-x01-y01
Title=$X$ distribution for $\eta^\prime\to\eta\pi^0\pi^0$
XLabel=$X$
YLabel=$1/\Gamma\text{d}\Gamma/\text{d}X$
LogY=0
END PLOT
BEGIN PLOT /BESIII_2018_I1623555/d02-x01-y02
Title=$Y$ distribution for $\eta^\prime\to\eta\pi^0\pi^0$
XLabel=$Y$
YLabel=$1/\Gamma\text{d}\Gamma/\text{d}Y$
LogY=0
END PLOT


BEGIN PLOT /BESIII_2018_I1623555/d03-x01-y01
Title=Ratio of $m_{\pi^0\pi^0}$ distribution to phase-space for $\eta^\prime\to\eta\pi^0\pi^0$
XLabel=$m_{\pi^0\pi^0}$ [$\text{GeV}$]
YLabel=$1/\Gamma\text{d}\Gamma/\text{d}m_{\pi^0\pi^0}/\text{PHSP}$
LogY=0
END PLOT
BEGIN PLOT /BESIII_2018_I1623555/d04-x01-y01
Title=Ratio of $m_{\eta\pi^0}$ distribution to phase-space for $\eta^\prime\to\eta\pi^0\pi^0$
XLabel=$m_{\eta\pi^0}$ [$\text{GeV}$]
YLabel=$1/\Gamma\text{d}\Gamma/\text{d}m_{\eta\pi^0}/\text{PHSP}$
LogY=0
END PLOT

BEGIN PLOT /BESIII_2018_I1623555/d03-x01-y02
Title=Ratio of $m_{\pi^+\pi^+}$ distribution to phase-space for $\eta^\prime\to\eta\pi^+\pi^+$
XLabel=$m_{\pi^+\pi^+}$ [$\text{GeV}$]
YLabel=$1/\Gamma\text{d}\Gamma/\text{d}m_{\pi^+\pi^+}/\text{PHSP}$
LogY=0
END PLOT
BEGIN PLOT /BESIII_2018_I1623555/d04-x01-y02
Title=Ratio of $m_{\eta\pi^\pm}$ distribution to phase-space for $\eta^\prime\to\eta\pi^+\pi^+$
XLabel=$m_{\eta\pi^\pm}$ [$\text{GeV}$]
YLabel=$1/\Gamma\text{d}\Gamma/\text{d}m_{\eta\pi^\pm}/\text{PHSP}$
LogY=0
END PLOT
