BEGIN PLOT /BESIII_2022_I2127373
LogY=0
END PLOT
BEGIN PLOT /BESIII_2022_I2127373/d01-x01-y01
Title=$e^+\nu_e$ mass squared in $\Lambda_c+\to \Lambda^0(p\pi^-)e^+\nu_e$
XLabel=$q^2$~[GeV$^2$]
YLabel=$1/\Gamma\mathrm{d}\Gamma/\mathrm{d}q^2$ [$\mathrm{GeV}^{-2}$]
END PLOT

BEGIN PLOT /BESIII_2022_I2127373/d01-x01-y02
Title=$\cos\theta_e$ in $\Lambda_c+\to \Lambda^0(p\pi^-)e^+\nu_e$
XLabel=$\cos\theta_e$
YLabel=$1/\Gamma\mathrm{d}\Gamma/\mathrm{d}\cos\theta_e$ 
END PLOT

BEGIN PLOT /BESIII_2022_I2127373/d01-x01-y03
Title=$\cos\theta_P$ in $\Lambda_c+\to \Lambda^0(p\pi^-)e^+\nu_e$
XLabel=$\cos\theta_P$
YLabel=$1/\Gamma\mathrm{d}\Gamma/\mathrm{d}\cos\theta_P$ 
END PLOT

BEGIN PLOT /BESIII_2022_I2127373/d01-x01-y04
Title=$\chi$ in $\Lambda_c+\to \Lambda^0(p\pi^-)e^+\nu_e$
XLabel=$\chi$ [rad]
YLabel=$1/\Gamma\mathrm{d}\Gamma/\mathrm{d}\chi$ [$\mathrm{rad}^{-1}$]
END PLOT
