Name: BESIII_2019_I1714778
Year: 2019
Summary: Mass distributions in the decay $D^+\to K^0_S\pi^+\pi^+\pi^-$
Experiment: BESIII
Collider: BEPC
InspireID: 1714778
Status: VALIDATED NOHEPDATA
Reentrant: true
Authors:
 - Peter Richardson <peter.richardson@durham.ac.uk>
References:
 - Phys.Rev.D 100 (2019) 7, 072008
Description:
  'Measurement of the mass distributions in the decay $D^+\to K^0_S\pi^+\pi^+\pi^-$ by BES. The data were read from the plots in the paper and therefore for some points the error bars are the size of the point. It is also not clear that any resolution effects have been unfolded.'
ValidationInfo:
  'Herwig 7 events'
#ReleaseTests:
# - $A my-hepmc-prefix :MODE=some_rivet_flag
Keywords: []
BibKey: BESIII:2019ymv
BibTeX: '@article{BESIII:2019ymv,
    author = "Ablikim, Medina and others",
    collaboration = "BESIII",
    title = "{Amplitude analysis of $D^{+} \rightarrow K_{S}^{0} \pi^{+} \pi^{+} \pi^{-}$}",
    eprint = "1901.05936",
    archivePrefix = "arXiv",
    primaryClass = "hep-ex",
    doi = "10.1103/PhysRevD.100.072008",
    journal = "Phys. Rev. D",
    volume = "100",
    number = "7",
    pages = "072008",
    year = "2019"
}
'
