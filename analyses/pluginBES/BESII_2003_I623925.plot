BEGIN PLOT /BESII_2003_I623925/d01-x01-y01
Title=$K^+K^-$ mass distribution in $J/\psi\to\gamma K^+K^-$
XLabel=$m_{K^+K^-}$ [GeV]
YLabel=$1/\Gamma\text{d}\Gamma/\text{d}m_{K^+K^-}$ [$\text{GeV}^{-1}$]
LogY=0
END PLOT
BEGIN PLOT /BESII_2003_I623925/d01-x01-y02
Title=$K^0_SK^0_S$ mass distribution in $J/\psi\to\gamma K^0_SK^0_S$
XLabel=$m_{K^0_SK^0_S}$ [GeV]
YLabel=$1/\Gamma\text{d}\Gamma/\text{d}m_{K^0_SK^0_S}$ [$\text{GeV}^{-1}$]
LogY=0
END PLOT
