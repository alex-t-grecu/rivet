BEGIN PLOT /BESII_2006_I717720/d01-x01-y01
Title=Hadronic cross section
XLabel=$\sqrt{s}$ [GeV]
YLabel=$\sigma$ [nb]
LogY=1
ConnectGaps=1
END PLOT
BEGIN PLOT /BESII_2006_I717720/d02-x01-y01
Title=$D\bar{D}$ Hadronic cross section
XLabel=$\sqrt{s}$ [GeV]
YLabel=$\sigma$ [nb]
LogY=0
ConnectGaps=1
END PLOT
BEGIN PLOT /BESII_2006_I717720/d02-x01-y02
Title=$D^0\bar{D}^0$ cross section
XLabel=$\sqrt{s}$ [GeV]
YLabel=$\sigma$ [nb]
LogY=0
ConnectGaps=1
END PLOT
BEGIN PLOT /BESII_2006_I717720/d02-x01-y03
Title=$D^+D^-$ cross section
XLabel=$\sqrt{s}$ [GeV]
YLabel=$\sigma$ [nb]
LogY=0
ConnectGaps=1
END PLOT
