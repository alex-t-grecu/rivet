BEGIN PLOT /BESIII_2014_I1318650/d01-x01-y01
Title=Cross section for $e^+e^-\to\pi^0\pi^0 h_c$
XLabel=$\sqrt{s}$ [GeV]
YLabel=$\sigma(e^+e^-\to\pi^0\pi^0 h_c)$ [pb]
LogY=0
ConnectGaps=1
END PLOT
BEGIN PLOT /BESIII_2014_I1318650/d01-x01-y02
Title=Cross section for $e^+e^-\to Z_c(4020)^0\pi^0\to\pi^0\pi^0 h_c$
XLabel=$\sqrt{s}$ [GeV]
YLabel=$\sigma(e^+e^-\to Z_c(4020)^0\pi^0\to\pi^0\pi^0 h_c)$ [pb]
LogY=0
ConnectGaps=1
END PLOT
