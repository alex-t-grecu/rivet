BEGIN PLOT /BESIII_2023_I2695411/d01-x01-y01
Title=$\sigma(e^+e^-\to \Xi^-\bar{\Xi}^+)$
XLabel=$\sqrt{s}$/GeV
YLabel=$\sigma(e^+e^-\to \Xi^-\bar{\Xi}^+)$/fb
ConnectGaps=1
END PLOT
BEGIN PLOT /BESIII_2023_I2695411/d01-x01-y02
Title=$F(s)$ for $e^+e^-\to \Xi^-\bar{\Xi}^+$
XLabel=$\sqrt{s}$/GeV
YLabel=$F(s)\times10^3$
ConnectGaps=1
END PLOT
