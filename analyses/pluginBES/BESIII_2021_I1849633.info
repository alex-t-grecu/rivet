Name: BESIII_2021_I1849633
Year: 2021
Summary: Cross Section for $e^+e^-\to\eta \psi(2S)$ for energies between 4.236 to 4.600 GeV
Experiment: BESIII
Collider: BEPC
InspireID: 1849633
Status: VALIDATED NOHEPDATA
Reentrant: false
Authors:
 - Peter Richardson <peter.richardson@durham.ac.uk>
References:
 - JHEP 10 (2021) 177
RunInfo: e+ e- to hadrons
Beams: [e+, e-]
Energies : [4.236, 4.242, 4.244, 4.258, 4.267, 4.278, 4.308, 4.358, 4.387, 4.416, 4.467, 4.527, 4.575, 4.6]
Options:
 - ENERGY=*
Description:
  'Cross section for  $e^+e^-\to\eta \psi(2S)$ for energies between 4.236 and 4.600 GeV measured by the BESIII collaboration.
   Beam energy must be specified as analysis option "ENERGY" when rivet-merging samples.'
ValidationInfo:
  'Herwig7 events'
#ReleaseTests:
# - $A my-hepmc-prefix :MODE=some_rivet_flag
Keywords: []
BibKey: BESIII:2021fae
BibTeX: '@article{BESIII:2021fae,
    author = "Ablikim, Medina and others",
    collaboration = "BESIII",
    title = "{Observation of $e^{+}e^{-}\rightarrow\eta\psi(2S)$~at center-of-mass energies from 4.236 to 4.600 GeV}",
    eprint = "2103.01480",
    archivePrefix = "arXiv",
    primaryClass = "hep-ex",
    doi = "10.1007/JHEP10(2021)177",
    journal = "JHEP",
    volume = "10",
    pages = "177",
    year = "2021"
}
'
