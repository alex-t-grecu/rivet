Name: BESIII_2019_I1749793
Year: 2019
Summary: $\phi$ momentum spectrum in $D\to\phi X$ decays
Experiment: BESIII
Collider: BEPC
InspireID: 1749793
Status: VALIDATED NOHEPDATA
Reentrant: true
Authors:
 - Peter Richardson <peter.richardson@durham.ac.uk>
References:
 - Phys.Rev.D 100 (2019) 7, 072006
RunInfo: e+e- to D Dbat at psi(3770)
Beams: [e+, e-]
Energies: [3.773]
Description:
  'Measurement of $\phi$ momentum spectrum in $D\to\phi X$ decays. The uncorrected data was read from the figures in the paper and the small background given subtracted.'
ValidationInfo:
  'Herwig7 events'
#ReleaseTests:
# - $A my-hepmc-prefix :MODE=some_rivet_flag
Keywords: []
BibKey: BESIII:2019zan
BibTeX: '@article{BESIII:2019zan,
    author = "Ablikim, Medina and others",
    collaboration = "BESIII",
    title = "{Improved measurements of the absolute branching fractions of the inclusive decays $D^{+(0)}\to\phi X$}",
    eprint = "1908.05401",
    archivePrefix = "arXiv",
    primaryClass = "hep-ex",
    doi = "10.1103/PhysRevD.100.072006",
    journal = "Phys. Rev. D",
    volume = "100",
    number = "7",
    pages = "072006",
    year = "2019"
}
'
