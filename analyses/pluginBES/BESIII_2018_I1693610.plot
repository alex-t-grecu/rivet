BEGIN PLOT /BESIII_2018_I1693610/d01-x01-y01
Title=$K^-\pi^+$ mass distribution in $D^0\to K^-\pi^+\eta^\prime$
XLabel=$m_{K^-\pi^+}$ [$\text{GeV}$]
YLabel=$1/\Gamma\text{d}\Gamma/\text{d}m_{K^-\pi^+}$ [$\text{GeV}$]
LogY=0
END PLOT
BEGIN PLOT /BESIII_2018_I1693610/d01-x01-y02
Title=$K^-\eta^\prime$ mass distribution in $D^0\to K^-\pi^+\eta^\prime$
XLabel=$m_{K^-\eta^\prime}$ [$\text{GeV}$]
YLabel=$1/\Gamma\text{d}\Gamma/\text{d}m_{K^-\eta^\prime}$ [$\text{GeV}$]
LogY=0
END PLOT
BEGIN PLOT /BESIII_2018_I1693610/d01-x01-y03
Title=$\pi^+\eta^\prime$ mass distribution in $D^0\to K^-\pi^+\eta^\prime$
XLabel=$m_{\pi^+\eta^\prime}$ [$\text{GeV}$]
YLabel=$1/\Gamma\text{d}\Gamma/\text{d}m_{\pi^+\eta^\prime}$ [$\text{GeV}$]
LogY=0
END PLOT
BEGIN PLOT /BESIII_2018_I1693610/dalitz_1
Title=Dalitz plot for $D^0\to K^-\pi^+\eta^\prime$
XLabel=$m^2_{K^-\pi^+}$ [$\mathrm{GeV}^2$]
YLabel=$m^2_{\pi^+\eta^\prime}$ [$\mathrm{GeV}^2$]
ZLabel=$1/\Gamma{\rm d}^2 \Gamma/{\rm d}m^2_{K^-\pi^+}/{\rm d}m^2_{\pi^+\eta^\prime}$ [$\rm{GeV}^{-4}$]
LogY=0
END PLOT


BEGIN PLOT /BESIII_2018_I1693610/d01-x01-y04
Title=$K^0_S\pi^0$ mass distribution in $D^0\to K^0_S\pi^0\eta^\prime$
XLabel=$m_{K^0_S\pi^0}$ [$\text{GeV}$]
YLabel=$1/\Gamma\text{d}\Gamma/\text{d}m_{K^0_S\pi^0}$ [$\text{GeV}$]
LogY=0
END PLOT
BEGIN PLOT /BESIII_2018_I1693610/d01-x01-y05
Title=$K^0_S\eta^\prime$ mass distribution in $D^0\to K^0_S\pi^0\eta^\prime$
XLabel=$m_{K^0_S\eta^\prime}$ [$\text{GeV}$]
YLabel=$1/\Gamma\text{d}\Gamma/\text{d}m_{K^0_S\eta^\prime}$ [$\text{GeV}$]
LogY=0
END PLOT
BEGIN PLOT /BESIII_2018_I1693610/d01-x01-y06
Title=$\pi^0\eta^\prime$ mass distribution in $D^0\to K^0_S\pi^0\eta^\prime$
XLabel=$m_{\pi^0\eta^\prime}$ [$\text{GeV}$]
YLabel=$1/\Gamma\text{d}\Gamma/\text{d}m_{\pi^0\eta^\prime}$ [$\text{GeV}$]
LogY=0
END PLOT
BEGIN PLOT /BESIII_2018_I1693610/dalitz_2
Title=Dalitz plot for $D^0\to K^0_S\pi^0\eta^\prime$
XLabel=$m^2_{K^0_S\pi^0}$ [$\mathrm{GeV}^2$]
YLabel=$m^2_{\pi^0\eta^\prime}$ [$\mathrm{GeV}^2$]
ZLabel=$1/\Gamma{\rm d}^2 \Gamma/{\rm d}m^2_{K^0_S\pi^0}/{\rm d}m^2_{\pi^0\eta^\prime}$ [$\rm{GeV}^{-4}$]
LogY=0
END PLOT

BEGIN PLOT /BESIII_2018_I1693610/d01-x01-y07
Title=$K^0_S\pi^+$ mass distribution in $D^+\to K^0_S\pi^+\eta^\prime$
XLabel=$m_{K^0_S\pi^+}$ [$\text{GeV}$]
YLabel=$1/\Gamma\text{d}\Gamma/\text{d}m_{K^0_S\pi^+}$ [$\text{GeV}$]
LogY=0
END PLOT
BEGIN PLOT /BESIII_2018_I1693610/d01-x01-y08
Title=$K^0_S\eta^\prime$ mass distribution in $D^+\to K^0_S\pi^+\eta^\prime$
XLabel=$m_{K^0_S\eta^\prime}$ [$\text{GeV}$]
YLabel=$1/\Gamma\text{d}\Gamma/\text{d}m_{K^0_S\eta^\prime}$ [$\text{GeV}$]
LogY=0
END PLOT
BEGIN PLOT /BESIII_2018_I1693610/d01-x01-y09
Title=$\pi^+\eta^\prime$ mass distribution in $D^+\to K^0_S\pi^+\eta^\prime$
XLabel=$m_{\pi^+\eta^\prime}$ [$\text{GeV}$]
YLabel=$1/\Gamma\text{d}\Gamma/\text{d}m_{\pi^+\eta^\prime}$ [$\text{GeV}$]
LogY=0
END PLOT
BEGIN PLOT /BESIII_2018_I1693610/dalitz_3
Title=Dalitz plot for $D^+\to K^0_S\pi^+\eta^\prime$
XLabel=$m^2_{K^0_S\pi^+}$ [$\mathrm{GeV}^2$]
YLabel=$m^2_{\pi^+\eta^\prime}$ [$\mathrm{GeV}^2$]
ZLabel=$1/\Gamma{\rm d}^2 \Gamma/{\rm d}m^2_{K^0_S\pi^+}/{\rm d}m^2_{\pi^+\eta^\prime}$ [$\rm{GeV}^{-4}$]
LogY=0
END PLOT
