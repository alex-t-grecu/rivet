BEGIN PLOT /BESIII_2022_I2071715/
LogY=0
END PLOT

BEGIN PLOT /BESIII_2022_I2071715/T
LogY=0
XLabel=$\cos\theta_\Lambda$
END PLOT

BEGIN PLOT /BESIII_2022_I2071715/cThetaL
Title=Cross section vs polar angle
LogY=0
XLabel=$\cos\theta_\Lambda$
YLabel=$1/\sigma\mathrm{d}\sigma/\mathrm{d}\cos\theta_L$
END PLOT

BEGIN PLOT /BESIII_2022_I2071715/T1
Title=$T_1$ for $\Lambda\to p^+\pi^-$
YLabel=$T_1$
END PLOT
BEGIN PLOT /BESIII_2022_I2071715/T2
Title=$T_2$ for $\Lambda\to p^+\pi^-$
YLabel=$T_2$
END PLOT
BEGIN PLOT /BESIII_2022_I2071715/T3
Title=$T_3$ for $\Lambda\to p^+\pi^-$
YLabel=$T_3$
END PLOT
BEGIN PLOT /BESIII_2022_I2071715/T4
Title=$T_4$ for $\Lambda\to p^+\pi^-$
YLabel=$T_4$
END PLOT
BEGIN PLOT /BESIII_2022_I2071715/T5
Title=$T_5$ for $\Lambda\to p^+\pi^-$
YLabel=$T_5$
END PLOT
BEGIN PLOT /BESIII_2022_I2071715/d02-x01-y01
Title=$\mu$ for $\Lambda\to p^+\pi^-$ (N.B. data not corrected)
YLabel=$\mu$
XLabel=$\cos\theta_{\Lambda}$
END PLOT

BEGIN PLOT /BESIII_2022_I2071715/d01-x01-y01
Title=$\alpha_\psi$ for $J/\psi\to \Lambda^0\bar\Lambda^0$
YLabel=$\alpha_\psi$
END PLOT
BEGIN PLOT /BESIII_2022_I2071715/d01-x02-y01
Title=$\Delta\Phi$ for $J/\psi\to \Lambda^0\bar\Lambda^0$
YLabel=$\Delta\Phi$ [rad]
END PLOT
BEGIN PLOT /BESIII_2022_I2071715/d01-x03-y01
Title=$\alpha_-$ for $\Lambda^0\to p^+ \pi^-$
YLabel=$\alpha_-$
END PLOT
BEGIN PLOT /BESIII_2022_I2071715/d01-x04-y01
Title=$\alpha_+$ for $\bar\Lambda^0\to \bar{p}^- \pi^+$
YLabel=$\alpha_+$
YMax=-0.6
YMin=-0.8
END PLOT
