Name: BESIII_2021_I1900124
Year: 2021
Summary:  Cross section for $e^+e^-\to\Lambda\bar{\Lambda}$ between 3.5 and 4.6 GeV
Experiment: BESIII
Collider: BEPC
InspireID: 1900124
Status: VALIDATED
Reentrant: true
Authors:
 - Peter Richardson <peter.richardson@durham.ac.uk>
References:
 -     Phys.Rev.D 104 (2021) 9, L091104
RunInfo:  e+e- to hadrons
Beams: [e+, e-]
Energies: [3.5100, 3.5146, 3.5815, 3.6500, 3.6702, 3.7730, 3.8077,
           3.8675, 3.8715, 3.8962, 4.0076, 4.1301, 4.1585, 4.1783,
           4.1893, 4.1996, 4.2097, 4.2188, 4.2263, 4.2358, 4.2439,
           4.2580, 4.2669, 4.2778, 4.2889, 4.3128, 4.3379, 4.3583,
           4.3776, 4.3980, 4.4156, 4.4370, 4.5995]
Options:
 - ENERGY=*
Description:
  'Measurement of the cross section for $e^+e^-\to\Lambda\bar{\Lambda}$ between 3.5 and 4.6 GeV.
   Beam energy must be specified as analysis option "ENERGY" when rivet-merging samples.'
ValidationInfo:
  'Herwig 7 events'
#ReleaseTests:
# - $A my-hepmc-prefix :MODE=some_rivet_flag
Keywords: []
BibKey: BESIII:2021ccp
BibTeX: '@article{BESIII:2021ccp,
    author = "Ablikim, M. and others",
    collaboration = "BESIII",
    title = "{Measurement of the cross section for e+e-\textrightarrow{}\ensuremath{\Lambda}\ensuremath{\Lambda}\textasciimacron{} and evidence of the decay \ensuremath{\psi}(3770)\textrightarrow{}\ensuremath{\Lambda}\ensuremath{\Lambda}\textasciimacron{}}",
    eprint = "2108.02410",
    archivePrefix = "arXiv",
    primaryClass = "hep-ex",
    doi = "10.1103/PhysRevD.104.L091104",
    journal = "Phys. Rev. D",
    volume = "104",
    number = "9",
    pages = "L091104",
    year = "2021"
}'
