BEGIN PLOT /BESIII_2013_I1223625/d01-x01-y01
Title=$p\omega$ mass distribution in $J/\psi\to p\bar{p}\omega$
XLabel=$m_{p\omega}$ [$\mathrm{GeV}$]
YLabel=$1/\Gamma\mathrm{d} \Gamma/\mathrm{d}m_{p\omega}$ [$\mathrm{GeV}^{-1}$]
LogY=0
END PLOT
BEGIN PLOT /BESIII_2013_I1223625/d01-x01-y02
Title=$p\bar{p}$ mass distribution in $J/\psi\to p\bar{p}\omega$
XLabel=$m_{p\bar{p}}$ [$\mathrm{GeV}$]
YLabel=$1/\Gamma\mathrm{d} \Gamma/\mathrm{d}m_{p\bar{p}}$ [$\mathrm{GeV}^{-1}$]
LogY=0
END PLOT
BEGIN PLOT /BESIII_2013_I1223625/d01-x01-y03
Title=$\bar{p}\omega$ mass distribution in $J/\psi\to p\bar{p}\omega$
XLabel=$m_{\bar{p}\omega}$ [$\mathrm{GeV}$]
YLabel=$1/\Gamma\mathrm{d} \Gamma/\mathrm{d}m_{\bar{p}\omega}$ [$\mathrm{GeV}^{-1}$]
LogY=0
END PLOT
BEGIN PLOT /BESIII_2013_I1223625/dalitz
Title=Dalitz plot for  $J/\psi\to p\bar{p}\omega$
XLabel=$m^2_{p\omega}$ [$\mathrm{GeV}^2$]
YLabel=$m^2_{\bar{p}\omega}$ [$\mathrm{GeV}^2$]
ZLabel=$1/\Gamma{\rm d}^2 \Gamma/{\rm d}m^2_{p\omega}/{\rm d}m^2_{\bar{p}\omega}$ [$\rm{GeV}^{-4}$]
LogY=0
END PLOT
