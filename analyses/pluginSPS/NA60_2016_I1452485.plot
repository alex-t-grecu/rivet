BEGIN PLOT /NA60_2016_I1452485/d01-x01-y01
Title=Differential $\eta\to \mu^+\mu^-\gamma$ decay
XLabel=$m_{\mu^+\mu^-}$ [GeV]
YLabel=$\left|F_{\eta}\left(m^2_{\mu^+\mu^-}\right)\right|^2$
END PLOT
BEGIN PLOT /NA60_2016_I1452485/d02-x01-y01
Title=Differential $\omega\to \mu^+\mu^-\pi^0$ decay
XLabel=$m_{\mu^+\mu^-}$ [GeV]
YLabel=$\left|F_{\omega\pi^0}\left(m^2_{\mu^+\mu^-}\right)\right|^2$
END PLOT
