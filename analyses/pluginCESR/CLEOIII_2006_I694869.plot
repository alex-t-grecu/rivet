BEGIN PLOT /CLEOIII_2006_I694869/d01-x01-y01
Title=Cross Section for $e^+e^-\to B_s^{*0}\bar{B}_s^{*0}$
XLabel=$\sqrt{s}$ [GeV]
YLabel=$\sigma$ [nb]
LogY=0
END PLOT