BEGIN PLOT /CLEOII_1996_I398228/d01-x01-y01
Title=$\eta$ momentum spectrum in $B$ decays
XLabel=$p$ [GeV]
YLabel=$\text{d}\mathcal{B}/\text{d}p$ $\text{GeV}^{-1}$
LogY=0
END PLOT
