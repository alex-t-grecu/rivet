BEGIN PLOT /CLEOIII_2006_I701217/d01-x01-y01
Title=Direct Photon Spectrum in $\Upsilon(1S)$ decay
XLabel=$x_\gamma$
YLabel=$1/\Gamma\text{d}\Gamma/\text{d}x_\gamma$
LogY=0
END PLOT
BEGIN PLOT /CLEOIII_2006_I701217/d02-x01-y01
Title=Direct Photon Spectrum in $\Upsilon(2S)$ decay
XLabel=$x_\gamma$
YLabel=$1/\Gamma\text{d}\Gamma/\text{d}x_\gamma$
LogY=0
END PLOT
BEGIN PLOT /CLEOIII_2006_I701217/d03-x01-y01
Title=Direct Photon Spectrum in $\Upsilon(3S)$ decay
XLabel=$x_\gamma$
YLabel=$1/\Gamma\text{d}\Gamma/\text{d}x_\gamma$
LogY=0
END PLOT
