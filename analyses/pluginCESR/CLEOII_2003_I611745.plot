BEGIN PLOT /CLEOII_2003_I611745/d01-x01-y01
Title=Longintudinal polarization in $B^-\to D^{*0}\rho^-$
YLabel=$f_L$
LogY=0
END PLOT
BEGIN PLOT /CLEOII_2003_I611745/d01-x01-y02
Title=Longintudinal polarization in $\bar{B}^0\to D^{*+}\rho^-$
YLabel=$f_L$
LogY=0
END PLOT

BEGIN PLOT /CLEOII_2003_I611745/d02-x01-y01
Title=$D^0$ helicity angle in $B^-\to D^{*0}\rho^-$
XLabel=$\cos\theta$
YLabel=$1/\Gamma\text{d}\Gamma/\text{d}\cos\theta$
LogY=0
END PLOT
BEGIN PLOT /CLEOII_2003_I611745/d02-x01-y02
Title=$D^0$ helicity angle in $\bar{B}^0\to D^{*+}\rho^-$
XLabel=$\cos\theta$
YLabel=$1/\Gamma\text{d}\Gamma/\text{d}\cos\theta$
LogY=0
END PLOT

BEGIN PLOT /CLEOII_2003_I611745/d02-x02-y01
Title=$\pi^-$ helicity angle in $B^-\to D^{*0}\rho^-$
XLabel=$\cos\theta$
YLabel=$1/\Gamma\text{d}\Gamma/\text{d}\cos\theta$
LogY=0
END PLOT
BEGIN PLOT /CLEOII_2003_I611745/d02-x02-y02
Title=$\pi^-$ helicity angle in $\bar{B}^0\to D^{*+}\rho^-$
XLabel=$\cos\theta$
YLabel=$1/\Gamma\text{d}\Gamma/\text{d}\cos\theta$
LogY=0
END PLOT
BEGIN PLOT /CLEOII_2003_I611745/d02-x03-y01
Title=Angle between decay planes in $B^-\to D^{*0}\rho^-$
XLabel=$\chi$
YLabel=$1/\Gamma\text{d}\Gamma/\text{d}\chi$
LogY=0
END PLOT
BEGIN PLOT /CLEOII_2003_I611745/d02-x03-y02
Title=Angle between decay planes in $\bar{B}^0\to D^{*+}\rho^-$
XLabel=$\chi$
YLabel=$1/\Gamma\text{d}\Gamma/\text{d}\chi$
LogY=0
END PLOT

