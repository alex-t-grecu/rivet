BEGIN PLOT /CLEOIII_2006_I694170/d01-x01-y01
Title=$\pi^+\pi^-$ mass distribution in $\Upsilon\to\gamma \pi^+\pi^-$
XLabel=$m_{\pi^+\pi^-}$ [GeV]
YLabel=$1/\Gamma\text{d}\Gamma/\text{d}m_{\pi^+\pi^-}$ [$\text{GeV}^{-1}$]
LogY=0
END PLOT
BEGIN PLOT /CLEOIII_2006_I694170/d02-x01-y01
Title=$\pi^+\pi^-$ mass distribution in $\Upsilon\to\gamma \pi^+\pi^-$
XLabel=$m_{\pi^+\pi^-}$ [GeV]
YLabel=$1/\Gamma\text{d}\Gamma/\text{d}m_{\pi^+\pi^-}$ [$\text{GeV}^{-1}$]
LogY=0
END PLOT
BEGIN PLOT /CLEOIII_2006_I694170/d03-x01-y01
Title=$K^+K^-$ mass distribution in $\Upsilon\to\gamma K^+K^-$
XLabel=$m_{K^+K^-}$ [GeV]
YLabel=$1/\Gamma\text{d}\Gamma/\text{d}m_{K^+K^-}$ [$\text{GeV}^{-1}$]
LogY=0
END PLOT
BEGIN PLOT /CLEOIII_2006_I694170/d04-x01-y01
Title=$p\bar{p}$ mass distribution in $\Upsilon\to\gamma p\bar{p}$
XLabel=$m_{p\bar{p}}$ [GeV]
YLabel=$1/\Gamma\text{d}\Gamma/\text{d}m_{p\bar{p}}$ [$\text{GeV}^{-1}$]
LogY=0
END PLOT

BEGIN PLOT /CLEOIII_2006_I694170/d05-x01-y01
Title=$\cos\theta_\pi$ distribution in $\Upsilon\to\gamma \pi^+\pi^-$ ($f_2$ region)
XLabel=$\cos\theta_\pi$
YLabel=$1/\Gamma\text{d}\Gamma/\text{d}\cos\theta_\pi$
LogY=0
END PLOT
BEGIN PLOT /CLEOIII_2006_I694170/d05-x01-y02
Title=$\cos\theta_\gamma$ distribution in $\Upsilon\to\gamma \pi^+\pi^-$ ($f_2$ region)
XLabel=$\cos\theta_\gamma$
YLabel=$1/\Gamma\text{d}\Gamma/\text{d}\cos\theta_\gamma$
LogY=0
END PLOT
BEGIN PLOT /CLEOIII_2006_I694170/d06-x01-y01
Title=$\cos\theta_K$ distribution in $\Upsilon\to\gamma K^+K^-$ ($f^\prime_2$ region)
XLabel=$\cos\theta_K$
YLabel=$1/\Gamma\text{d}\Gamma/\text{d}\cos\theta_K$
LogY=0
END PLOT
BEGIN PLOT /CLEOIII_2006_I694170/d06-x01-y02
Title=$\cos\theta_\gamma$ distribution in $\Upsilon\to\gamma K^+K^-$ ($f^\prime_2$ region)
XLabel=$\cos\theta_\gamma$
YLabel=$1/\Gamma\text{d}\Gamma/\text{d}\cos\theta_\gamma$
LogY=0
