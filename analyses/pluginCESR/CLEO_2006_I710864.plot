BEGIN PLOT /CLEO_2006_I710864/d01
XLabel=$\sqrt{s}$/GeV
LogY=0
ConnectGaps=1
END PLOT
BEGIN PLOT /CLEO_2006_I710864/d01-x01-y01
Title=$\sigma(e^+e^-\to J/\psi\pi^+\pi^-)$
YLabel=$\sigma(e^+e^-\to J/\psi\pi^+\pi^-)$/pb
END PLOT
BEGIN PLOT /CLEO_2006_I710864/d01-x01-y02
Title=$\sigma(e^+e^-\to J/\psi\pi^0\pi^0)$
YLabel=$\sigma(e^+e^-\to J/\psi\pi^0\pi^0)$/pb
END PLOT
BEGIN PLOT /CLEO_2006_I710864/d01-x01-y03
Title=$\sigma(e^+e^-\to J/\psi K^+K^-)$
YLabel=$\sigma(e^+e^-\to J/\psi K^+K^-)$/pb
END PLOT

BEGIN PLOT /CLEO_2006_I710864/d02-x01-y01
Title=$\pi^+\pi^-$ mass for $\psi(4230)\to \pi^+\pi^-J/\psi$
XLabel=$m_{\pi^+\pi^-}$ [GeV]
YLabel=$1/\Gamma\text{d}\Gamma/\text{d}m_{\pi^+\pi^-}$ [$\text{GeV}^{-1}$]
LogY=0
END PLOT
