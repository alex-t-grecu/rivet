BEGIN PLOT /FOCUS_2004_I654030/d01-x01-y01
Title=$K^+\pi^-$ mass distribution in $D^+\to K^+ \pi^+\pi^-$
XLabel=$m^2_{K^+\pi^-}$ [$\mathrm{GeV}^2$]
YLabel=$1/\Gamma\mathrm{d} \Gamma/\mathrm{d}m_{K^+\pi^+}$ [$\mathrm{GeV}^{-2}$]
LogY=0
END PLOT
BEGIN PLOT /FOCUS_2004_I654030/d01-x01-y02
Title=$\pi^+\pi^-$ mass distribution in $D^+\to K^+ \pi^+\pi^-$
XLabel=$m^2_{\pi^+\pi^-}$ [$\mathrm{GeV}^2$]
YLabel=$1/\Gamma\mathrm{d} \Gamma/\mathrm{d}m_{\pi^+\pi^+}$ [$\mathrm{GeV}^{-2}$]
LogY=0
END PLOT
BEGIN PLOT /FOCUS_2004_I654030/dalitz_1
Title=Dalitz plot for $D^+\to K^+ \pi^+\pi^-$
YLabel=$m^2_{\pi^+\pi^-}$ [$\mathrm{GeV}^2$]
XLabel=$m^2_{K^+\pi^-}$ [$\mathrm{GeV}^2$]
ZLabel=$1/\Gamma{\rm d}^2 \Gamma/{\rm d}m^2_{K^+\pi^-}/{\rm d}m^2_{\pi^+\pi^-}$ [$\rm{GeV}^{-4}$]
LogY=0
END PLOT

BEGIN PLOT /FOCUS_2004_I654030/d02-x01-y01
Title=$K^+\pi^-$ mass distribution in $D_s^+\to K^+ \pi^+\pi^-$
XLabel=$m^2_{K^+\pi^-}$ [$\mathrm{GeV}^2$]
YLabel=$1/\Gamma\mathrm{d} \Gamma/\mathrm{d}m_{K^+\pi^+}$ [$\mathrm{GeV}^{-2}$]
LogY=0
END PLOT
BEGIN PLOT /FOCUS_2004_I654030/d02-x01-y02
Title=$\pi^+\pi^-$ mass distribution in $D_s^+\to K^+ \pi^+\pi^-$
XLabel=$m^2_{\pi^+\pi^-}$ [$\mathrm{GeV}^2$]
YLabel=$1/\Gamma\mathrm{d} \Gamma/\mathrm{d}m_{\pi^+\pi^+}$ [$\mathrm{GeV}^{-2}$]
LogY=0
END PLOT
BEGIN PLOT /FOCUS_2004_I654030/dalitz_2
Title=Dalitz plot for $D_s^+\to K^+ \pi^+\pi^-$
YLabel=$m^2_{\pi^+\pi^-}$ [$\mathrm{GeV}^2$]
XLabel=$m^2_{K^+\pi^-}$ [$\mathrm{GeV}^2$]
ZLabel=$1/\Gamma{\rm d}^2 \Gamma/{\rm d}m^2_{K^+\pi^-}/{\rm d}m^2_{\pi^+\pi^-}$ [$\rm{GeV}^{-4}$]
LogY=0
END PLOT
