BEGIN PLOT /A2_2017_I1498079/d01-x01-y01
Title=Differential $\pi^0\to e^+e^-\gamma$ decay
XLabel=$m_{e^+e^-}$/MeV
YLabel=$\left|F_{\pi^0}\left(m^2_{e^+e^-}\right)\right|^2$
LogY=0
END PLOT
