BEGIN PLOT /ARGUS_1990_I298245/d01-x01-y01
Title=$e^-$ spectrum in continuum
XLabel=$p$ [GeV]
YLabel=$1/N\text{d}N/\text{d}p$ [$\text{GeV}^{1-}$]
END PLOT
BEGIN PLOT /ARGUS_1990_I298245/d01-x01-y02
Title=$\mu^-$ spectrum in continuum
XLabel=$p$ [GeV]
YLabel=$1/N\text{d}N/\text{d}p$ [$\text{GeV}^{1-}$]
END PLOT
BEGIN PLOT /ARGUS_1990_I298245/d02-x01-y01
Title=$e^-$ spectrum in $\Upsilon(4S)$ decays
XLabel=$p$ [GeV]
YLabel=$1/N_B\text{d}N/\text{d}p$ [$\text{GeV}^{1-}$]
LogY=0
END PLOT
BEGIN PLOT /ARGUS_1990_I298245/d02-x01-y02
Title=$\mu^-$ spectrum in $\Upsilon(4S)$ decays
XLabel=$p$ [GeV]
YLabel=$1/N_B\text{d}N/\text{d}p$ [$\text{GeV}^{1-}$]
LogY=0
END PLOT
