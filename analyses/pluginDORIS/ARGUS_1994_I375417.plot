BEGIN PLOT /ARGUS_1994_I375417/d01-x01
XLabel=$\cos\alpha_-$
YLabel=$1/\Gamma\text{d}\Gamma/\text{d}\cos\alpha_-$
LogY=0
END PLOT
BEGIN PLOT /ARGUS_1994_I375417/d01-x01-y01
Title=Helicity angle in $\tau\to\pi^-\pi^0\nu_\tau$ ($0.50<\sqrt{Q^2}<0.69\,$GeV)
END PLOT
BEGIN PLOT /ARGUS_1994_I375417/d01-x01-y02
Title=Helicity angle in $\tau\to\pi^-\pi^0\nu_\tau$ ($0.69<\sqrt{Q^2}<0.776\,$GeV)
END PLOT
BEGIN PLOT /ARGUS_1994_I375417/d01-x01-y03
Title=Helicity angle in $\tau\to\pi^-\pi^0\nu_\tau$ ($0.776<\sqrt{Q^2}<0.85\,$GeV)
END PLOT
BEGIN PLOT /ARGUS_1994_I375417/d01-x01-y04
Title=Helicity angle in $\tau\to\pi^-\pi^0\nu_\tau$ ($0.85<\sqrt{Q^2}<1.1\,$GeV)
END PLOT
