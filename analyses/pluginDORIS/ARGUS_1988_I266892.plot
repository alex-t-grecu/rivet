BEGIN PLOT /ARGUS_1988_I266892/d01-x01-y01
Title=Azimuthal angle between $p\bar{p}$ (continuum, same hemisphere)
XLabel=$\phi_\perp$
YLabel=$1/\sigma\text{d}\sigma/\text{d}\phi_\perp$
LogY=0
END PLOT
BEGIN PLOT /ARGUS_1988_I266892/d01-x01-y02
Title=Azimuthal angle between $p\bar{p}$ ($\Upsilon(1S)$, same hemisphere)
XLabel=$\phi_\perp$
YLabel=$1/\sigma\text{d}\sigma/\text{d}\phi_\perp$
LogY=0
END PLOT
BEGIN PLOT /ARGUS_1988_I266892/d02-x01-y01
Title=Azimuthal angle between $p\bar{p}$ (continuum, opposite hemisphere)
XLabel=$\phi_\perp$
YLabel=$1/\sigma\text{d}\sigma/\text{d}\phi_\perp$
LogY=0
END PLOT

BEGIN PLOT /ARGUS_1988_I266892/d03-x01-y01
Title=Number of $\Lambda^0\bar{\Lambda^0}$ pairs
YLabel=$n_{\Lambda^0\bar\Lambda^0}$
LogY=0
END PLOT

BEGIN PLOT /ARGUS_1988_I266892/d04-x01-y01
Title=Ratio of $\Lambda^0\bar{\Lambda^0}$ pairs
YLabel=$2n_{\Lambda^0\bar\Lambda^0}/(n_{\Lambda^0}+n_{\bar\Lambda^0})$
LogY=0
END PLOT

BEGIN PLOT /ARGUS_1988_I266892/d05-x01-y01
Title=Number of $\Xi^-\bar{\Lambda^0}$ pairs
YLabel=$n_{\Xi^-\bar\Lambda^0}$
LogY=0
END PLOT
BEGIN PLOT /ARGUS_1988_I266892/d06-x01-y01
Title=Ratio of $\Xi^-\bar{\Lambda^0}$ pairs
YLabel=$n_{\Xi^-\bar\Lambda^0}/n_{\Xi^-}$
LogY=0
END PLOT

BEGIN PLOT /ARGUS_1988_I266892/d07-x01-y01
Title=Number of $\Lambda(1520)^0\bar{\Lambda^0}$ pairs
YLabel=$n_{\Lambda(1520)^0\bar\Lambda^0}$
LogY=0
END PLOT
BEGIN PLOT /ARGUS_1988_I266892/d08-x01-y01
Title=Ratio of $\Lambda(1520)^0\bar{\Lambda^0}$ pairs
YLabel=$n_{\Lambda(1520)^0\bar\Lambda^0}/n_{\Lambda(1520)^0}$
LogY=0
END PLOT
