BEGIN PLOT /ARGUS_1987_I247567/d01-x01-y01
Title=Cross section for $\gamma\gamma\to 2\pi^+2\pi^-\pi^0$
YLabel=$\sigma$ [nb]
XLabel=$\sqrt{s}$ [GeV]
LogY=0
ConnectGaps=1
END PLOT
BEGIN PLOT /ARGUS_1987_I247567/d02-x01-y01
Title=Cross section for $\gamma\gamma\to \rho^0\omega$
YLabel=$\sigma$ [nb]
XLabel=$\sqrt{s}$ [GeV]
LogY=0
ConnectGaps=1
END PLOT
