BEGIN PLOT /ARGUS_1987_I247005/d01-x01-y01
Title=$\phi$ heilcity angle in $D^0\to K^0_S\phi$
XLabel=$\cos\theta_\phi$
YLabel=$1/\Gamma\mathrm{d} \Gamma/\mathrm{d}\cos\theta_\phi$
LogY=0
END PLOT
