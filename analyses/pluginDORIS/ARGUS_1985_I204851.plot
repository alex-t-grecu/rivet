BEGIN PLOT /ARGUS_1985_I204851/d01-x01-y01
Title=Cross section for $e^+e^-\to D^{*+}X$ with $D^{*+}\to D^0(\to K^-\pi^+)\pi^+$
YLabel=$\sigma\times\mathcal{B}$ [pb]
XCustomMajorTicks=1. $x_p>0.5$   2.   $x_p>0$ 
LogY=0
END PLOT
BEGIN PLOT /ARGUS_1985_I204851/d01-x01-y02
Title=Cross section for $e^+e^-\to D^{*+}X$ with $D^{*+}\to D^0(\to K^-\pi^+\pi^+\pi^-)\pi^+$
YLabel=$\sigma\times\mathcal{B}$ [pb]
XCustomMajorTicks=1. $x_p>0.5$   2.   $x_p>0$ 
LogY=0
END PLOT

BEGIN PLOT /ARGUS_1985_I204851/d02-x01-y01
Title=$D^{*+}$ scaled momentum
XLabel=$x_p$ 
YLabel=$s\times\mathrm{d}\sigma/\mathrm{d}x_p$ [$\text{GeV}^2\mu\text{b}$]
LogY=0
END PLOT
