# BEGIN PLOT /ATLAS_2023_I2663725/.*
XTwosidedTicks=1
YTwosidedTicks=1
RatioPlot=1
RatioPlotYMin=0.8
RatioPlotYMax=1.2
# END PLOT

# BEGIN HISTOGRAM /ATLAS_2023_I2663725/d06
Title=the transverse momentum of the leading jet
XLabel=$p_\mathrm{T}^\mathrm{Jet1}$ [GeV]
YLabel=$\mathrm{d}\sigma\,/\,\mathrm{d} p_\mathrm{T}^\mathrm{Jet1}$ [fb/GeV]
# END HISTOGRAM

# BEGIN HISTOGRAM /ATLAS_2023_I2663725/d13
Title=the transverse momentum of the leading jet
XLabel=$p_\mathrm{T}^\mathrm{Jet1}$ [GeV]
YLabel=$\mathrm{d}\sigma\,/\,\mathrm{d} p_\mathrm{T}^\mathrm{Jet1}$ [fb/GeV]
# END HISTOGRAM

# BEGIN HISTOGRAM /ATLAS_2023_I2663725/d04
Title=the transverse momentum of the leading lepton
XLabel=$p_\mathrm{T}^\mathrm{Lep1}$ [GeV]
YLabel=$\mathrm{d}\sigma\,/\,\mathrm{d} p_\mathrm{T}^\mathrm{Lep1}$ [fb/GeV]
# END HISTOGRAM

# BEGIN HISTOGRAM /ATLAS_2023_I2663725/d11
Title=the transverse momentum of the leading lepton
XLabel=$p_\mathrm{T}^\mathrm{Lep1}$ [GeV]
YLabel=$\mathrm{d}\sigma\,/\,\mathrm{d} p_\mathrm{T}^\mathrm{Lep1}$ [fb/GeV]
# END HISTOGRAM

# BEGIN HISTOGRAM /ATLAS_2023_I2663725/d05
Title=the transverse momentum of the leading photon
XLabel=$p_\mathrm{T}^\mathrm{Gamma1}$ [GeV]
YLabel=$\mathrm{d}\sigma\,/\,\mathrm{d} p_\mathrm{T}^\mathrm{Gamma1}$ [fb/GeV]
# END HISTOGRAM

# BEGIN HISTOGRAM /ATLAS_2023_I2663725/d12
Title=the transverse momentum of the leading photon
XLabel=$p_\mathrm{T}^\mathrm{Gamma1}$ [GeV]
YLabel=$\mathrm{d}\sigma\,/\,\mathrm{d} p_\mathrm{T}^\mathrm{Gamma1}$ [fb/GeV]
# END HISTOGRAM

# BEGIN HISTOGRAM /ATLAS_2023_I2663725/d07
Title=the momentum of the $Z\gamma$ system
XLabel=$p_\mathrm{T}^{\elll\ell\gamma}$ [GeV]
YLabel=$\mathrm{d}\sigma\,/\,\mathrm{d} p_\mathrm{T}^{\ell\ell\gamma}$ [fb/GeV]
LogX=1
# END HISTOGRAM

# BEGIN HISTOGRAM /ATLAS_2023_I2663725/d15
Title=the momentum of the $Z\gamma$ system
XLabel=$p_\mathrm{T}^{\ell\ell\gamma}$ [GeV]
YLabel=$\mathrm{d}\sigma\,/\,\mathrm{d} p_\mathrm{T}^{\ell\ell\gamma}$ [fb/GeV]
LogX=1
# END HISTOGRAM

# BEGIN HISTOGRAM /ATLAS_2023_I2663725/d14
Title=the momentum of the dilepton system
XLabel=$p_\mathrm{T}^{\ell\ell}$ [GeV]
YLabel=$\mathrm{d}\sigma\,/\,\mathrm{d} p_\mathrm{T}^{\ell\ell}$ [fb/GeV]
LogX=1
# END HISTOGRAM

# BEGIN HISTOGRAM /ATLAS_2023_I2663725/d08
Title=the invariant mass of the two leading jets
XLabel=$m_{jj}$ [GeV]
YLabel=$\mathrm{d}\sigma\,/\,\mathrm{d} m_{jj}$ [fb/GeV]
LogX=1
# END HISTOGRAM

# BEGIN HISTOGRAM /ATLAS_2023_I2663725/d16
Title=the invariant mass of the two leading jets
XLabel=$m_{jj}$ [GeV]
YLabel=$\mathrm{d}\sigma\,/\,\mathrm{d} m_{jj}$ [fb/GeV]
LogX=1
# END HISTOGRAM

# BEGIN HISTOGRAM /ATLAS_2023_I2663725/d10
Title=the azimuthal angle difference between $Z\gamma$ and dijet
XLabel=$\Delta \phi (\ell\ell\gamma, jj)$
YLabel=$\mathrm{d}\sigma\,/\,\mathrm{d} \Delta \phi (\ell\ell\gamma, jj)$ [fb]
# END HISTOGRAM

# BEGIN HISTOGRAM /ATLAS_2023_I2663725/d18
Title=the azimuthal angle difference between $Z\gamma$ and dijet
XLabel=$\Delta \phi (\ell\ell\gamma, jj)$
YLabel=$\mathrm{d}\sigma\,/\,\mathrm{d} \Delta \phi (\ell\ell\gamma, jj)$ [fb]
# END HISTOGRAM

# BEGIN HISTOGRAM /ATLAS_2023_I2663725/d09
Title=the rapidity difference between two leading jets
XLabel=$\Delta y (j,j)$
YLabel=$\mathrm{d}\sigma\,/\,\mathrm{d} \Delta y (j,j)$ [fb]
# END HISTOGRAM

# BEGIN HISTOGRAM /ATLAS_2023_I2663725/d17
Title=the rapidity difference between two leading jets
XLabel=$\Delta y (j,j)$
YLabel=$\mathrm{d}\sigma\,/\,\mathrm{d} \Delta y (j,j)$ [fb]
# END HISTOGRAM

# BEGIN HISTOGRAM /ATLAS_2023_I2663725/d19
Title=the $\gamma$ centrality
XLabel=Centrality
YLabel=$\mathrm{d}\sigma\,/\,\mathrm{d}$ Centrality [fb]
# END HISTOGRAM

