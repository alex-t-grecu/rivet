# BEGIN PLOT /ATLAS_2018_I1711223/d..
LogY=0
YTwosidedTicks=1
# END PLOT

# BEGIN PLOT /ATLAS_2018_I1711223/d12-x01-y01
Title=$W^{\pm}Zjj\to \ell\nu\ell\ell$ fid. cross section as a function of the jet multiplicity
YLabel=$\sigma^{\mathrm{fid.}}$ [fb]
XLabel=$\mathrm{N}_{\mathrm{jets}}$
LogY=1
XCustomMajorTicks=2 2 3 3 4 4 5 $\geq5$
# END PLOT #

# BEGIN PLOT /ATLAS_2018_I1711223/d06-x01-y01
Title=Differential fiducial $W^{\pm}Zjj$ cross section as a function of $m_\mathrm{T}^{WZ}$
YLabel=$\Delta\sigma^{\mathrm{fid.}}$ [fb]
XLabel=$m_\mathrm{T}^{WZ}$ [GeV]
LogY=1
# END PLOT #

# BEGIN PLOT /ATLAS_2018_I1711223/d08-x01-y01
Title=Differential fiducial $W^{\pm}Zjj$ cross section as a function of $\Sum p_\mathrm{T}^\ell$
YLabel=$\Delta\sigma^{\mathrm{fid.}}$ [fb]
XLabel=$\Sum p_\mathrm{T}^\ell$ [GeV]
LogY=1
# END PLOT #

# BEGIN PLOT /ATLAS_2018_I1711223/d10-x01-y01
Title=Differential fiducial $W^{\pm}Zjj$ cross section as a function of $\Delta\phi(W,Z)$
YLabel=$\Delta\sigma^{\mathrm{fid.}}$ [fb]
XLabel=$\Delta\phi(W,Z)$ [rad]
LogY=1
# END PLOT #

# BEGIN PLOT /ATLAS_2018_I1711223/d14-x01-y01
Title=Differential fiducial $W^{\pm}Zjj$ cross section as a function of $m_{jj}$
YLabel=$\sigma^{\mathrm{fid.}}$ [fb]
XLabel=$m_{jj}$ [GeV]
LogY=1
# END PLOT #

# BEGIN PLOT /ATLAS_2018_I1711223/d16-x01-y01
Title=Differential fiducial $W^{\pm}Zjj$ cross section as a function of $|\Delta y_{jj}|$
YLabel=$\sigma^{\mathrm{fid.}}$ [fb]
XLabel=$|\Delta y_{jj}|$
LogY=1
# END PLOT #

# BEGIN PLOT /ATLAS_2018_I1711223/d18-x01-y01
Title=Differential fiducial $W^{\pm}Zjj$ cross section as a function of $\Delta \phi_{jj}$
YLabel=$\sigma^{\mathrm{fid.}}$ [fb]
XLabel=$\Delta \phi_{jj}$ [rad]
LogY=1
# END PLOT #

# BEGIN PLOT /ATLAS_2018_I1711223/d20-x01-y01
Title=$W^{\pm}Zjj\to \ell\nu\ell\ell$ fid. cross section as a function of gap-jet multiplicity
YLabel=$\sigma^{\mathrm{fid.}}$ [fb]
XLabel=$N_\mathrm{jets}^\mathrm{gap}$
LogY=1
XCustomMajorTicks=0 0 1 1 2 2 3 $\geq3$
# END PLOT #

