# BEGIN PLOT /ATLAS_2022_I2103950/d11
XLabel=$\cos\theta^\ast$
YLabel=$\mathrm{d}\sigma/\mathrm{d}\cos\theta^\ast$ [fb]
# END PLOT

# BEGIN PLOT /ATLAS_2022_I2103950/d09
XLabel=$|\Delta\phi_{e\mu}|$
YLabel=$\mathrm{d}\sigma/\mathrm{d}|\Delta\phi_{e\mu}|$ [fb]
# END PLOT

# BEGIN PLOT /ATLAS_2022_I2103950/d15
XLabel=$m_{e\mu}$ [GeV]
YLabel=$\mathrm{d}\sigma/\mathrm{d}m_{e\mu}$ [fb/GeV]
# END PLOT

# BEGIN PLOT /ATLAS_2022_I2103950/d13
XLabel=$p_\mathrm{T}^\mathrm{lead\,\ell}$ [GeV]
YLabel=$\mathrm{d}\sigma/\mathrm{d}p_\mathrm{T}^\mathrm{lead\,\ell}$ [fb/GeV]
# END PLOT

# BEGIN PLOT /ATLAS_2022_I2103950/d17
XLabel=$p_\mathrm{T}^{e\mu}$ [GeV]
YLabel=$\mathrm{d}\sigma/\mathrm{d}p_\mathrm{T}^{e\mu}$ [fb/GeV]
# END PLOT

# BEGIN PLOT /ATLAS_2022_I2103950/d07
XLabel=$|y_{e\mu}|$
YLabel=$\mathrm{d}\sigma/\mathrm{d}|y_{e\mu}|$ [fb]
# END PLOT

